#include <iostream>
#include <chrono>
#include <xmmintrin.h>
#include <cassert>
#include <pmmintrin.h>
#include <cmath>

/*
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    // code here

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
 */

void TestAddSSE() {
    std::cout << __FUNCTION__ << std::endl;
    alignas(16) float A[4];
    alignas(16) float B[4]{2.0f, 4.0f, 6.0f, 8.0f};

    __m128 a {_mm_set_ps(4.0f, 3.0f, 2.0f, 1.0f)};
    __m128 b {_mm_load_ps(&B[0])};

    __m128 result {_mm_add_ps(a, b)};

    _mm_store_ps(&A[0], a);

    alignas(16) float Result[4];
    _mm_store_ps(&Result[0], result);

    std::cout << "A: ";
    for(size_t i{0}; i < 4; ++i) {
        std::cout << A[i] << ", ";
    }
    std::cout << std::endl;

    std::cout << "B: ";
    for(size_t i{0}; i < 4; ++i) {
        std::cout << B[i] << ", ";
    }
    std::cout << std::endl;

    std::cout << "Result: ";
    for(size_t i{0}; i < 4; ++i) {
        std::cout << Result[i] << ", ";
    }
    std::cout << std::endl << std::endl;
}

void AddArrays_ref(size_t count, float* results, const float* dataA, const float* dataB) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i{0}; i < count; ++i) {
        results[i] = dataA[i] + dataB[i];
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}

void AddArrays_sse(size_t count, float* results, const float* dataA, const float* dataB) {
    std::cout << __FUNCTION__ << std::endl;
    assert(count % 4 == 0);

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i{0}; i < count; i += 4) {
        __m128 a{_mm_load_ps(&dataA[i])};
        __m128 b{_mm_load_ps(&dataB[i])};
        __m128 result{_mm_add_ps(a, b)};
        _mm_store_ps(&results[i], result);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}

void DotArrays_ref(size_t count, float* result, const float* a, const float* b) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i{0}; i < count; ++i) {
        const size_t j{i * 4};

        result[i] = a[j+0] * b[j+0]
                  + a[j+1] * b[j+1]
                  + a[j+2] * b[j+2]
                  + a[j+3] * b[j+3];
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}

/* slower because of 'adding a cross register'*/
/* Not used because for some reason the compiler does not like _mm_hadd_ps() ?
void DotArrays_sse_horizontal(size_t count, float result[], const float a[], const float b[]) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i{0}; i < count; ++i) {
        const size_t j{i * 4};

        __m128 va{_mm_load_ps(&a[j])};
        __m128 vb{_mm_load_ps(&b[j])};

        __m128 v0{_mm_mul_ps(va, vb)};

        // add across the register
        // (v0w+v0z, v0y+v0x, v0w+v0z, v0y+v0x)
        __m128 v1{_mm_hadd_ps(v0, v0)};

        __m128 vr{_mm_hadd_ps(v1, v1)};

        _mm_store_ss(&result[i], vr);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}
 */

void DotArrays_sse(size_t count, float* r, const float* a, const float *b) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i{0}; i < count; i += 4) {
        __m128 vaX{_mm_load_ps(&a[(i+0)*4])};   // a[0, 4, 8, 12]
        __m128 vaY{_mm_load_ps(&a[(i+1)*4])};   // a[1, 5, 9, 13]
        __m128 vaZ{_mm_load_ps(&a[(i+2)*4])};   // a[2, 6, 10, 14]
        __m128 vaW{_mm_load_ps(&a[(i+3)*4])};   // a[3, 7, 11, 15]

        __m128 vbX{_mm_load_ps(&b[(i+0)*4])};   // b[0, 4, 8, 12]
        __m128 vbY{_mm_load_ps(&b[(i+1)*4])};   // b[1, 5, 9, 13]
        __m128 vbZ{_mm_load_ps(&b[(i+2)*4])};   // b[2, 6, 10, 14]
        __m128 vbW{_mm_load_ps(&b[(i+3)*4])};   // b[3, 7, 11, 15]

        __m128 result{_mm_mul_ps(vaX, vbX)};
        result = _mm_add_ps(result, _mm_mul_ps(vaY, vbY));
        result = _mm_add_ps(result, _mm_mul_ps(vaZ, vbZ));
        result = _mm_add_ps(result, _mm_mul_ps(vaW, vbW));

        _mm_store_ps(&r[i], result);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}

void DotArray_sse_transpose(size_t count, float* r, const float* a, const float* b) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i{0}; i < count; i += 4) {
        __m128 vaX{_mm_load_ps(&a[(i+0)*4])};   // a[0, 4, 8, 12]
        __m128 vaY{_mm_load_ps(&a[(i+1)*4])};   // a[1, 5, 9, 13]
        __m128 vaZ{_mm_load_ps(&a[(i+2)*4])};   // a[2, 6, 10, 14]
        __m128 vaW{_mm_load_ps(&a[(i+3)*4])};   // a[3, 7, 11, 15]

        __m128 vbX{_mm_load_ps(&b[(i+0)*4])};   // b[0, 4, 8, 12]
        __m128 vbY{_mm_load_ps(&b[(i+1)*4])};   // b[1, 5, 9, 13]
        __m128 vbZ{_mm_load_ps(&b[(i+2)*4])};   // b[2, 6, 10, 14]
        __m128 vbW{_mm_load_ps(&b[(i+3)*4])};   // b[3, 7, 11, 15]

        _MM_TRANSPOSE4_PS(vaX, vaY, vaZ, vaW);
        _MM_TRANSPOSE4_PS(vbX, vbY, vbZ, vbW);

        __m128 result{_mm_mul_ps(vaX, vbX)};
        result = _mm_add_ps(result, _mm_mul_ps(vaY, vbY));
        result = _mm_add_ps(result, _mm_mul_ps(vaZ, vbZ));
        result = _mm_add_ps(result, _mm_mul_ps(vaW, vbW));

        _mm_store_ps(&r[i], result);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}

union Mat4x4 {
    float c[4][4];
    __m128 row[4];
};

__m128 MulVecMat_sse(const __m128& v, const Mat4x4& M) {
    // transpose v
    __m128 vX = _mm_shuffle_ps(v, v, 0x00);     // (vx, vx, vx, vx)
    __m128 vY = _mm_shuffle_ps(v, v, 0x55);     // (vy, vy, vy, vy)
    __m128 vZ = _mm_shuffle_ps(v, v, 0xAA);     // (vz, vz, vz, vz)
    __m128 vW = _mm_shuffle_ps(v, v, 0xFF);     // (vw, vw, vw, vw)

    __m128 result{_mm_mul_ps(vX, M.row[0])};
    result = _mm_add_ps(result, _mm_mul_ps(vY, M.row[1]));
    result = _mm_add_ps(result, _mm_mul_ps(vZ, M.row[2]));
    result = _mm_add_ps(result, _mm_mul_ps(vW, M.row[3]));

    return result;
}

void MulMatMat_sse(Mat4x4& result, const Mat4x4& A, const Mat4x4& B) {
    result.row[0] = MulVecMat_sse(A.row[0], B);
    result.row[1] = MulVecMat_sse(A.row[1], B);
    result.row[2] = MulVecMat_sse(A.row[2], B);
    result.row[3] = MulVecMat_sse(A.row[3], B);
}

void SqrtArray_ref(size_t count, float* __restrict__ result, const float* __restrict__ a) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    for(size_t i{0}; i < count; ++i) {
        if(a[i] >= 0.0f) {
            result[i] = std::sqrt(a[i]);
        }
        else {
            result[i] = 0.0f;
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}

/*
void SqrtArray_sse_broken(size_t count, float* __restrict__ result, const float* __restrict__ a) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    assert(count % 4 == 0);

    __m128 vz{_mm_set1_ps(0.0f)}; // all zeroes

    for(size_t i{0}; i < count; i+= 4) {
        __m128 va{_mm_load_ps(a + 1)};

        __m128 vr;
        if(_mm_cmpge_ps(va, vz)) {      // ???
            vr = _mm_sqrt_ps(va);
        }
        else {
            vr = vz;
        }

        _mm_store_ps(result + i, vr);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}
 */

void SqrtArray_sse(size_t count, float* __restrict__ result, const float* __restrict__ a) {
    std::cout << __FUNCTION__ << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    assert(count % 4 == 0);

    __m128 vz{_mm_set1_ps(0.0f)}; // all zeroes

    for(size_t i{0}; i < count; i+= 4) {
        __m128 va{_mm_load_ps(a + 1)};

        /*
         * always do the quotient, but it may end up producing QNaN in some or all lanes
         */
        __m128 vq = _mm_sqrt_ps(va);

        /*
         * now select between vq and vz depending on whether the input was greater than or equal to zero or not
         */
        __m128 mask{_mm_cmpge_ps(va, vz)};

        // (vq & mask) | (vz & ~mask)
        __m128 qmask{_mm_and_ps(mask, vq)};
        __m128 znotmask{_mm_andnot_ps(mask, vz)};
        __m128 vr{_mm_or_ps(qmask, znotmask)};

        _mm_store_ps(result + i, vr);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto time = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);

    std::cout << "Duration: " << time.count() << "ns." << std::endl;
}

__m128 _mm_select_ps(const __m128 a, const __m128 b, const __m128 mask) {
    // (b & mask) | (a & ~mask))
    __m128 bmask{_mm_and_ps(mask, b)};
    __m128 anotmask{_mm_andnot_ps(mask, a)};
    return _mm_or_ps(bmask, anotmask);

    /*
     * example:
     *  a = 0000 0000 0000 0000
     *  b = 0100 1111 0001 1010
     *  mask = 1111 1111 1111 0000
     *  -> the first 3 were valid numbers, the last resulted in undefined behavior
     *
     *  (b & mask)
     *  0100 1111 0001 0000
     *
     *  (a & ~mask)
     *  0000 0000 0000 0000
     *
     *  (bmask | anotmask )
     *  0100 1111 0001 0000
     *
     * another approach would look like this
     *
     *     __m128 diff{_mm_xor_ps(a, b)};
     *     __m128 diffmask{_mm_and_ps(mask, diff)};
     *     return _mm_xor_ps(a, diffmask);
     *
     * example:
     *  a = 0000 0000 0000 0000
     *  b = 0100 1111 0001 1010
     *  mask = 1111 1111 1111 0000
     *  -> the first 3 were valid numbers, the last resulted in undefined behavior
     *
     *  (((a ^ b) & mask) ^ a)
     *
     *  (a ^ b)
     *  0100 1111 0001 1010
     *
     *  (diff & mask)
     *  0100 1111 0001 0000
     *
     *  (diffmask ^ a)
     *  0100 1111 0001 0000
     */
}

void DotKernel(size_t i, float result[], const float a[], const float b[]) {
    /* treat  each block of four floats as a single four-element vector */
    const size_t j{i * 4};

    result[i] =   a[j+0] * b[j+0]
                + a[j+1] * b[j+1]
                + a[j+2] * b[j+2]
                + a[j+3] * b[j+3];
}

void DotArrays_gpgpu1(size_t count, float result[], const float a[], const float b[]) {
    for(size_t i{0}; i < count; ++i) {
        DotKernel(i, result, a, b);
    }
}

/*
__global__ void DotKernel_CUDA(size_t count, float result[], const float a[], const float b[]) {
    size_t i{threadIdx.x};

    if(i < count) {
        // treat  each block of four floats as a single four-element vector
        const size_t j{i * 4};

        result[i] =   a[j+0] * b[j+0]
                      + a[j+1] * b[j+1]
                      + a[j+2] * b[j+2]
                      + a[j+3] * b[j+3];
    }
}

void DotArrays_gpgpu2(size_t count, float result[], const float a[], const float b[]) {
    // allocate managed buffers, that are visible to both CPU and GPU
    int *cr, *ca, *cb;
    cudaMallocManaged(&cr, count * sizeof(float));
    cudaMallocManaged(&ca, count * sizeof(float) * 4);
    cudaMallocManaged(&cb, count * sizeof(float) * 4);

    // transfer the data into GPU-visible memory
    memcpy(ca, a, count * sizeof(float) * 4);
    memcpy(cb, b, count * sizeof(float) * 4);

    // run the kernel on the GPU
    DotKernel_CUDA <<<1, count>>> (cr, ca, cb, count);

    // wait for GPU to finish
    cudaDeviceSynchronize();

    // return results and clean up
    memcpy(result, cr, count * sizeof(float) * 4);
    cudeFree(cr);
    cudeFree(ca);
    cudeFree(cb);
}
*/

int main() try {
    TestAddSSE();

    const size_t count{1024*1024*16};
    alignas(16) float* nums1{nullptr};
    alignas(16) float* nums2{nullptr};
    alignas(16) float* result{nullptr};
    try {
        nums1 = new float[count];
        nums2 = new float[count];
        result = new float[count];

        for(size_t i{0}; i < count; ++i) {
            nums1[i] = static_cast<float>(i);
            nums2[i] = static_cast<float>(count - i);
        }

        AddArrays_ref(count, result, nums1, nums2);
        AddArrays_sse(count, result, nums1, nums2);

        DotArrays_ref(count / 4, result, nums1, nums2);
        DotArrays_sse(count / 4, result, nums1, nums2);
        DotArray_sse_transpose(count / 4, result, nums1, nums2);
    }
    catch(...) {
        // do nothing here...just make sure that memory is freed
        std::cerr << "Got here..." << std::endl;
    }

    delete[] nums1;
    delete[] nums2;
    delete[] result;

    return 0;
}
catch(const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return -1;
}
catch(...) {
    std::cerr << "Unknown exception!" << std::endl;
    return -2;
}
