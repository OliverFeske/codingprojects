//
// Created by Olive on 13/03/2024.
//

#include "BinaryArithmetic.h"
#include <stdexcept>
#include <iostream>
#include <iomanip>

std::string BinaryArithmetic::withSeparators(std::string bitString) {
    if(bitString.empty()) {
        return {};
    };

    size_t neededLeadingZeroes = bitString.size() % 4 != 0 ? 4 - (bitString.size() % 4) : 0;
    bitString = std::string(neededLeadingZeroes, '0') + bitString;
    const size_t numSeparators{(bitString.size() - 1) / 4};
    std::string result(bitString.size() + numSeparators, '_');

    size_t bitIndex = 0;
    for (size_t i = 0; i < result.size(); i++) {
        if ((i + 1) % 5 != 0) {
            result[i] = bitString[bitIndex];

            bitIndex++;
        }
    }

    return result;
}

void BinaryArithmetic::resizeToLargest(std::string &a, std::string &b) {
    if(a.size() > b.size()) {
        b = std::string(a.size() - b.size(), '0') + b;
    } else {
        a = std::string(b.size() - a.size(), '0') + a;
    }
}

std::string BinaryArithmetic::addition(const std::string &augend, const std::string &addend, bool printSteps) {
    std::string result(augend.size(), ' ' );
    bool carry{ false };
    std::string carryString;

    auto printStep = [&carry, &result, &augend, &addend, &carryString](size_t stepIndex){
        const long long width = result.size() + 2;
        carryString.push_back(carry ? '1' : '0');
        std::cout
                << "Step " << stepIndex << ":\n"
                << std::setw(width) << augend << '\n'
                << std::setw(width) << "+ " + addend << '\n'
                << std::setw(width) << carryString << '\n'
                << std::setw(width) << std::setfill('-') << '-' << '\n'
                << std::setw(width) << std::setfill(' ') << "= " + result << '\n'
                << '\n';
    };

    for(int i = result.size() - 1; i >= 0; i--) {
        bool first{ augend[i] == '1' };
        bool second{ addend[i] == '1' };

        size_t number = carry + first + second;

        result[i] = number % 2 == 0 ? '0' : '1';

        carry = number > 1;

        if(printSteps) {
            printStep(result.size() - i);
        }
    }

    if(carry) {
        result = "1" + result;
        if(printSteps) {
            printStep(result.size());
        }
    }

    return result;
}

std::string BinaryArithmetic::subtraction(const std::string &minuend, const std::string &subtrahend, bool printSteps) {
    std::string result(minuend.size(), ' ' );
    bool borrow{ false };
    std::string borrowString;

    for(int i = result.size() - 1; i >= 0; i--) {
        bool first { minuend[i] == '1'};
        bool second { subtrahend[i] == '1'};

        int number = first - second - borrow;

        result[i] = number % 2 == 0 ? '0' : '1';

        borrow = number < 0;

        if(printSteps) {
            const long long width = result.size() + 2;
            borrowString.push_back(borrow ? '1' : '0');
            std::cout
                    << "Step " << result.size() - i << ":\n"
                    << std::setw(width) << minuend << '\n'
                    << std::setw(width) << "+ " + subtrahend << '\n'
                    << std::setw(width) << borrowString << '\n'
                    << std::setw(width) << std::setfill('-') << '-' << '\n'
                    << std::setw(width) << std::setfill(' ') << "= " + result << '\n'
                    << '\n';
        }
    }

    return result;
}

std::string BinaryArithmetic::multiplication(const std::string &multiplicand, const std::string &multiplier, bool printSteps) {
    std::string result;

    for(int i = multiplier.size() - 1; i >= 0; i--) {
        std::string partialSum{ multiplier[i] == '1' ? multiplicand : std::string(multiplicand.size(), '0') };

        partialSum += std::string(multiplier.size() - 1 - i , '0');

        resizeToLargest(partialSum, result);

        std::string newSum{ addition(partialSum, result) };

        if(printSteps) {
            const long long width = multiplicand.size() + multiplier.size() + 2;
            std::cout
                    << "Step " << multiplier.size() - i << ":\n"
                    << std::setw(width) << multiplicand << '\n'
                    << "x " << std::setw(width - 2) << multiplier << '\n'
                    << std::setw(width) << std::setfill('-') << '-' << '\n'
                    << std::setw(width) << std::setfill(' ') << result << '\n'
                    << "x " << std::setw(width - 2) << partialSum << '\n'
                    << std::setw(width) << std::setfill('-') << '-' << '\n'
                    << "= " << std::setw(width - 2) << std::setfill(' ') << newSum << '\n'
                    << '\n';
        }

        result = newSum;
    }

    return result;
}

std::string BinaryArithmetic::division(const std::string &dividend, const std::string &divisor, bool printSteps) {
    if(printSteps) {
        const long long width = dividend.size() + 2;
        std::cout
                << std::setw(width) << dividend << '\n'
                << "/ " << std::setw(width - 2) << divisor << '\n'
                << std::setw(width) << std::setfill('-') << '-' << std::setfill(' ') << '\n'
                << "  " << divisor << "\n";
    }

    std::string result;

    std::string remainder{ dividend.substr(0, divisor.size()) };

    for(size_t i = remainder.size(); i < dividend.size(); i++) {
        bool doesFit = divisor <= remainder;
        result.push_back(doesFit ? '1' : '0');

        std::string newRemainder = doesFit ? subtraction(remainder, divisor) : remainder;
        newRemainder += dividend[i];
        newRemainder = newRemainder.substr(1, newRemainder.size() - 1);

        if(printSteps) {
            std::cout
                    << std::setw(i + 2) << (doesFit ? divisor : std::string(divisor.size(), '0')) << '\n'
                    << std::setw(dividend.size() + 2) << std::setfill('-') << '-' << std::setfill(' ') << " -> put " << doesFit << " bring " << dividend[i] << '\n'
                    << std::setw(i + 3) << newRemainder << '\n';
        }

        remainder = newRemainder;
    }

    bool doesFit = divisor <= remainder;
    result.push_back(doesFit ? '1' : '0');

    if(printSteps) {
        const long long width = dividend.size() + 2;
        std::cout
                << std::setw(width) << divisor << '\n'
                << std::setw(width) << std::setfill('-') << '-' << std::setfill(' ') << " -> put " << doesFit << '\n'
                << std::setw(width) << (doesFit ? subtraction(remainder, divisor) : remainder) << '\n'
                << "\n"
                << "= " << std::setw(width - 2) << std::setfill(' ') << result << '\n'
                << '\n';
    }

    return result;
}

void BinaryArithmetic::additionExamples() {
    std::cout << withSeparators(addition("0000", "0000")) << std::endl;
    std::cout << withSeparators(addition("1111", "1111")) << std::endl;
    std::cout << withSeparators(addition("1010", "0101")) << std::endl;
    std::cout << withSeparators(addition("10101010", "01010101")) << std::endl;
    std::cout << withSeparators(addition("11001101", "00111011", true)) << std::endl;
}

void BinaryArithmetic::subtractionExamples() {
    std::cout << withSeparators(subtraction("11001101", "00111011", true)) << std::endl;
    std::cout << withSeparators(subtraction("10011111", "00010001")) << std::endl;
    std::cout << withSeparators(subtraction("01110111", "00001001")) << std::endl;
}

void BinaryArithmetic::multiplicationExamples() {
    std::cout << withSeparators(multiplication("1010", "10101", true)) << std::endl;
}

void BinaryArithmetic::divisionexamples() {
    std::cout << withSeparators(division("11", "10")) << std::endl;
    std::cout << withSeparators(division("1000", "100")) << std::endl;
    std::cout << withSeparators(division("10000", "1000")) << std::endl;
    std::cout << withSeparators(division("11011", "11")) << std::endl;
    std::cout << withSeparators(division("11011", "10", true)) << std::endl;
}

