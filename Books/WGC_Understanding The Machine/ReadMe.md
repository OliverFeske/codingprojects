# Write Great Code Vol. 1 - Understanding The Machine

## 2. Numeric Representation

### 2.2 Numbering System

##### 2.2.4.1 Representing hexadecimal values in different programming languages

- 0x as prefix in C based languages > 0xdead
- 0 prefix and h or H suffix in MASM > 0deadh
- &h or &H prefix in Visual Basic > &hdead
- $ prefix in Delphi/Free Pascal > $dead
- $ prefix in HLA as well as _ as separators > $dead_beef

##### 2.2.5.1 Representing Octal Values in Programming Languages

- 0(zero) in C > 0123 = 83(base 10)
- Q or q in MASM > q123
- 0o in Swift > 0o123
- &O (letter O not zero) in Visual Basic > &O123

### 2.4 Internal Numeric Representation

#### 2.4.2 Bit Strings

- 4 Bits is a Nibble > represents a hexadecimal digit
- 8 Bits are a Byte
- 16/32/64/... Bits are a word
- LO(low order/least significant)
- HO(high order/most significant)
- e.g.: 7 6 5 4 3 2 1 0
    - LO = 0
    - HO = 7

### 2.6 Useful Properties of Binary Numbers

- // todo: add these

### 2.8 Saturation

When converting a larger type to a smaller type you can use saturation to convert
to the wanted type with a possible loss of precision. Clipping will happen if
the larger type is outside the bounds of the smaller type.

### 2.10 Fixed Point Representation

The accuracy for a fractional number is equivalent to 1/(2 ^ n; n = number of
digits after the point). The result will choose the closest representation.
So for n = 2 and wanted 2.4 the result will be 2.5 as the only options for the
fraction are 0.0, 0.25, 0.5 and 0.75.

## 3 Binary Arithmetic and Bit Operations

