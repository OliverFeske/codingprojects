cmake_minimum_required(VERSION 3.26)
project(WGC_Understanding_The_Machine)

set(CMAKE_CXX_STANDARD 23)

add_executable(WGC_Understanding_The_Machine main.cpp
        BinaryArithmetic.cpp
        BinaryArithmetic.h)
