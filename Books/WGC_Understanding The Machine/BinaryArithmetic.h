//
// Created by Olive on 13/03/2024.
//

#ifndef WGC_UNDERSTANDING_THE_MACHINE_BINARYARITHMETIC_H
#define WGC_UNDERSTANDING_THE_MACHINE_BINARYARITHMETIC_H

#include <vector>
#include <string>

class BinaryArithmetic {
public:
    static std::string withSeparators(std::string bitString);

    /** Only works for positive numbers. */
    static void resizeToLargest(std::string& a, std::string& b);

    /** Limited to only positive numbers. Numbers must be the same size! */
    static std::string addition(const std::string &augend, const std::string &addend, bool printSteps = false);

    /** Limited to only subtracting a smaller number from a bigger number. */
    static std::string subtraction(const std::string &minuend, const std::string &subtrahend, bool printSteps = false);

    /** Limited to only positive numbers. */
    static std::string multiplication(const std::string &multiplicand, const std::string &multiplier, bool printSteps = false);

    /** Limited to positive numbers. */
    static std::string division(const std::string &dividend, const std::string &divisor, bool printSteps = false);

    static std::string modulo(const std::string &dividend, const std::string &divisor, bool printSteps = false) {}

    static std::string exponentiation(const std::string &base, const std::string &divisor, bool printSteps = false) {}

    static std::string root(const std::string &degree, const std::string &radicand, bool printSteps = false) {}

    static std::string logarithm(const std::string &base, const std::string &antilogarithm, bool printSteps = false) {}

    static void additionExamples();

    static void subtractionExamples();

    static void multiplicationExamples();

    static void divisionexamples();
};

#endif //WGC_UNDERSTANDING_THE_MACHINE_BINARYARITHMETIC_H
