#version 330 core
in vec2 TexCoords;

uniform sampler2D Texture;

out vec4 FragColor;

void main()
{
    const float Offset = 1.0 / 300.0;
    vec2 Offsets[9] = vec2[](
        vec2(-Offset, Offset),  // top left
        vec2(0.0, Offset),      // top center
        vec2(Offset, Offset),   // top right
        vec2(-Offset, 0.0),     // center left
        vec2(0.0, 0.0),         // center center
        vec2(Offset, 0.0),      // center right
        vec2(-Offset, -Offset), // bottom left
        vec2(0.0, -Offset),     // bottom center
        vec2(Offset, -Offset)  // bottom right
    );

    float Kernel[9] = float[](
        1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0,
        2.0 / 16.0, 4.0 / 16.0, 2.0 / 16.0,
        1.0 / 16.0, 2.0 / 16.0, 1.0 / 16.0
    );

    vec3 SampleTex[9];
    for(int i = 0; i < 9; i++) {
        SampleTex[i] = vec3(texture(Texture, TexCoords.st + Offsets[i]));
    }

    vec3 Color = vec3(0.0);
    for(int i = 0; i < 9; i++) {
        Color += SampleTex[i] * Kernel[i];
    }

    FragColor = vec4(Color, 1.0);
}