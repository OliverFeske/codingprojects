#version 330 core
in vec2 TexCoords;

uniform sampler2D TextureDiffuse1;

out vec4 FragColor;

void main()
{
	FragColor = texture(TextureDiffuse1, TexCoords);
}