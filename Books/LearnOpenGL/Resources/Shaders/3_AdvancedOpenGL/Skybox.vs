#version 330 core
layout (location = 0) in vec3 aPos;

out vec3 TexCoords;

uniform mat4 View;
uniform mat4 Projection;

void main()
{
    TexCoords = aPos;
    vec4 Pos = Projection * View * vec4(aPos, 1.0);
    gl_Position = Pos.xyww; // This sets the z to w / w which is always 1, so it will always be behind other objects
}