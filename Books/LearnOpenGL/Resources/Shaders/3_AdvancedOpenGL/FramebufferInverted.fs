#version 330 core
in vec2 TexCoords;

uniform sampler2D Texture;

out vec4 FragColor;

void main()
{
    FragColor = vec4(vec3(1.0 - texture(Texture, TexCoords)), 1.0);
}