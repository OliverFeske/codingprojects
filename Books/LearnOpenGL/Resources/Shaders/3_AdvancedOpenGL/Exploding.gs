#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS_OUT {
    vec2 TexCoords;
} gs_in[];

uniform float Time;

out vec2 TexCoords;

vec3 GetNormal()
{
    vec3 A = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
    vec3 B = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
    return normalize(cross(A, B));
}

vec4 explode(vec4 Position, vec3 Normal) 
{
    float Magnitude = 2.0;
    vec3 Direction = Normal * ((sin(Time) + 1.0) / 2.0) * Magnitude;
    return Position + vec4(Direction, 0.0);
}

void main()
{
    vec3 Normal = GetNormal();

    gl_Position = explode(gl_in[0].gl_Position, Normal);
    TexCoords = gs_in[0].TexCoords;
    EmitVertex();

    gl_Position = explode(gl_in[1].gl_Position, Normal);
    TexCoords = gs_in[1].TexCoords;
    EmitVertex();

    gl_Position = explode(gl_in[2].gl_Position, Normal);
    TexCoords = gs_in[2].TexCoords;
    EmitVertex();

    EndPrimitive();
}