#version 330 core
in vec3 Normal;
in vec3 Position;

uniform vec3 CameraPos;
uniform samplerCube Skybox;

out vec4 FragColor;

void main()
{
    float Ratio = 1.0 / 1.52;
    vec3 I = normalize(Position - CameraPos);
    vec3 R = refract(I, normalize(Normal), Ratio);
    FragColor = vec4(texture(Skybox, R).rgb, 1.0);
}