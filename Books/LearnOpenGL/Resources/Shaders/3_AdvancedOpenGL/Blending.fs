#version 330 core
in vec2 TexCoords;

uniform sampler2D Texture1;

out vec4 FragColor;

void main()
{
    FragColor = texture(Texture1, TexCoords);
}