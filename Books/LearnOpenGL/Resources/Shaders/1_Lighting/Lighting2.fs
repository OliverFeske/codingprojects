#version 330 core
out vec4 FragColor;

uniform vec3 LightColor;
uniform vec3 ObjectColor;
uniform vec3 LightPosition;
uniform vec3 ViewPosition;
uniform float AmbientStrength;
uniform float DiffuseStrength;
uniform float SpecularStrength;

in vec3 Normal;
in vec3 FragPosition;

void main()
{
	vec3 Ambient = AmbientStrength * LightColor;

	vec3 NormalizedNormal = normalize(Normal);
	vec3 LightDirection = normalize(LightPosition - FragPosition);
	float Diff = max(dot(NormalizedNormal, LightDirection), 0.0);
	vec3 Diffuse = Diff * LightColor * DiffuseStrength;

	vec3 ViewDirection = normalize(ViewPosition - FragPosition);
	vec3 ReflectDirection = reflect(-LightDirection, NormalizedNormal);
	float Spec = pow(max(dot(ViewDirection, ReflectDirection), 0.0), 32);
	vec3 Specular = SpecularStrength * Spec * LightColor;

	vec3 Result = (Ambient + Diffuse + Specular) * ObjectColor;
	FragColor = vec4(Result, 1.0);
}