#version 330 core
struct MaterialStruct {
	sampler2D Diffuse;
	sampler2D Specular;
	float Shininess;
};

struct LightStruct {
	vec3 Position;

	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;

	float Constant;
	float Linear;
	float Quadratic;
};

out vec4 FragColor;

uniform vec3 ViewPosition;
uniform MaterialStruct Material;
uniform LightStruct Light;

in vec3 Normal;
in vec3 FragPosition;
in vec2 TexCoords;

void main()
{
	float Distance = length(Light.Position - FragPosition);
	float Attenuation = 1.0 / (Light.Constant + Light.Linear * Distance + Light.Quadratic * (Distance * Distance));

	vec3 Ambient = Light.Ambient * vec3(texture(Material.Diffuse, TexCoords));

	vec3 NormalizedNormal = normalize(Normal);
	vec3 LightDirection = normalize(Light.Position - FragPosition);
	float Diff = max(dot(NormalizedNormal, LightDirection), 0.0);
	vec3 Diffuse = Light.Diffuse * Diff * vec3(texture(Material.Diffuse, TexCoords));

	vec3 ViewDirection = normalize(ViewPosition - FragPosition);
	vec3 ReflectDirection = reflect(-LightDirection, NormalizedNormal);
	float Spec = pow(max(dot(ViewDirection, ReflectDirection), 0.0), Material.Shininess);
	vec3 Specular = Spec * Light.Specular * vec3(texture(Material.Specular, TexCoords));

	vec3 Result = (Ambient + Diffuse + Specular) * Attenuation;
	FragColor = vec4(Result, 1.0);
}