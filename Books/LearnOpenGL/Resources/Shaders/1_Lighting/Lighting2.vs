#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

out vec3 FragPosition;
out vec3 Normal;

void main()
{
	gl_Position = Projection * View * Model * vec4(aPos, 1.0);
	FragPosition = vec3(Model * vec4(aPos, 1.0));
	Normal = mat3(transpose(inverse(Model))) * aNormal;
}