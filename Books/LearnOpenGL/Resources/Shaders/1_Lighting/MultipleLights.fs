#version 330 core

struct MaterialStruct {
	sampler2D Diffuse;
	sampler2D Specular;
	float Shininess;
};

struct DirectionalLightStruct {
	vec3 Direction;

	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;
};

struct PointLightStruct {
	vec3 Position;

	float Constant;
	float Linear;
	float Quadratic;

	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;
};
#define PointLightCount 4

struct SpotLightStruct {
	vec3 Position;
	vec3 Direction;
	float CutOff;
	float OuterCutOff;

	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;

	float Constant;
	float Linear;
	float Quadratic;
};

uniform vec3 ViewPosition;
uniform MaterialStruct Material;
uniform DirectionalLightStruct DirectionalLight;
uniform PointLightStruct PointLights[PointLightCount];
uniform SpotLightStruct SpotLight;

in vec3 Normal;
in vec3 FragPosition;
in vec2 TexCoords;

out vec4 FragColor;

vec3 CalculateDirectionalLight(DirectionalLightStruct Light, vec3 Norm, vec3 ViewDirection);
vec3 CalculatePointLight(PointLightStruct Light, vec3 Norm, vec3 FragPos, vec3 ViewDirection);
vec3 CalculateSpotLight(SpotLightStruct Light, vec3 Norm, vec3 FragPos, vec3 ViewDirection);

void main()
{
	vec3 NormalizedNormal = normalize(Normal);
	vec3 ViewDirection = normalize(ViewPosition - FragPosition);

	vec3 Output = CalculateDirectionalLight(DirectionalLight, NormalizedNormal, ViewDirection);

	for(int i = 0; i < PointLightCount; ++i) {
		Output += CalculatePointLight(PointLights[i], NormalizedNormal, FragPosition, ViewDirection);
	};

	Output += CalculateSpotLight(SpotLight, NormalizedNormal, FragPosition, ViewDirection);

	FragColor = vec4(Output, 1.0);
}

vec3 CalculateDirectionalLight(DirectionalLightStruct Light, vec3 Norm, vec3 ViewDirection) {
	vec3 LightDirection = normalize(-Light.Direction);

	float Diff = max(dot(Norm, LightDirection), 0.0);

	vec3 ReflectDirection = reflect(-LightDirection, Norm);
	float Spec = pow(max(dot(ViewDirection, ReflectDirection), 0.0), Material.Shininess);

	vec3 Ambient = Light.Ambient * vec3(texture(Material.Diffuse, TexCoords));
	vec3 Diffuse = Light.Diffuse * Diff * vec3(texture(Material.Diffuse, TexCoords));
	vec3 Specular = Light.Specular * Spec * vec3(texture(Material.Specular, TexCoords));

	return Ambient + Diffuse + Specular;
}

vec3 CalculatePointLight(PointLightStruct Light, vec3 Norm, vec3 FragPos, vec3 ViewDirection) {
	vec3 LightDirection = normalize(Light.Position - FragPos);

	float Diff = max(dot(Norm, LightDirection), 0.0);

	vec3 ReflectDirection = reflect(-LightDirection, Norm);
	float Spec = pow(max(dot(ViewDirection, ReflectDirection), 0.0), Material.Shininess);

	float Distance = length(Light.Position - FragPos);
	float Attenuation = 1.0 / (Light.Constant + Light.Linear * Distance + Light.Quadratic * (Distance * Distance));

	vec3 Ambient = Light.Ambient * vec3(texture(Material.Diffuse, TexCoords));
	vec3 Diffuse = Light.Diffuse * Diff * vec3(texture(Material.Diffuse, TexCoords));
	vec3 Specular = Light.Specular * Spec * vec3(texture(Material.Specular, TexCoords));

	return (Ambient + Diffuse + Specular) * Attenuation;
}

vec3 CalculateSpotLight(SpotLightStruct Light, vec3 Norm, vec3 FragPos, vec3 ViewDirection) {
	vec3 LightDirection = normalize(Light.Position - FragPos);

	float Diff = max(dot(Norm, LightDirection), 0.0);

	vec3 ReflectDirection = reflect(-LightDirection, Norm);
	float Spec = pow(max(dot(ViewDirection, ReflectDirection), 0.0), Material.Shininess);

	float Theta = dot(LightDirection, normalize(-Light.Direction));
	float Epsilon = Light.CutOff - Light.OuterCutOff;
	float Intensity = clamp((Theta - Light.OuterCutOff) / Epsilon, 0.0, 1.0);

	float Distance = length(Light.Position - FragPos);
	float Attenuation = 1.0 / (Light.Constant + Light.Linear * Distance + Light.Quadratic * (Distance * Distance));

	vec3 Ambient = Light.Ambient * vec3(texture(Material.Diffuse, TexCoords));
	vec3 Diffuse = Light.Diffuse * Diff * vec3(texture(Material.Diffuse, TexCoords));
	vec3 Specular = Light.Specular * Spec * vec3(texture(Material.Specular, TexCoords));

	return (Ambient + Diffuse * Intensity + Specular * Intensity) * Attenuation;
}