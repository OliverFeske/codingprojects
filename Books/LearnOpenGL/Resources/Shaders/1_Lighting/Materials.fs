#version 330 core
struct MaterialStruct {
	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;
	float Shininess;
};

struct LightStruct {
	vec3 Position;

	vec3 Ambient;
	vec3 Diffuse;
	vec3 Specular;
};

out vec4 FragColor;

uniform vec3 LightColor;
uniform vec3 ObjectColor;
uniform vec3 ViewPosition;
uniform MaterialStruct Material;
uniform LightStruct Light;

in vec3 Normal;
in vec3 FragPosition;

void main()
{
	vec3 Ambient = Light.Ambient * Material.Ambient;

	vec3 NormalizedNormal = normalize(Normal);
	vec3 LightDirection = normalize(Light.Position - FragPosition);
	float Diff = max(dot(NormalizedNormal, LightDirection), 0.0);
	vec3 Diffuse = Light.Diffuse * (Diff * Material.Diffuse);

	vec3 ViewDirection = normalize(ViewPosition - FragPosition);
	vec3 ReflectDirection = reflect(-LightDirection, NormalizedNormal);
	float Spec = pow(max(dot(ViewDirection, ReflectDirection), 0.0), Material.Shininess);
	vec3 Specular = Spec * (Material.Specular * Light.Specular);

	vec3 Result = Ambient + Diffuse + Specular;
	FragColor = vec4(Result, 1.0);
}