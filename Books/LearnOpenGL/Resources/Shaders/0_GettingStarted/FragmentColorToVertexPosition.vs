#version 330 core

layout (location = 0) in vec3 aPos; // position variable has attribute position 0

out vec4 CustomColor;

void main()
{
	gl_Position = vec4(aPos, 1.0);
	CustomColor = gl_Position;
}