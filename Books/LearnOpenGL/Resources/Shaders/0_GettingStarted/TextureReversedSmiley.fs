#version 330 core
out vec4 FragColor;

in vec3 CustomColor;
in vec2 TexCoord;

uniform sampler2D CustomTexture1;
uniform sampler2D CustomTexture2;

void main()
{
	FragColor = mix(texture(CustomTexture1, TexCoord), texture(CustomTexture2, vec2(1.0 - TexCoord.x, TexCoord.y)), 0.2);
}