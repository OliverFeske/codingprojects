#version 330 core
out vec4 FragColor;

in vec3 CustomColor;
in vec2 TexCoord;

uniform sampler2D CustomTexture1;
uniform sampler2D CustomTexture2;
uniform float MixStrength;

void main()
{
	FragColor = mix(texture(CustomTexture1, TexCoord), texture(CustomTexture2, TexCoord), MixStrength);
}