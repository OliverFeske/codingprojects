#include <iostream>

#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

#include "0_GettingStarted/Cameras.h"
#include "0_GettingStarted/CoordinateSystems.h"
#include "0_GettingStarted/HelloTriangle.h"
#include "0_GettingStarted/HelloWindow.h"
#include "0_GettingStarted/Shaders.h"
#include "0_GettingStarted/Textures.h"
#include "0_GettingStarted/Transformations.h"
#include "1_Lighting/LightCasters.h"
#include "1_Lighting/LightingMaps.h"
#include "1_Lighting/Colors.h"
#include "1_Lighting/Materials.h"
#include "1_Lighting/MultipleLights.h"
#include "1_Lighting/BasicLighting.h"
#include "2_ModelLoading/ModelLoading.h"
#include "3_AdvancedOpenGL/Blending.h"
#include "3_AdvancedOpenGL/Cubemaps.h"
#include "3_AdvancedOpenGL/DepthTesting.h"
#include "3_AdvancedOpenGL/StencilTesting.h"
#include "3_AdvancedOpenGL/FaceCulling.h"
#include "3_AdvancedOpenGL/Framebuffers.h"
#include "3_AdvancedOpenGL/GeometryShader.h"

int main(int argc, char** argv) try
{
    int result{0};
    //result = HelloWindow::run();
    //result = HelloTriangle::run(HelloTriangle::Version::TwoPrograms);
    //result = Shaders::run(Shaders::Version::Default);
    //result = Textures::run(Textures::Version::VisibilityUpDown);
    //result = Transformations::run(Transformations::Version::SecondContainer);
    //result = CoordinateSystems::run(CoordinateSystems::Version::RotatingContainers);
    //result = Cameras::run();
    //result = BasicLighting::run();
    //result = Materials::run();
    //result = LightingMaps::run();
    //result = LightCasters::run();
    //result = MultipleLights::run();
    //result = ModelLoading::run();
    //result = DepthTesting::run();
    //result = StencilTesting::run();
    //result = Blending::run();
    //result = FaceCulling::run();
    //result = Framebuffers::run();
    //result = Cubemaps::run();
    result = GeometryShader::run();

    return result;
}
catch (const std::exception& e)
{
    std::cerr << e.what() << "\n";
    return -1;
}
catch (...)
{
    std::cerr << "Unknown exception!" << "\n";
    return -1;
}
/*
	    glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
	    glm::mat4 Model = glm::mat4(1.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();

        CubeShader.Use();
        CubeShader.SetMat4("Model", Model);
        CubeShader.SetMat4("View", View);
        CubeShader.SetMat4("Projection", Projection);
        CubeShader.SetVec3("cameraPos", GCameraInstance.GetPosition());

        glBindVertexArray(CubeVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, SkyboxTexture);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        glDepthFunc(GL_LEQUAL);
        SkyboxShader.Use();
        View = glm::mat4(glm::mat3(GCameraInstance.GetViewMatrix()));
        SkyboxShader.SetMat4("View", View);
        SkyboxShader.SetMat4("Projection", Projection);

	    glBindVertexArray(SkyboxVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, SkyboxTexture);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);
        glDepthFunc(GL_LESS);
    */