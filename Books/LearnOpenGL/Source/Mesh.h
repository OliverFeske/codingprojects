#pragma once

#include <string>
#include <vector>

#include "glm/glm.hpp"

class Shader;

struct Vertex
{
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
};

struct Texture
{
    uint32_t ID;
    std::string Type;
    std::string Path;
};

class Mesh
{
public:
    Mesh(std::vector<Vertex> Vertices, std::vector<uint32_t> Indices, std::vector<Texture> Textures);

    std::vector<Vertex> Vertices;
    std::vector<uint32_t> Indices;
    std::vector<Texture> Textures;

    void Draw(Shader& Shader);

private:
    void SetupMesh();

    uint32_t VAO, VBO, EBO;
};

