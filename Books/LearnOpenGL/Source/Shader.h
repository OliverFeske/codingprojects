#pragma once

#include <string>

#include "glm/fwd.hpp"

class Shader
{
public:
	unsigned int ID;

	Shader(const std::string& VertexPath, const std::string& FragmentPath, const std::string& GeometryPath = "");

	void Use();
    
	void SetBool(const std::string& Name, bool Value) const;
	void SetInt(const std::string& Name, int Value) const;
	void SetFloat(const std::string& Name, float Value) const;
	void SetMat4(const std::string& Name, glm::mat4 Value) const;
	void SetVec3(const std::string& Name, glm::vec3 Value) const;
};

