#pragma once

#include <windows.h>
#include <string>
#include <iostream>
#include <vector>

#include "glad/glad.h"
#include "glfw/glfw3.h"

#include "Camera.h"
#include "stb/stb_image.h"

// Globals
static GLFWwindow* GWindow = nullptr;
static float GWidth = 1920.0f;
static float GHeight = 1080.0f;
static Camera GCameraInstance = Camera();
static float GDeltaTime = 0.0f;
static float GLastFrame = 0.0f;
static float GLastX = 990.0f;
static float GLastY = 540.0f;

static std::string GetExePath()
{
    auto GetPathInternal = []()
    {
        wchar_t buffer[MAX_PATH];
        GetModuleFileName(nullptr, buffer, MAX_PATH);
        std::wstring bufferWString{buffer};
        std::string bufferString{bufferWString.begin(), bufferWString.end()};

        while (!(bufferString.back() == '\\' || bufferString.back() == '/' || bufferString.empty()))
        {
            bufferString.pop_back();
        }

        for (char& c : bufferString)
        {
            if (c == '\\')
            {
                c = '/';
            }
        }

        return bufferString;
    };

    static std::string Path = GetPathInternal();

    return Path;
}

static std::string GetResourcePath()
{
    static std::string Path = GetExePath() + "Resources/";
    return Path;
}

static std::string GetShadersPath()
{
    static std::string Path = GetResourcePath() + "Shaders/";
    return Path;
}

static std::string GetTexturesPath()
{
    static std::string Path = GetResourcePath() + "Textures/";
    return Path;
}

static std::string GetModelsPath()
{
    static std::string Path = GetResourcePath() + "Models/";
    return Path;
}

static void FrameBufferSizeCallback(GLFWwindow* window, int width, int height)
{
    GWidth = (float)width;
    GHeight = (float)height;
    glViewport(0, 0, (int)GWidth, (int)GHeight);
}

static void ProcessInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        GCameraInstance.ProcessKeyboard(Camera::MoveDirection::Forward, GDeltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        GCameraInstance.ProcessKeyboard(Camera::MoveDirection::Backward, GDeltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        GCameraInstance.ProcessKeyboard(Camera::MoveDirection::Left, GDeltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        GCameraInstance.ProcessKeyboard(Camera::MoveDirection::Right, GDeltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
    {
        GCameraInstance.ProcessKeyboard(Camera::MoveDirection::Down, GDeltaTime);
    }
    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
    {
        GCameraInstance.ProcessKeyboard(Camera::MoveDirection::Up, GDeltaTime);
    }
}

static void MouseCallback(GLFWwindow* window, double XPos, double YPos)
{
    float XOffset = static_cast<float>(XPos) - GLastX;
    float YOffset = GLastY - static_cast<float>(YPos);
    GLastX = static_cast<float>(XPos);
    GLastY = static_cast<float>(YPos);
    GCameraInstance.ProcessMouseMovement(XOffset, YOffset);
}

static void ScrollCallback(GLFWwindow* window, double XOffset, double YOffset)
{
    GCameraInstance.ProcessMouseScroll(static_cast<float>(YOffset));
}

static int SetupDefaultGLFWWindow()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GWindow = glfwCreateWindow(1980, 1080, "LearnOpenGL", nullptr, nullptr);
    if (!GWindow)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent(GWindow);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback(GWindow, FrameBufferSizeCallback);
    glfwSetCursorPosCallback(GWindow, MouseCallback);
    glfwSetInputMode(GWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPos(GWindow, GLastX, GLastY);
    glfwSetScrollCallback(GWindow, ScrollCallback);
    glfwSwapInterval(0);

    glViewport(0, 0, (int)GWidth, (int)GHeight);

    return 0;
}

static void DefaultGLFWWindowLoopStart()
{
    static uint32_t FrameCounter = 0;
    static float SavedTime = 0.0f;

    float CurrentFrame = (float)glfwGetTime();
    GDeltaTime = CurrentFrame - GLastFrame;
    GLastFrame = CurrentFrame;

    ++FrameCounter;
    SavedTime += GDeltaTime;
    if (SavedTime > 1.0f)
    {
        glfwSetWindowTitle(GWindow, std::to_string(FrameCounter).c_str());
        SavedTime -= 1.0f;
        FrameCounter = 0;
    }

    ProcessInput(GWindow);

    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

static void DefaultGLFWWindowLoopEnd()
{
    glfwSwapBuffers(GWindow);
    glfwPollEvents();
}

static uint32_t LoadTexture(const std::string& Path)
{
    uint32_t TextureID;
    glGenTextures(1, &TextureID);

    int32_t Width, Height, NRComponents;
    unsigned char* Data = stbi_load(Path.c_str(), &Width, &Height, &NRComponents, 0);
    if (Data)
    {
        GLenum Format;
        if (NRComponents == 1)
        {
            Format = GL_RED;
        }
        else if (NRComponents == 3)
        {
            Format = GL_RGB;
        }
        else if (NRComponents == 4)
        {
            Format = GL_RGBA;
        }
        else
        {
            std::cerr << "Could not determine Texture Format!\n";
        }

        glBindTexture(GL_TEXTURE_2D, TextureID);
        glTexImage2D(GL_TEXTURE_2D, 0, Format, Width, Height, 0, Format, GL_UNSIGNED_BYTE, Data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else
    {
        std::cerr << "Could not load Texture: " << Path << "\n";
    }

    stbi_image_free(Data);

    return TextureID;
}

static uint32_t LoadCubeMap(const std::vector<std::string>& FacePaths)
{
    uint32_t TextureID;
	glGenTextures(1, &TextureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, TextureID);

    int Width, Height, NRChannels;
    for (uint32_t i = 0; i < FacePaths.size(); ++i)
    {
        unsigned char* Data = stbi_load(FacePaths[i].c_str(), &Width, &Height, &NRChannels, 0);
        if (Data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, Data);
        }
        else
        {
            std::cerr << "Cubemap tex failed to load path: " << FacePaths[i] << "\n";
        }

        stbi_image_free(Data);
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return TextureID;
}