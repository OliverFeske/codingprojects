#pragma once

#include "glm/glm.hpp"

class Camera final
{
public:
	enum class MoveDirection : char
    {
		Forward,
		Backward,
		Left,
		Right,
		Up,
		Down
    };

	Camera(glm::vec3 Position = glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3 Up = glm::vec3(0.0f, 1.0f, 0.0f), float Yaw = -90.0f, float Pitch = 0.0f);

	void Initialize(glm::vec3 InPosition = glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3 InUp = glm::vec3(0.0f, 1.0f, 0.0f), float InYaw = -90.0f, float InPitch = 0.0f);
    glm::mat4 GetViewMatrix() const;
	float GetFOV() const;
	glm::vec3 GetPosition() const;
	glm::vec3 GetFront() const;

    void ProcessKeyboard(MoveDirection Direction, float DeltaSeconds);
    void ProcessMouseMovement(float XOffset, float YOffset);
	void ProcessMouseScroll(float YOffset);

private:
	void UpdateCameraVectors();

	glm::vec3 Position;
	glm::vec3 Front;
	glm::vec3 Up;
	glm::vec3 Right;
	glm::vec3 WorldUp;

	float Yaw;
	float Pitch;

	float MovementSpeed;
	float MouseSensitivity;
	float FOV;
	float ZoomSpeed;
};

