#pragma once

#include <string>
#include <vector>

#include "Mesh.h"
#include "assimp/material.h"

struct aiMesh;
struct aiScene;
struct aiNode;

struct Texture;
class Shader;

class Model
{
public:
	Model(const std::string& Path);

	void Draw(Shader& Shader);

private:
	void LoadModel(const std::string& Path);
	void ProcessNode(aiNode* Node, const aiScene* Scene);
	Mesh ProcessMesh(aiMesh* InMesh, const aiScene* Scene);
	std::vector<Texture> LoadMaterialTextures(aiMaterial* Material, aiTextureType Type, std::string TypeName);

	std::vector<Mesh> Meshes;
	std::string Directory;
	std::vector<Texture> LoadedTextures;
};

