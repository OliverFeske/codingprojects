#include "Model.h"

#include <iostream>

#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "assimp/scene.h"

#include "Utilities.h"

Model::Model(const std::string& Path)
{
	LoadModel(Path);
}

void Model::Draw(Shader& Shader)
{
	for (uint32_t i = 0; i < Meshes.size(); ++i)
	{
	    Meshes[i].Draw(Shader);
	}
}

void Model::LoadModel(const std::string& Path)
{
	Assimp::Importer Import;
	const aiScene* Scene = Import.ReadFile(Path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if (!Scene || Scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !Scene->mRootNode)
	{
	    std::cerr << "ERROR:ASSIMP::" << Import.GetErrorString() << "\n";
		return;
	}

	Directory = Path.substr(0, Path.find_last_of('/'));

	ProcessNode(Scene->mRootNode, Scene);
}

void Model::ProcessNode(aiNode* Node, const aiScene* Scene)
{
	for (uint32_t i = 0; i < Node->mNumMeshes; ++i)
	{
	    aiMesh* Mesh = Scene->mMeshes[Node->mMeshes[i]];
		Meshes.push_back(ProcessMesh(Mesh, Scene));
	}

	for (uint32_t i = 0; i < Node->mNumChildren; ++i)
	{
	    ProcessNode(Node->mChildren[i], Scene);
	}
}

Mesh Model::ProcessMesh(aiMesh* InMesh, const aiScene* Scene)
{
	std::vector<Vertex> Vertices;
	std::vector<uint32_t> Indices;
	std::vector<Texture> Textures;

	for (uint32_t i = 0; i < InMesh->mNumVertices; ++i)
	{
		aiVector3D aiVertex = InMesh->mVertices[i];
		aiVector3D aiNormal = InMesh->mNormals[i];
		aiVector3D* aiTexCoordsPtr = InMesh->mTextureCoords[0];
		glm::vec2 aiTexCoords = glm::vec2(0.0f);
		if (aiTexCoordsPtr)
		{
			aiTexCoords = glm::vec2(aiTexCoordsPtr[i].x, aiTexCoordsPtr[i].y);
		}

	    Vertex Vertex;
		Vertex.Position = glm::vec3(aiVertex.x, aiVertex.y, aiVertex.z);
		Vertex.Normal = glm::vec3(aiNormal.x, aiNormal.y, aiNormal.z);
		Vertex.TexCoords = aiTexCoords;

		Vertices.push_back(Vertex);
	}

	for (uint32_t i = 0; i < InMesh->mNumFaces; ++i)
	{
	    aiFace Face = InMesh->mFaces[i];
		for (uint32_t j = 0; j < Face.mNumIndices; ++j)
		{
		    Indices.push_back(Face.mIndices[j]);
		}
	}

	if (InMesh->mMaterialIndex >= 0)
	{
	    aiMaterial* Material = Scene->mMaterials[InMesh->mMaterialIndex];

		std::vector<Texture> DiffuseMaps = LoadMaterialTextures(Material, aiTextureType_DIFFUSE, "TextureDiffuse");
		Textures.insert(Textures.end(), DiffuseMaps.begin(), DiffuseMaps.end());

		std::vector<Texture> SpecularMaps = LoadMaterialTextures(Material, aiTextureType_SPECULAR, "TextureSpecular");
		Textures.insert(Textures.end(), SpecularMaps.begin(), SpecularMaps.end());
	}

	return Mesh{Vertices, Indices, Textures};
}

std::vector<Texture> Model::LoadMaterialTextures(aiMaterial* Material, aiTextureType Type, std::string TypeName)
{
	std::vector<Texture> Textures;

	for (uint32_t i = 0; i < Material->GetTextureCount(Type); ++i)
	{
	    aiString String;
		Material->GetTexture(Type, i, &String);

		bool Skip = false;
		for (uint32_t j = 0; j < LoadedTextures.size(); ++j)
		{
		    if (std::strcmp(LoadedTextures[j].Path.c_str(), String.C_Str()) == 0)
		    {
		        Textures.push_back(LoadedTextures[j]);
				Skip = true;
				break;
		    }
		}

		if (!Skip)
		{
			Texture Texture;
			Texture.ID = LoadTexture(Directory + '/' + String.C_Str());
			Texture.Type = TypeName;
			Texture.Path = String.C_Str();
			Textures.push_back(Texture);
			LoadedTextures.push_back(Texture);
		}
	}

	return Textures;
}
