#include "LightingMaps.h"

#include "glad/glad.h"
#include "glfw/glfw3.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Utilities.h"
#include "Shader.h"

float LightingMaps::Vertices[288] = {
    // positions          // normals           // texture coords
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
};

glm::vec3 LightingMaps::LightPosition = glm::vec3(1.2f, 1.0f, 2.0f);

int LightingMaps::run()
{
    SetupDefaultGLFWWindow();

    GCameraInstance.Initialize(glm::vec3(-1.17f, 2.32f, 2.95f), glm::vec3(0.0f, 1.0f, 0.0f), -52.7f, -37.0f);

    Shader CubeShader{GetShadersPath() + "1_Lighting/LightingMaps.vs", GetShadersPath() + "1_Lighting/LightingMaps.fs"};
    Shader LightSourceShader{GetShadersPath() + "1_Lighting/LightSource2.vs" , GetShadersPath() + "1_Lighting/LightSource2.fs"};

    const uint32_t DiffuseMap = LoadTexture(GetTexturesPath() + "Container2.png");
    const uint32_t SpecularMap = LoadTexture(GetTexturesPath() + "Container2_specular.png");
    const uint32_t EmissionMap = LoadTexture(GetTexturesPath() + "Matrix.jpg");

    uint32_t CubeVAO, VBO;
    glGenVertexArrays(1, &CubeVAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(CubeVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    uint32_t LightVAO;
    glGenVertexArrays(1, &LightVAO);

    glBindVertexArray(LightVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    CubeShader.Use();
    CubeShader.SetVec3("ObjectColor", glm::vec3(1.0f, 0.5f, 0.31f));
    CubeShader.SetVec3("LightColor", glm::vec3(1.0f, 1.0f, 1.0f));
    CubeShader.SetInt("Material.Diffuse", 0);
    CubeShader.SetInt("Material.Specular", 1);
    CubeShader.SetInt("Material.Emission", 2);
    CubeShader.SetFloat("Material.EmissiveStrength", 1.0f);
    CubeShader.SetFloat("Material.Shininess", 32.0f);
    CubeShader.SetVec3("Light.Position", LightPosition);
    CubeShader.SetVec3("Light.Specular", glm::vec3(1.0f));
    CubeShader.SetVec3("Light.Ambient", glm::vec3(0.2f));
    CubeShader.SetVec3("Light.Diffuse", glm::vec3(0.5f));

    while (!glfwWindowShouldClose(GWindow))
    {
        DefaultGLFWWindowLoopStart();

        glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();
        glm::mat4 Model = glm::mat4(1.0f);
        CubeShader.Use();
        CubeShader.SetMat4("Projection", Projection);
        CubeShader.SetMat4("View", View);
        CubeShader.SetMat4("Model", Model);
        CubeShader.SetVec3("ViewPosition", GCameraInstance.GetPosition());

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, DiffuseMap);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, SpecularMap);

        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, EmissionMap);

        glBindVertexArray(CubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        Model = glm::translate(Model, LightPosition);
        Model = glm::scale(Model, glm::vec3(0.2f));
        LightSourceShader.Use();
        LightSourceShader.SetMat4("Projection", Projection);
        LightSourceShader.SetMat4("View", View);
        LightSourceShader.SetMat4("Model", Model);

        glBindVertexArray(LightVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        DefaultGLFWWindowLoopEnd();
    }

    return 0;
}
