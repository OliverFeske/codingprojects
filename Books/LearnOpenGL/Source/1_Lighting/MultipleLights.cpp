#include "MultipleLights.h"

#include "glad/glad.h"
#include "glfw/glfw3.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Utilities.h"
#include "Shader.h"

float MultipleLights::Vertices[288] = {
    // positions          // normals           // texture coords
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
};

glm::vec3 MultipleLights::CubePositions[10] = {
    glm::vec3( 0.0f,  0.0f,  0.0f),
    glm::vec3( 2.0f,  5.0f, -15.0f),
    glm::vec3(-1.5f, -2.2f, -2.5f),
    glm::vec3(-3.8f, -2.0f, -12.3f),
    glm::vec3( 2.4f, -0.4f, -3.5f),
    glm::vec3(-1.7f,  3.0f, -7.5f),
    glm::vec3( 1.3f, -2.0f, -2.5f),
    glm::vec3( 1.5f,  2.0f, -2.5f),
    glm::vec3( 1.5f,  0.2f, -1.5f),
    glm::vec3(-1.3f,  1.0f, -1.5f)
};

glm::vec3 MultipleLights::PointLightPositions[4] = {
	glm::vec3( 0.7f,  0.2f,  2.0f),
	glm::vec3( 2.3f, -3.3f, -4.0f),
	glm::vec3(-4.0f,  2.0f, -12.0f),
	glm::vec3( 0.0f,  0.0f, -3.0f)
};

int MultipleLights::run()
{
    SetupDefaultGLFWWindow();

    GCameraInstance.Initialize(glm::vec3(0.0f, 0.0f, 3.0f));

    Shader CubeShader{GetShadersPath() + "1_Lighting/MultipleLights.vs", GetShadersPath() + "1_Lighting/MultipleLights.fs"};
    Shader LightSourceShader{GetShadersPath() + "1_Lighting/LightSource2.vs" , GetShadersPath() + "1_Lighting/LightSource2.fs"};

    const uint32_t DiffuseMap = LoadTexture(GetTexturesPath() + "Container2.png");
    const uint32_t SpecularMap = LoadTexture(GetTexturesPath() + "Container2_specular.png");

    uint32_t CubeVAO, VBO;
    glGenVertexArrays(1, &CubeVAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(CubeVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    uint32_t LightVAO;
    glGenVertexArrays(1, &LightVAO);

    glBindVertexArray(LightVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    CubeShader.Use();
    CubeShader.SetInt("Material.Diffuse", 0);
    CubeShader.SetInt("Material.Specular", 1);
    CubeShader.SetFloat("Material.Shininess", 32.0f);

    // Directional Light
    CubeShader.SetVec3("DirectionalLight.Direction", glm::vec3(-0.2f, -1.0f, -0.3f));
    CubeShader.SetVec3("DirectionalLight.Ambient", glm::vec3(0.05f));
    CubeShader.SetVec3("DirectionalLight.Diffuse", glm::vec3(0.4f));
    CubeShader.SetVec3("DirectionalLight.Specular", glm::vec3(0.5f));

    // Point Lights
    for (uint32_t i = 0; i < 4; ++i)
    {
        std::string PointLightString = "PointLights[" + std::to_string(i) + "].";
        CubeShader.SetVec3(PointLightString + "Position", PointLightPositions[i]);
        CubeShader.SetVec3(PointLightString + "Ambient", glm::vec3(0.05f));
        CubeShader.SetVec3(PointLightString + "Diffuse", glm::vec3(0.8f));
        CubeShader.SetVec3(PointLightString + "Specular", glm::vec3(1.0f));
        CubeShader.SetFloat(PointLightString + "Constant", 1.0f);
        CubeShader.SetFloat(PointLightString + "Linear", 0.09f);
        CubeShader.SetFloat(PointLightString + "Quadratic", 0.032f);
    }

    // Spotlight
    CubeShader.SetFloat("SpotLight.CutOff", glm::cos(glm::radians(12.5f)));
    CubeShader.SetFloat("SpotLight.OuterCutOff", glm::cos(glm::radians(17.5f)));

    CubeShader.SetVec3("SpotLight.Ambient", glm::vec3(0.0f));
    CubeShader.SetVec3("SpotLight.Diffuse", glm::vec3(1.0f));
    CubeShader.SetVec3("SpotLight.Specular", glm::vec3(1.0f));

    CubeShader.SetFloat("SpotLight.Constant", 1.0f);
    CubeShader.SetFloat("SpotLight.Linear", 0.09f);
    CubeShader.SetFloat("SpotLight.Quadratic", 0.032f);

    while (!glfwWindowShouldClose(GWindow))
    {
        DefaultGLFWWindowLoopStart();

        glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, DiffuseMap);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, SpecularMap);

        glBindVertexArray(CubeVAO);

        CubeShader.Use();
        CubeShader.SetVec3("SpotLight.Position", GCameraInstance.GetPosition());
        CubeShader.SetVec3("SpotLight.Direction", GCameraInstance.GetFront());

        CubeShader.SetMat4("Projection", Projection);
        CubeShader.SetMat4("View", View);
        CubeShader.SetVec3("ViewPosition", GCameraInstance.GetPosition());

        glm::mat4 Model = glm::mat4(1.0f);
        float Angle = 0.0f;
        for (uint32_t i = 0; i < 10; ++i)
        {
            Angle = 20.0f * (float)i;
            Model = glm::mat4(1.0f);
            Model = glm::translate(Model, CubePositions[i]);
            Model = glm::rotate(Model, glm::radians(Angle), glm::vec3(1.0f, 0.3f, 0.5f));
            CubeShader.SetMat4("Model", Model);
            
            glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        LightSourceShader.Use();
        LightSourceShader.SetMat4("Projection", Projection);
        LightSourceShader.SetMat4("View", View);

        glBindVertexArray(LightVAO);
        for (uint32_t i = 0; i < 4; ++i)
        {
            Model = glm::mat4(1.0f);
            Model = glm::translate(Model, PointLightPositions[i]);
            Model = glm::scale(Model, glm::vec3(0.2f));
            LightSourceShader.SetMat4("Model", Model);

            glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        DefaultGLFWWindowLoopEnd();
    }

    return 0;
}
