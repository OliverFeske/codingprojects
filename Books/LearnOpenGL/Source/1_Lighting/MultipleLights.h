#pragma once

#include "glm/fwd.hpp"

class MultipleLights
{
public:
	static int run();
	
private:
	static float Vertices[288];
	static glm::vec3 CubePositions[10];
	static glm::vec3 PointLightPositions[4];
};

