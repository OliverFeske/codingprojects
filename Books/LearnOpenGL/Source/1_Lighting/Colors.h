#pragma once

#include "glm/fwd.hpp"

class Colors
{
public:
	static int run();

private:
	static float Vertices[108];
	static glm::vec3 LightPosition;
};

