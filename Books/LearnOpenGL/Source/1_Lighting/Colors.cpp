#include "Colors.h"
#include "Utilities.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Shader.h"

glm::vec3 Colors::LightPosition = glm::vec3(1.2f, 1.0f, 2.0f);

float Colors::Vertices[108] = {
    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    -0.5f, 0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,

    -0.5f, -0.5f, 0.5f,
    0.5f, -0.5f, 0.5f,
    0.5f, 0.5f, 0.5f,
    0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,
    -0.5f, -0.5f, 0.5f,

    -0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,
    -0.5f, -0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,

    0.5f, 0.5f, 0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, 0.5f,
    0.5f, 0.5f, 0.5f,

    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, 0.5f,
    0.5f, -0.5f, 0.5f,
    -0.5f, -0.5f, 0.5f,
    -0.5f, -0.5f, -0.5f,

    -0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, 0.5f,
    0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, -0.5f
};

int Colors::run()
{
    SetupDefaultGLFWWindow();

    GCameraInstance.Initialize(glm::vec3(-1.17f, 2.32f, 2.95f), glm::vec3(0.0f, 1.0f, 0.0f), -52.7f, -37.0f);

    Shader LightingShader{GetShadersPath() + "1_Lighting/Lighting.vs", GetShadersPath() + "1_Lighting/Lighting.fs"};
    Shader LightSourceShader{GetShadersPath() + "1_Lighting/LightSource.vs", GetShadersPath() + "1_Lighting/LightSource.fs"};

    uint32_t CubeVAO, VBO;
    glGenVertexArrays(1, &CubeVAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(CubeVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    uint32_t LightSourceVAO;
    glGenVertexArrays(1, &LightSourceVAO);

    glBindVertexArray(LightSourceVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    LightingShader.Use();
    LightingShader.SetVec3("ObjectColor", glm::vec3(1.0f, 0.5f, 0.31f));
    LightingShader.SetVec3("LightColor", glm::vec3(1.0f, 1.0f, 1.0f));

    while (!glfwWindowShouldClose(GWindow))
    {
        DefaultGLFWWindowLoopStart();

        glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();
        glm::mat4 Model = glm::mat4(1.0f);
        LightingShader.Use();
        LightingShader.SetMat4("Projection", Projection);
        LightingShader.SetMat4("View", View);
        LightingShader.SetMat4("Model", Model);

        glBindVertexArray(CubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        Model = glm::translate(Model, LightPosition);
        Model = glm::scale(Model, glm::vec3(0.2f));
        LightSourceShader.Use();
        LightSourceShader.SetMat4("Projection", Projection);
        LightSourceShader.SetMat4("View", View);
        LightSourceShader.SetMat4("Model", Model);

        glBindVertexArray(LightSourceVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        DefaultGLFWWindowLoopEnd();
    }

    glfwTerminate();

    return 0;
}
