#include "BasicLighting.h"
#include "Utilities.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Shader.h"

float BasicLighting::Vertices[216] = {
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
     0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
     0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 

    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
     0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
     0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
};

int BasicLighting::run()
{
    SetupDefaultGLFWWindow();

    GCameraInstance.Initialize(glm::vec3(-1.17f, 2.32f, 2.95f), glm::vec3(0.0f, 1.0f, 0.0f), -52.7f, -37.0f);

    Shader CubeShader{GetShadersPath() + "1_Lighting/Lighting2.vs", GetShadersPath() + "1_Lighting/Lighting2.fs"};
    Shader LightSourceShader{GetShadersPath() + "1_Lighting/LightSource2.vs" , GetShadersPath() + "1_Lighting/LightSource2.fs"};

    uint32_t CubeVAO, VBO;
    glGenVertexArrays(1, &CubeVAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(CubeVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    uint32_t LightVAO;
    glGenVertexArrays(1, &LightVAO);

    glBindVertexArray(LightVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    CubeShader.Use();
    CubeShader.SetVec3("ObjectColor", glm::vec3(1.0f, 0.5f, 0.31f));
    CubeShader.SetVec3("LightColor", glm::vec3(1.0f, 1.0f, 1.0f));
    CubeShader.SetFloat("AmbientStrength", 0.1f);
    CubeShader.SetFloat("DiffuseStrength", 1.0f);

    while (!glfwWindowShouldClose(GWindow))
    {
        DefaultGLFWWindowLoopStart();

        glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();
        glm::mat4 Model = glm::mat4(1.0f);
        float Time = (float)glfwGetTime();
        glm::vec3 LightPosition = glm::vec3(sin(Time * 1.4f) * 1.5f, cos(Time * 1.5f) * 1.5f, cos(Time * 1.6f)) * 1.5f;
        CubeShader.Use();
        CubeShader.SetMat4("Projection", Projection);
        CubeShader.SetMat4("View", View);
        CubeShader.SetMat4("Model", Model);
        CubeShader.SetVec3("ViewPosition", GCameraInstance.GetPosition());
        CubeShader.SetVec3("LightPosition", LightPosition);
        //CubeShader.SetFloat("AmbientStrength", sin(Time));
        //CubeShader.SetFloat("DiffuseStrength", sin(Time));
        CubeShader.SetFloat("SpecularStrength", sin(Time) * 20.0f);

        glBindVertexArray(CubeVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        Model = glm::translate(Model, LightPosition);
        Model = glm::scale(Model, glm::vec3(0.2f));
        LightSourceShader.Use();
        LightSourceShader.SetMat4("Projection", Projection);
        LightSourceShader.SetMat4("View", View);
        LightSourceShader.SetMat4("Model", Model);

        glBindVertexArray(LightVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);

        DefaultGLFWWindowLoopEnd();
    }

    return 0;
}
