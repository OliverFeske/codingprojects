#pragma once

#include "glm/fwd.hpp"

class LightingMaps
{
public:
	static int run();

private:
	static float Vertices[288];
	static glm::vec3 LightPosition;
};

