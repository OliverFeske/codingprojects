#pragma once

#include "glm/fwd.hpp"

class Materials
{
public:
	static int run();

private:
    static float Vertices[216];
    static glm::vec3 LightPosition;
};

