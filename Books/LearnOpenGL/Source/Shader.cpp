#include "Shader.h"

#include "glad/glad.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include "glm/gtc/type_ptr.hpp"

Shader::Shader(const std::string& VertexPath, const std::string& FragmentPath, const std::string& GeometryPath)
{
	// Read Shaders
	std::string VertexShaderCode, FragmentShaderCode;
	std::ifstream VertexShaderFile{VertexPath}, FragmentShaderFile{FragmentPath};
	if (VertexShaderFile.is_open())
	{
		std::stringstream Buffer;
		Buffer << VertexShaderFile.rdbuf();
		VertexShaderCode = Buffer.str();
	}
	else
	{
	    std::cerr << "ERROR:SHADER:VERTEX:COULD_NOT_READ_FILE\n" << VertexPath << "\n";
	}

	if (FragmentShaderFile.is_open())
	{
		std::stringstream Buffer;
		Buffer << FragmentShaderFile.rdbuf();
		FragmentShaderCode = Buffer.str();
	}
	else
	{
	    std::cerr << "ERROR:SHADER:FRAGMENT:COULD_NOT_READ_FILE\n" << FragmentPath << "\n";
	}

	const char* VertexShaderSource = VertexShaderCode.c_str();

	// Compile Shaders
	uint32_t VertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(VertexShader, 1, &VertexShaderSource, nullptr);
	glCompileShader(VertexShader);

	int Success;
	char InfoLog[512];
	glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &Success);
	if (!Success)
	{
	    glGetShaderInfoLog(VertexShader, 512, nullptr, InfoLog);
		std::cerr << "ERROR:SHADER:VERTEX:COMPILATION_FAILED\n" << VertexPath << "\n" << InfoLog << "\n";
	}

	const char* FragmentShaderSource = FragmentShaderCode.c_str();
	uint32_t FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(FragmentShader, 1, &FragmentShaderSource, nullptr);
	glCompileShader(FragmentShader);

	glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &Success);
	if (!Success)
	{
		glGetShaderInfoLog(FragmentShader, 512, nullptr, InfoLog);
	    std::cerr << "ERROR:SHADER:FRAGMENT:COMPILATION_FAILED\n" << FragmentPath << "\n" << InfoLog << "\n";
	}

	// Create Shader program
	ID = glCreateProgram();

	uint32_t GeometryShader = 0;
	// Attach GeometryShader if available
	if(!GeometryPath.empty())
	{
		std::string GeometryShaderCode;
	    std::ifstream GeometryShaderFile{GeometryPath};
	    if (GeometryShaderFile.is_open())
		{
		    std::stringstream Buffer;
			Buffer << GeometryShaderFile.rdbuf();
			GeometryShaderCode = Buffer.str();
		}
		else
		{
		    std::cerr << "ERROR:SHADER:GEOMETRY:COULD_NOT_READ_FILE\n" << GeometryPath << "\n";
		}

		const char* GeometryShaderSource = GeometryShaderCode.c_str();
	    GeometryShader = glCreateShader(GL_GEOMETRY_SHADER);
		glShaderSource(GeometryShader, 1, &GeometryShaderSource, nullptr);
		glCompileShader(GeometryShader);

		glGetShaderiv(GeometryShader, GL_COMPILE_STATUS, &Success);
		if (!Success)
		{
		    glGetShaderInfoLog(GeometryShader, 512, nullptr, InfoLog);
			std::cerr << "ERROR:SHADER:GEOMETRY:COMPILATION_FAILED\n" << GeometryPath << "\n" << InfoLog << "\n";
		}
		glAttachShader(ID, GeometryShader);
	}

	// Link Program
	glAttachShader(ID, VertexShader);
	glAttachShader(ID, FragmentShader);
	glLinkProgram(ID);

	glGetProgramiv(ID, GL_LINK_STATUS, &Success);
	if (!Success)
	{
	    glGetProgramInfoLog(ID, 512, nullptr, InfoLog);
		std::cerr << "ERROR:SHADER:PROGRAM:LINK_FAILED\n" << InfoLog << "\n";
	}

	glDeleteShader(VertexShader);
	glDeleteShader(FragmentShader);
	if(!GeometryPath.empty())
	{
	    glDeleteShader(GeometryShader);
	}
}

void Shader::Use()
{
	glUseProgram(ID);
}

void Shader::SetBool(const std::string& Name, bool Value) const
{
	glUniform1i(glGetUniformLocation(ID, Name.c_str()), (int)Value);
}

void Shader::SetInt(const std::string& Name, int Value) const
{
	glUniform1i(glGetUniformLocation(ID, Name.c_str()), Value);
}

void Shader::SetFloat(const std::string& Name, float Value) const
{
	glUniform1f(glGetUniformLocation(ID, Name.c_str()), Value);
}

void Shader::SetMat4(const std::string& Name, glm::mat4 Value) const
{
	auto loc = glGetUniformLocation(ID, Name.c_str());
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(Value));
}

void Shader::SetVec3(const std::string& Name, glm::vec3 Value) const
{
    glUniform3fv(glGetUniformLocation(ID, Name.c_str()), 1, glm::value_ptr(Value));
}