#include "StencilTesting.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Model.h"
#include "Shader.h"
#include "Utilities.h"

int StencilTesting::run()
{
    SetupDefaultGLFWWindow();

	stbi_set_flip_vertically_on_load(true);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	Shader BackpackShader{GetShadersPath() + "2_ModelLoading/Backpack.vs", GetShadersPath() + "2_ModelLoading/Backpack.fs"};
	Shader StencilShader{GetShadersPath() + "3_AdvancedOpenGL/Stencil.vs", GetShadersPath() + "3_AdvancedOpenGL/Stencil.fs"};

	Model Backpack{GetModelsPath() + "backpack/backpack.obj"};

	while (!glfwWindowShouldClose(GWindow))
	{
	    DefaultGLFWWindowLoopStart();

		glClear(GL_STENCIL_BUFFER_BIT);

		glStencilFunc(GL_ALWAYS, 1, 0xFF);
		glStencilMask(0xFF);

		BackpackShader.Use();

        glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();
        BackpackShader.SetMat4("Projection", Projection);
        BackpackShader.SetMat4("View", View);

        glm::mat4 Model = glm::mat4(1.0f);
        Model = glm::translate(Model, glm::vec3(0.0f));
        Model = glm::scale(Model, glm::vec3(1.0f));
        BackpackShader.SetMat4("Model", Model);
        Backpack.Draw(BackpackShader);

		glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
		glStencilMask(0x00);
		glDisable(GL_DEPTH_TEST);

		StencilShader.Use();

        Model = glm::scale(Model, glm::vec3(1.0f));
	    StencilShader.SetMat4("Projection", Projection);
        StencilShader.SetMat4("View", View);
		StencilShader.SetMat4("Model", Model);
		Backpack.Draw(StencilShader);

	    glStencilFunc(GL_ALWAYS, 1, 0xFF);
		glStencilMask(0xFF);
		glEnable(GL_DEPTH_TEST);

		DefaultGLFWWindowLoopEnd();
	}

	glfwTerminate();

	return 0;
}
