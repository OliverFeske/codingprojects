#include "GeometryShader.h"

#include "glm/gtc/matrix_transform.hpp"

#include "Model.h"
#include "Shader.h"
#include "Utilities.h"

float GeometryShader::Points[20] = {
	-0.5f,  0.5f, 1.0f, 0.0f, 0.0f,	// top left
	 0.5f,  0.5f, 0.0f, 1.0f, 0.0f,	// top right
	 0.5f, -0.5f, 0.0f, 0.0f, 1.0f,	// bottom right
	-0.5f, -0.5f, 1.0f, 1.0f, 0.0f	// bottom left
};

int GeometryShader::run()
{
	SetupDefaultGLFWWindow();

	glEnable(GL_DEPTH_TEST);

	//Shader HousesShader{GetShadersPath() + "3_AdvancedOpenGL/Houses.vs", GetShadersPath() + "3_AdvancedOpenGL/Houses.fs", GetShadersPath() + "3_AdvancedOpenGL/Houses.gs"};
    Shader ExplodingShader{GetShadersPath() + "3_AdvancedOpenGL/Exploding.vs", GetShadersPath() + "3_AdvancedOpenGL/Exploding.fs", GetShadersPath() + "3_AdvancedOpenGL/Exploding.gs"};
	Model Backpack{GetModelsPath() + "backpack/backpack.obj"};

	uint32_t VAO, VBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Points), &Points, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
	glBindVertexArray(0);

	while (!glfwWindowShouldClose(GWindow))
	{
	    DefaultGLFWWindowLoopStart();

		/*
		HousesShader.Use();
		glBindVertexArray(VAO);
		glDrawArrays(GL_POINTS, 0, 4);
        */

		ExplodingShader.Use();
	    glm::mat4 projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 view = GCameraInstance.GetViewMatrix();
        glm::mat4 Model = glm::mat4(1.0f);
        ExplodingShader.SetMat4("Projection", projection);
        ExplodingShader.SetMat4("View", view);
        ExplodingShader.SetMat4("Model", Model);

		ExplodingShader.SetFloat("Time", glfwGetTime());
		Backpack.Draw(ExplodingShader);

		DefaultGLFWWindowLoopEnd();
	}


	return 0;
}
