#pragma once

#include <vector>

#include "glm/fwd.hpp"

class Blending
{
public:
	static int run();

private:
	static float CubeVertices[];
	static float PlaneVertices[];
	static float TransparentVertices[];
	static std::vector<glm::vec3> Windows;
};
