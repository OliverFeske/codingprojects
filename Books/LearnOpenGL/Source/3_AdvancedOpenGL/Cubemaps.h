#pragma once
class Cubemaps
{
public:
	static int run();

private:
	static float CubeVertices[216];
	static float SkyboxVertices[108];
};

