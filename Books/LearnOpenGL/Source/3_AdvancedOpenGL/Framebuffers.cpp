#include "Framebuffers.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Shader.h"
#include "Utilities.h"

float Framebuffers::CubeVertices[180] = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
    
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

float Framebuffers::PlaneVertices[30] = {
     5.0f, -0.5f,  5.0f,  2.0f, 0.0f,
    -5.0f, -0.5f,  5.0f,  0.0f, 0.0f,
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f,

     5.0f, -0.5f,  5.0f,  2.0f, 0.0f,
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f,
     5.0f, -0.5f, -5.0f,  2.0f, 2.0f
};

float Framebuffers::FrameBufferVertices[24] = {
    // Position     // Texture Coords
    -1.0f,  1.0f,   0.0f, 1.0f,         // top left
    -1.0f, -1.0f,   0.0f, 0.0f,         // bottom left
     1.0f, -1.0f,   1.0f, 0.0f,         // bottom right

    -1.0f,  1.0f,   0.0f, 1.0f,         // top left
     1.0f, -1.0f,   1.0f, 0.0f,         // bottom right
     1.0f,  1.0f,   1.0f, 1.0f          // top right
};

int Framebuffers::run()
{
    int Result = SetupDefaultGLFWWindow();
    if (Result != 0)
    {
        return Result;
    }

    glEnable(GL_DEPTH_TEST);

    // Shader setup
    Shader TextureShader{GetShadersPath() + "3_AdvancedOpenGL/Blending.vs", GetShadersPath() + "3_AdvancedOpenGL/Blending.fs"};
    //Shader FramebufferShader{GetShadersPath() + "3_AdvancedOpenGL/Framebuffer.vs", GetShadersPath() + "3_AdvancedOpenGL/Framebuffer.fs"};
    //Shader FramebufferShader{GetShadersPath() + "3_AdvancedOpenGL/Framebuffer.vs", GetShadersPath() + "3_AdvancedOpenGL/FramebufferInverted.fs"};
    //Shader FramebufferShader{GetShadersPath() + "3_AdvancedOpenGL/Framebuffer.vs", GetShadersPath() + "3_AdvancedOpenGL/FramebufferGrayscale.fs"};
    //Shader FramebufferShader{GetShadersPath() + "3_AdvancedOpenGL/Framebuffer.vs", GetShadersPath() + "3_AdvancedOpenGL/FramebufferKernelSharpen.fs"};
    //Shader FramebufferShader{GetShadersPath() + "3_AdvancedOpenGL/Framebuffer.vs", GetShadersPath() + "3_AdvancedOpenGL/FramebufferKernelBlur.fs"};
    Shader FramebufferShader{GetShadersPath() + "3_AdvancedOpenGL/Framebuffer.vs", GetShadersPath() + "3_AdvancedOpenGL/FramebufferKernelEdgeDetection.fs"};

    // VAO Setup
    uint32_t CubeVAO, CubeVBO;
    glGenVertexArrays(1, &CubeVAO);
    glGenBuffers(1, &CubeVBO);
    glBindVertexArray(CubeVAO);
    glBindBuffer(GL_ARRAY_BUFFER, CubeVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(CubeVertices), &CubeVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    uint32_t PlaneVAO, PlaneVBO;
    glGenVertexArrays(1, &PlaneVAO);
    glGenBuffers(1, &PlaneVBO);
    glBindVertexArray(PlaneVAO);
    glBindBuffer(GL_ARRAY_BUFFER, PlaneVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(PlaneVertices), &PlaneVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    uint32_t FramebufferVAO, FramebufferVBO;
    glGenVertexArrays(1, &FramebufferVAO);
    glGenBuffers(1, &FramebufferVBO);
    glBindVertexArray(FramebufferVAO);
    glBindBuffer(GL_ARRAY_BUFFER, FramebufferVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(FrameBufferVertices), &FrameBufferVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

    // Framebuffer setup
    uint32_t FBO;
    glGenFramebuffers(1, &FBO);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);

    uint32_t TextureColorBuffer;
    glGenTextures(1, &TextureColorBuffer);
    glBindTexture(GL_TEXTURE_2D, TextureColorBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, (int)GWidth, (int)GHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, TextureColorBuffer, 0);

    uint32_t RBO;
    glGenRenderbuffers(1, &RBO);
    glBindRenderbuffer(GL_RENDERBUFFER, RBO);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, (int)GWidth, (int)GHeight);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        std::cerr << "ERROR:FRAMEBUFFER:NOT_COMPLETED\n";
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Texture setup
    uint32_t CubeTexture = LoadTexture(GetTexturesPath() + "Container.jpg");
    uint32_t FloorTexture = LoadTexture(GetTexturesPath() + "marble.jpg");

    TextureShader.Use();
    TextureShader.SetInt("Texture1", 0);

    FramebufferShader.Use();
    FramebufferShader.SetInt("Texture", 0);

    // Force update the Window size to update framebuffer to correct size!
    glfwSetWindowSize(GWindow, GWidth, GHeight);

    while (!glfwWindowShouldClose(GWindow))
    {
        DefaultGLFWWindowLoopStart();

        glBindFramebuffer(GL_FRAMEBUFFER, FBO);
        glEnable(GL_DEPTH_TEST);

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        TextureShader.Use();

        glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();
        TextureShader.SetMat4("Projection", Projection);
        TextureShader.SetMat4("View", View);

        glBindVertexArray(CubeVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, CubeTexture);
        glm::mat4 Model = glm::mat4(1.0f);
        Model = glm::translate(Model, glm::vec3(-1.0f, 0.0f, -1.0f));
        TextureShader.SetMat4("Model", Model);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        Model = glm::mat4(1.0f);
        Model = glm::translate(Model, glm::vec3(2.0f, 0.0f, 0.0f));
        TextureShader.SetMat4("Model", Model);
	    glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(PlaneVAO);
        glBindTexture(GL_TEXTURE_2D, FloorTexture);
        Model = glm::mat4(1.0f);
        TextureShader.SetMat4("Model", Model);
	    glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glDisable(GL_DEPTH_TEST);
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        FramebufferShader.Use();
        glBindVertexArray(FramebufferVAO);
        glBindTexture(GL_TEXTURE_2D, TextureColorBuffer);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        DefaultGLFWWindowLoopEnd();
    }

    return 0;
}
