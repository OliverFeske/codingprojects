#include "DepthTesting.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Model.h"
#include "Shader.h"
#include "Utilities.h"

int DepthTesting::run()
{
    SetupDefaultGLFWWindow();

	stbi_set_flip_vertically_on_load(true);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	Shader BackpackShader{GetShadersPath() + "2_ModelLoading/Backpack.vs", GetShadersPath() + "3_AdvancedOpenGL/Depth.fs"};

	Model Backpack{GetModelsPath() + "backpack/backpack.obj"};

	while (!glfwWindowShouldClose(GWindow))
	{
	    DefaultGLFWWindowLoopStart();

		BackpackShader.Use();

        glm::mat4 projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 view = GCameraInstance.GetViewMatrix();
        BackpackShader.SetMat4("Projection", projection);
        BackpackShader.SetMat4("View", view);

        glm::mat4 Model = glm::mat4(1.0f);
        Model = glm::translate(Model, glm::vec3(0.0f, 0.0f, 0.0f));
        Model = glm::scale(Model, glm::vec3(1.0f, 1.0f, 1.0f));
        BackpackShader.SetMat4("Model", Model);
        Backpack.Draw(BackpackShader);

		DefaultGLFWWindowLoopEnd();
	}

	glfwTerminate();

	return 0;
}
