#include "Blending.h"

#include <algorithm>
#include <map>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "Shader.h"
#include "Utilities.h"

float Blending::CubeVertices[180] = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
    
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

float Blending::PlaneVertices[30] = {
     5.0f, -0.5f,  5.0f,  2.0f, 0.0f,
    -5.0f, -0.5f,  5.0f,  0.0f, 0.0f,
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f,

     5.0f, -0.5f,  5.0f,  2.0f, 0.0f,
    -5.0f, -0.5f, -5.0f,  0.0f, 2.0f,
     5.0f, -0.5f, -5.0f,  2.0f, 2.0f
};
float Blending::TransparentVertices[30] = {
    0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
    0.0f, -0.5f,  0.0f,  0.0f,  1.0f,
    1.0f, -0.5f,  0.0f,  1.0f,  1.0f,

    0.0f,  0.5f,  0.0f,  0.0f,  0.0f,
    1.0f, -0.5f,  0.0f,  1.0f,  1.0f,
    1.0f,  0.5f,  0.0f,  1.0f,  0.0f
};

std::vector<glm::vec3> Blending::Windows = {
    glm::vec3(-1.5f, 0.0f, -0.48f),
    glm::vec3(1.5f, 0.0f, 0.51f),
    glm::vec3(0.0f, 0.0f, 0.7f),
    glm::vec3(-0.3f, 0.0f, -2.3f),
    glm::vec3(0.5f, 0.0f, -0.6f)
};

int Blending::run()
{
	SetupDefaultGLFWWindow();

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	Shader BlendingShader{GetShadersPath() + "3_AdvancedOpenGL/Blending.vs", GetShadersPath() + "3_AdvancedOpenGL/Blending.fs"};

    uint32_t CubeVAO, CubeVBO;
    glGenVertexArrays(1, &CubeVAO);
    glGenBuffers(1, &CubeVBO);
    glBindVertexArray(CubeVAO);
    glBindBuffer(GL_ARRAY_BUFFER, CubeVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(CubeVertices), &CubeVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    uint32_t PlaneVAO, PlaneVBO;
    glGenVertexArrays(1, &PlaneVAO);
    glGenBuffers(1, &PlaneVBO);
    glBindVertexArray(PlaneVAO);
    glBindBuffer(GL_ARRAY_BUFFER, PlaneVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(PlaneVertices), &PlaneVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    uint32_t TransparentVAO, TransparentVBO;
    glGenVertexArrays(1, &TransparentVAO);
    glGenBuffers(1, &TransparentVBO);
    glBindVertexArray(TransparentVAO);
    glBindBuffer(GL_ARRAY_BUFFER, TransparentVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(TransparentVertices), &TransparentVertices, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

    uint32_t CubeTexture = LoadTexture(GetTexturesPath() + "marble.jpg");
    uint32_t FloorTexture = LoadTexture(GetTexturesPath() + "metal.png");
    uint32_t TransparentTexture = LoadTexture(GetTexturesPath() + "blending_transparent_window.png");

    BlendingShader.Use();
    BlendingShader.SetInt("Texture1", 0);

	while (!glfwWindowShouldClose(GWindow))
	{
	    DefaultGLFWWindowLoopStart();

	    glm::mat4 Projection = glm::perspective(glm::radians(GCameraInstance.GetFOV()), GWidth / GHeight, 0.1f, 100.0f);
        glm::mat4 View = GCameraInstance.GetViewMatrix();
        BlendingShader.SetMat4("Projection", Projection);
        BlendingShader.SetMat4("View", View);

        glm::mat4 Model = glm::mat4(1.0f);

        // Cubes
        glBindVertexArray(CubeVAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, CubeTexture);
        Model = glm::translate(Model, glm::vec3(-1.0f, 0.0f, -1.0f));
        BlendingShader.SetMat4("Model", Model);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        Model = glm::mat4(1.0f);
        Model = glm::translate(Model, glm::vec3(2.0f, 0.0f, 0.0f));
        BlendingShader.SetMat4("Model", Model);
	    glDrawArrays(GL_TRIANGLES, 0, 36);

        // Floor
        glBindVertexArray(PlaneVAO);
        glBindTexture(GL_TEXTURE_2D, FloorTexture);
        Model = glm::mat4(1.0f);
        BlendingShader.SetMat4("Model", Model);
	    glDrawArrays(GL_TRIANGLES, 0, 6);

        // Windows
        std::multimap<float, glm::vec3> Sorted;
        for (auto&& Window : Windows)
        {
            float Distance = glm::length(GCameraInstance.GetPosition() - Window);
            Sorted.insert(std::make_pair(Distance, Window));
        }
        glBindVertexArray(TransparentVAO);
        glBindTexture(GL_TEXTURE_2D, TransparentTexture);
        for (auto Itr = Sorted.rbegin(); Itr != Sorted.rend(); ++Itr)
        {
            Model = glm::mat4(1.0f);
            Model = glm::translate(Model, Itr->second);
            BlendingShader.SetMat4("Model", Model);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        }

		DefaultGLFWWindowLoopEnd();
	}

	return 0;
}
