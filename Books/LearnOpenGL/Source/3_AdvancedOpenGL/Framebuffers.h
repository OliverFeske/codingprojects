#pragma once
class Framebuffers
{
public:
	static int run();

private:
    static float CubeVertices[180];
	static float PlaneVertices[30];
	static float FrameBufferVertices[24];
};

