#include "Transformations.h"

#include <iostream>
#include <algorithm>

#include "glad/glad.h"
#include "glfw/glfw3.h"
#include "Shader.h"
#include "Utilities.h"

#include "stb/stb_image.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

int Transformations::run(Version version)
{
    switch (version) {
    case Version::Default: return Default();
    case Version::RotateThenTranslate: return RotateThenTranslate();
    case Version::SecondContainer: return SecondContainer();
    default: return 0;
    }
}

int Transformations::Default()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(1280, 1080, "LearnOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback(window, FrameBufferSizeCallback);

    glViewport(0, 0, 1280, 1080);

    // Rendering Setup
    uint32_t Texture1;
    glGenTextures(1, &Texture1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int Width, Height, NRChannels;
    std::string TextureName1{GetTexturesPath() + "Container.jpg"};
    unsigned char* Data1 = stbi_load(TextureName1.c_str(), &Width, &Height, &NRChannels, 0);
    if (Data1)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, Data1);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cerr << "ERROR:TEXTURE:LOAD_FAILED\n" << TextureName1 << "\n";
    }

    stbi_image_free(Data1);

    uint32_t Texture2;
    glGenTextures(1, &Texture2);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, Texture2);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    std::string TextureName2{GetTexturesPath() + "Awesomeface.png"};
    stbi_set_flip_vertically_on_load(true);
    unsigned char* Data2 = stbi_load(TextureName2.c_str(), &Width, &Height, &NRChannels, 0);
    if (Data2)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, Data2);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cerr << "ERROR:TEXTURE:LOAD_FAILED\n" << TextureName2 << "\n";
    }

    stbi_image_free(Data2);

    float Vertices[] = {
        // positions          // colors           // texture coords
        0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top right
        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom left
        -0.5f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f // top left 
    };

    uint32_t Indices[]
    {
        0, 1, 3, // Triangle 1
        1, 2, 3 // Triangle 2
    };

    uint32_t VAO, VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

    // 8 = XYZ (Position) RGB (Color) ST (Texture)
    // ST is equivalent to UV -> S or U = X, T or V = Y
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float))); // last parameter is offset
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float))); // last parameter is offset
    glEnableVertexAttribArray(2);


    Shader DefaultShader{GetShadersPath() + "0_GettingStarted/TextureTransformable.vs", GetShadersPath() + "0_GettingStarted/Texture.fs"};
    DefaultShader.Use();



    DefaultShader.SetInt("CustomTexture1", 0);
    DefaultShader.SetInt("CustomTexture2", 1);

    while (!glfwWindowShouldClose(window))
    {
        // Input
        ProcessInput(window);

        // Rendering
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glm::mat4 Transform = glm::mat4(1.0f);
        Transform = glm::translate(Transform, glm::vec3(0.5f, -0.5f, 0.0f));
        Transform = glm::rotate(Transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));

        uint32_t TransformLocation = glGetUniformLocation(DefaultShader.ID, "Transform");
        glUniformMatrix4fv(TransformLocation, 1, GL_FALSE, glm::value_ptr(Transform));

        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

        // Check events and swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

int Transformations::RotateThenTranslate()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(1280, 1080, "LearnOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback(window, FrameBufferSizeCallback);

    glViewport(0, 0, 1280, 1080);

    // Rendering Setup
    uint32_t Texture1;
    glGenTextures(1, &Texture1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int Width, Height, NRChannels;
    std::string TextureName1{GetTexturesPath() + "Container.jpg"};
    unsigned char* Data1 = stbi_load(TextureName1.c_str(), &Width, &Height, &NRChannels, 0);
    if (Data1)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, Data1);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cerr << "ERROR:TEXTURE:LOAD_FAILED\n" << TextureName1 << "\n";
    }

    stbi_image_free(Data1);

    uint32_t Texture2;
    glGenTextures(1, &Texture2);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, Texture2);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    std::string TextureName2{GetTexturesPath() + "Awesomeface.png"};
    stbi_set_flip_vertically_on_load(true);
    unsigned char* Data2 = stbi_load(TextureName2.c_str(), &Width, &Height, &NRChannels, 0);
    if (Data2)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, Data2);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cerr << "ERROR:TEXTURE:LOAD_FAILED\n" << TextureName2 << "\n";
    }

    stbi_image_free(Data2);

    float Vertices[] = {
        // positions          // colors           // texture coords
        0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top right
        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom left
        -0.5f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f // top left 
    };

    uint32_t Indices[]
    {
        0, 1, 3, // Triangle 1
        1, 2, 3 // Triangle 2
    };

    uint32_t VAO, VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

    // 8 = XYZ (Position) RGB (Color) ST (Texture)
    // ST is equivalent to UV -> S or U = X, T or V = Y
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float))); // last parameter is offset
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float))); // last parameter is offset
    glEnableVertexAttribArray(2);


    Shader DefaultShader{GetShadersPath() + "0_GettingStarted/TextureTransformable.vs", GetShadersPath() + "0_GettingStarted/Texture.fs"};
    DefaultShader.Use();



    DefaultShader.SetInt("CustomTexture1", 0);
    DefaultShader.SetInt("CustomTexture2", 1);

    while (!glfwWindowShouldClose(window))
    {
        // Input
        ProcessInput(window);

        // Rendering
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glm::mat4 Transform = glm::mat4(1.0f);
        Transform = glm::rotate(Transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));
        Transform = glm::translate(Transform, glm::vec3(0.5f, -0.5f, 0.0f));

        uint32_t TransformLocation = glGetUniformLocation(DefaultShader.ID, "Transform");
        glUniformMatrix4fv(TransformLocation, 1, GL_FALSE, glm::value_ptr(Transform));

        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

        // Check events and swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

int Transformations::SecondContainer()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(1280, 1080, "LearnOpenGL", nullptr, nullptr);
    if (!window)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback(window, FrameBufferSizeCallback);

    glViewport(0, 0, 1280, 1080);

    // Rendering Setup
    uint32_t Texture1;
    glGenTextures(1, &Texture1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int Width, Height, NRChannels;
    std::string TextureName1{GetTexturesPath() + "Container.jpg"};
    unsigned char* Data1 = stbi_load(TextureName1.c_str(), &Width, &Height, &NRChannels, 0);
    if (Data1)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0, GL_RGB, GL_UNSIGNED_BYTE, Data1);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cerr << "ERROR:TEXTURE:LOAD_FAILED\n" << TextureName1 << "\n";
    }

    stbi_image_free(Data1);

    uint32_t Texture2;
    glGenTextures(1, &Texture2);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, Texture2);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    std::string TextureName2{GetTexturesPath() + "Awesomeface.png"};
    stbi_set_flip_vertically_on_load(true);
    unsigned char* Data2 = stbi_load(TextureName2.c_str(), &Width, &Height, &NRChannels, 0);
    if (Data2)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, Width, Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, Data2);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cerr << "ERROR:TEXTURE:LOAD_FAILED\n" << TextureName2 << "\n";
    }

    stbi_image_free(Data2);

    float Vertices[] = {
        // positions          // colors           // texture coords
        0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top right
        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom left
        -0.5f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f // top left 
    };

    uint32_t Indices[]
    {
        0, 1, 3, // Triangle 1
        1, 2, 3 // Triangle 2
    };

    uint32_t VAO, VBO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

    // 8 = XYZ (Position) RGB (Color) ST (Texture)
    // ST is equivalent to UV -> S or U = X, T or V = Y
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float))); // last parameter is offset
    glEnableVertexAttribArray(1);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float))); // last parameter is offset
    glEnableVertexAttribArray(2);


    Shader DefaultShader{GetShadersPath() + "0_GettingStarted/TextureTransformable.vs", GetShadersPath() + "0_GettingStarted/TextureFragmentShader.fs"};
    DefaultShader.Use();



    DefaultShader.SetInt("CustomTexture1", 0);
    DefaultShader.SetInt("CustomTexture2", 1);

    while (!glfwWindowShouldClose(window))
    {
        // Input
        ProcessInput(window);

        // Rendering
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // container 1
        glm::mat4 Transform = glm::mat4(1.0f);
        Transform = glm::translate(Transform, glm::vec3(0.5f, -0.5f, 0.0f));
        Transform = glm::rotate(Transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));

        uint32_t TransformLocation = glGetUniformLocation(DefaultShader.ID, "Transform");
        glUniformMatrix4fv(TransformLocation, 1, GL_FALSE, glm::value_ptr(Transform));

        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

        // container 2
        Transform = glm::mat4(1.0f);
        Transform = glm::translate(Transform, glm::vec3(-0.5f, 0.5f, 0.0f));
        Transform = glm::scale(Transform, glm::vec3(1.0f) * sin((float)glfwGetTime()));

        TransformLocation = glGetUniformLocation(DefaultShader.ID, "Transform");
        glUniformMatrix4fv(TransformLocation, 1, GL_FALSE, glm::value_ptr(Transform));

        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);

        // Check events and swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

void Transformations::FrameBufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void Transformations::ProcessInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }

    /*
    static bool UpOldState = glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS;
    static bool DownOldState = glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS;

    bool UpNewState = glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS;
    bool DownNewState = glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS;

    if (UpOldState != UpNewState && UpNewState)
    {
        MixStrength = std::clamp(MixStrength + 0.1f, 0.0f, 1.0f);
    }
    else if (DownOldState != DownNewState && DownNewState)
    {
        MixStrength = std::clamp(MixStrength - 0.1f, 0.0f, 1.0f);
    }

    UpOldState = UpNewState;
    DownOldState = DownNewState;
    */
}