#pragma once

struct GLFWwindow;

class HelloTriangle
{
public:
	enum class Version : char
	{
	    Default,
		TwoNextToEachOther,
		TwoDifferentVAOAndVBO,
		TwoPrograms
	};

	static int run(Version version = Version::Default);

private:
	static int Default();
	static int TwoNextToEachOther();
	static int TwoDifferentVAOAndVBO();
	static int TwoPrograms();

	static void FrameBufferSizeCallback( GLFWwindow* window, int width, int height );
	static void ProcessInput( GLFWwindow* window );

	static const char* VertexShaderString;
	static const char* FragmentShaderString;
};

