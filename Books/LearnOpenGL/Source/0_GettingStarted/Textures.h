#pragma once

struct GLFWwindow;

class Textures
{
public:
    enum class Version : char
    {
        Default,
        OnlyHappyReverse,
        TextureWrapping,
        CenterPixelsOnly,
        VisibilityUpDown
    };

    static int run(Version version = Version::Default);

private:
    static int Default();
    static int OnlyHappyReverse();
    static int TextureWrapping();
    static int CenterPixelsOnly();
    static int VisibilityUpDown();

    static void FrameBufferSizeCallback( GLFWwindow* window, int width, int height );
    static void ProcessInput( GLFWwindow* window );

    static float MixStrength;
};
