#pragma once

struct GLFWwindow;

class HelloWindow
{
public:
    static int run();

private:
    static void FrameBufferSizeCallback( GLFWwindow* window, int width, int height );
    static void ProcessInput( GLFWwindow* window );
};

