#pragma once

struct GLFWwindow;

class CoordinateSystems
{
public:
	enum class Version : char
    {
		Default,
		Parameters,
		ViewMatrixAsCamera,
		RotatingContainers
    };

	static int run(Version version = Version::Default);

private:
	static int Default();
	static int Parameters();
	static int ViewMatrixAsCamera();
	static int RotatingContainers();

    static void FrameBufferSizeCallback(GLFWwindow* window, int width, int height);
	static void ProcessInput(GLFWwindow* window);
};

