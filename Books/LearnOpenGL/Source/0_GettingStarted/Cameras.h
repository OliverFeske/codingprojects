#pragma once

#include "glm/fwd.hpp"

class Cameras
{
public:
	static int run();

private:
	static float Vertices[180];
	static glm::vec3 CubePositions[10];
};
