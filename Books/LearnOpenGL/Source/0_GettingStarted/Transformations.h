#pragma once

struct GLFWwindow;

class Transformations
{
public:
	enum class Version : char
	{
		Default,
		RotateThenTranslate,
		SecondContainer
	};

	static int run(Version version = Version::Default);

private:
	static int Default();
	static int RotateThenTranslate();
	static int SecondContainer();

	static void FrameBufferSizeCallback(GLFWwindow* window, int width, int height);
	static void ProcessInput(GLFWwindow* window);
};

