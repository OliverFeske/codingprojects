#include "HelloWindow.h"

#include <iostream>
#include <glad/glad.h>
#include <glfw/glfw3.h>

void HelloWindow::FrameBufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void HelloWindow::ProcessInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
}

int HelloWindow::run()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(1280, 1080, "LearnOpenGL", nullptr, nullptr);
	if (!window)
	{
	    std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
		glfwTerminate();
		return 1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
	    std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
		return 1;
	}

	glfwSetFramebufferSizeCallback(window, FrameBufferSizeCallback);

	glViewport(0, 0, 1280, 1080);

	while (!glfwWindowShouldClose(window))
	{
		// Input
		ProcessInput(window);

		// Rendering
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// Check events and swap buffers
	    glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();

	return 0;
}
