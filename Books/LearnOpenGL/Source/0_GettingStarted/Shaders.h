#pragma once

struct GLFWwindow;

class Shaders
{
public:
	enum class Version : char
    {
		Default,
		UpsideDown,
		HorizontalOffset,
		FragmentColorToVertexPosition
	};

	static int run(Version version = Version::Default);
    
private:
	static int Default();
	static int UpsideDown();
	static int HorizontalOffset();
	static int FragmentColorToVertexPosition();

	static void FrameBufferSizeCallback( GLFWwindow* window, int width, int height );
	static void ProcessInput( GLFWwindow* window );
};