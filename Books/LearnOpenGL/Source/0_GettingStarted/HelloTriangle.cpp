#include "HelloTriangle.h"

#include <iostream>
#include <glad/glad.h>
#include <glfw/glfw3.h>

const char* HelloTriangle::VertexShaderString =
    "#version 330 core\n"
    "layout (location = 0) in vec3 aPos;\n"
    "void main()\n"
    "{\n"
    "gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
    "}\n";

const char* HelloTriangle::FragmentShaderString =
    "#version 330 core\n"
    "out vec4 FragColor;\n"
    "void main()\n"
    "{\n"
    "FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
    "}\n";

void HelloTriangle::FrameBufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

void HelloTriangle::ProcessInput(GLFWwindow* window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
}

int HelloTriangle::run(Version version)
{
    switch (version)
    {
    case Version::Default: return Default();
    case Version::TwoNextToEachOther: return TwoNextToEachOther();
    case Version::TwoDifferentVAOAndVBO: return TwoDifferentVAOAndVBO();
    case Version::TwoPrograms: return TwoPrograms();
    default: return 0;
    }
}

int HelloTriangle::Default()
{
    glfwInit();
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

    GLFWwindow* window = glfwCreateWindow( 1280, 1080, "LearnOpenGL", nullptr, nullptr );
    if (!window)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent( window );

    if (!gladLoadGLLoader( (GLADloadproc)glfwGetProcAddress ))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback( window, FrameBufferSizeCallback );

    glViewport( 0, 0, 1280, 1080 );

    // Rendering Setup
    uint32_t VertexShader = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( VertexShader, 1, &VertexShaderString, nullptr );
    glCompileShader( VertexShader );

    int32_t Success;
    char InfoLog[ 512 ];
    glGetShaderiv( VertexShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( VertexShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:VERTEX:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t FragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( FragmentShader, 1, &FragmentShaderString, nullptr );
    glCompileShader( FragmentShader );

    glGetShaderiv( FragmentShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( FragmentShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:FRAGMENT:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t ShaderProgram = glCreateProgram();
    glAttachShader( ShaderProgram, VertexShader );
    glAttachShader( ShaderProgram, FragmentShader );
    glLinkProgram( ShaderProgram );

    glGetProgramiv( ShaderProgram, GL_LINK_STATUS, &Success );
    if (!Success)
    {
        glGetProgramInfoLog( ShaderProgram, 512, nullptr, InfoLog );
        std::cerr << "ERROR:PROGRAM:LINK_FAILED\n" << InfoLog << "\n";
    }

    glDeleteShader( VertexShader );
    glDeleteShader( FragmentShader );

    float Vertices[]
    {
         0.5f,  0.5f, 0.0f, // top right
         0.5f, -0.5f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f, // bottom left
        -0.5f,  0.5f, 0.0f  // top left
    };

    uint32_t Indices[]
    {
        0, 1, 3,    // Triangle 1
        1, 2, 3     // Triangle 2
    };

    uint32_t VAO, VBO, EBO;
    glGenVertexArrays( 1, &VAO );
    glGenBuffers( 1, &VBO );
    glGenBuffers( 1, &EBO );

    glBindVertexArray( VAO );

    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertices ), Vertices, GL_STATIC_DRAW );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, EBO );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( Indices ), Indices, GL_STATIC_DRAW );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), nullptr );
    glEnableVertexAttribArray( 0 );

    while (!glfwWindowShouldClose( window ))
    {
        // Input
        ProcessInput( window );

        // Rendering
        glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
        glClear( GL_COLOR_BUFFER_BIT );

        glUseProgram( ShaderProgram );
        glBindVertexArray( VAO );
        glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr );

        // Check events and swap buffers
        glfwSwapBuffers( window );
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

int HelloTriangle::TwoNextToEachOther()
{
    glfwInit();
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

    GLFWwindow* window = glfwCreateWindow( 1280, 1080, "LearnOpenGL", nullptr, nullptr );
    if (!window)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent( window );

    if (!gladLoadGLLoader( (GLADloadproc)glfwGetProcAddress ))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback( window, FrameBufferSizeCallback );

    glViewport( 0, 0, 1280, 1080 );

    // Rendering Setup
    uint32_t VertexShader = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( VertexShader, 1, &VertexShaderString, nullptr );
    glCompileShader( VertexShader );

    int32_t Success;
    char InfoLog[ 512 ];
    glGetShaderiv( VertexShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( VertexShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:VERTEX:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t FragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( FragmentShader, 1, &FragmentShaderString, nullptr );
    glCompileShader( FragmentShader );

    glGetShaderiv( FragmentShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( FragmentShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:FRAGMENT:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t ShaderProgram = glCreateProgram();
    glAttachShader( ShaderProgram, VertexShader );
    glAttachShader( ShaderProgram, FragmentShader );
    glLinkProgram( ShaderProgram );

    glGetProgramiv( ShaderProgram, GL_LINK_STATUS, &Success );
    if (!Success)
    {
        glGetProgramInfoLog( ShaderProgram, 512, nullptr, InfoLog );
        std::cerr << "ERROR:PROGRAM:LINK_FAILED\n" << InfoLog << "\n";
    }

    glDeleteShader( VertexShader );
    glDeleteShader( FragmentShader );

    float Vertices[]
    {
        -0.5f,  -0.5f,   0.0f, // T1 bottom left
        -0.25f,  0.5f,   0.0f, // T1 top
         0.0f,  -0.5f,   0.0f, // T1 bottom right

         0.0f,  -0.5f,   0.0f, // T2 bottom left
         0.25f,  0.5f,   0.0f, // T2 top
         0.5f,  -0.5f,   0.0f, // T2 bottom right
    };

    uint32_t VAO, VBO;
    glGenVertexArrays( 1, &VAO );
    glGenBuffers( 1, &VBO );

    glBindVertexArray( VAO );

    glBindBuffer( GL_ARRAY_BUFFER, VBO );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertices ), Vertices, GL_STATIC_DRAW );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), nullptr );
    glEnableVertexAttribArray( 0 );

    while (!glfwWindowShouldClose( window ))
    {
        // Input
        ProcessInput( window );

        // Rendering
        glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
        glClear( GL_COLOR_BUFFER_BIT );

        glUseProgram( ShaderProgram );
        glBindVertexArray( VAO );
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // Check events and swap buffers
        glfwSwapBuffers( window );
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

int HelloTriangle::TwoDifferentVAOAndVBO()
{
    glfwInit();
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

    GLFWwindow* window = glfwCreateWindow( 1280, 1080, "LearnOpenGL", nullptr, nullptr );
    if (!window)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent( window );

    if (!gladLoadGLLoader( (GLADloadproc)glfwGetProcAddress ))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback( window, FrameBufferSizeCallback );

    glViewport( 0, 0, 1280, 1080 );

    // Rendering Setup
    uint32_t VertexShader = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( VertexShader, 1, &VertexShaderString, nullptr );
    glCompileShader( VertexShader );

    int32_t Success;
    char InfoLog[ 512 ];
    glGetShaderiv( VertexShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( VertexShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:VERTEX:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t FragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( FragmentShader, 1, &FragmentShaderString, nullptr );
    glCompileShader( FragmentShader );

    glGetShaderiv( FragmentShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( FragmentShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:FRAGMENT:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t ShaderProgram = glCreateProgram();
    glAttachShader( ShaderProgram, VertexShader );
    glAttachShader( ShaderProgram, FragmentShader );
    glLinkProgram( ShaderProgram );

    glGetProgramiv( ShaderProgram, GL_LINK_STATUS, &Success );
    if (!Success)
    {
        glGetProgramInfoLog( ShaderProgram, 512, nullptr, InfoLog );
        std::cerr << "ERROR:PROGRAM:LINK_FAILED\n" << InfoLog << "\n";
    }

    glDeleteShader( VertexShader );
    glDeleteShader( FragmentShader );

    float Vertices1[]
    {
        -0.5f,  -0.5f,   0.0f, // T1 bottom left
        -0.25f,  0.5f,   0.0f, // T1 top
         0.0f,  -0.5f,   0.0f, // T1 bottom right
    };

    float Vertices2[]
    {
         0.0f,  -0.5f,   0.0f, // T2 bottom left
         0.25f,  0.5f,   0.0f, // T2 top
         0.5f,  -0.5f,   0.0f, // T2 bottom right
    };

    uint32_t VAOs[2], VBOs[2];
    glGenVertexArrays( 2, VAOs );
    glGenBuffers( 2, VBOs );

    // Triangle 1
    glBindVertexArray( VAOs[0] );

    glBindBuffer( GL_ARRAY_BUFFER, VBOs[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertices1 ), Vertices1, GL_STATIC_DRAW );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), nullptr );
    glEnableVertexAttribArray( 0 );

    // Triangle 2
    glBindVertexArray( VAOs[ 1 ] );

    glBindBuffer( GL_ARRAY_BUFFER, VBOs[ 1 ] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertices2 ), Vertices2, GL_STATIC_DRAW );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), nullptr );
    glEnableVertexAttribArray( 0 );

    while (!glfwWindowShouldClose( window ))
    {
        // Input
        ProcessInput( window );

        // Rendering
        glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
        glClear( GL_COLOR_BUFFER_BIT );

        glUseProgram( ShaderProgram );

        glBindVertexArray( VAOs[0] );
        glDrawArrays( GL_TRIANGLES, 0, 3);

        glBindVertexArray(VAOs[1]);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // Check events and swap buffers
        glfwSwapBuffers( window );
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

int HelloTriangle::TwoPrograms()
{
    glfwInit();
    glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 3 );
    glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 3 );
    glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );

    GLFWwindow* window = glfwCreateWindow( 1280, 1080, "LearnOpenGL", nullptr, nullptr );
    if (!window)
    {
        std::cerr << __FUNCTION__ << ": Failed to create GLFW window\n";
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent( window );

    if (!gladLoadGLLoader( (GLADloadproc)glfwGetProcAddress ))
    {
        std::cerr << __FUNCTION__ << ": Failed to initialize GLAD\n";
        return 1;
    }

    glfwSetFramebufferSizeCallback( window, FrameBufferSizeCallback );

    glViewport( 0, 0, 1280, 1080 );

    // Rendering Setup
    uint32_t VertexShader = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource( VertexShader, 1, &VertexShaderString, nullptr );
    glCompileShader( VertexShader );

    int32_t Success;
    char InfoLog[ 512 ];
    glGetShaderiv( VertexShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( VertexShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:VERTEX:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t FragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource( FragmentShader, 1, &FragmentShaderString, nullptr );
    glCompileShader( FragmentShader );

    glGetShaderiv( FragmentShader, GL_COMPILE_STATUS, &Success );
    if (!Success)
    {
        glGetShaderInfoLog( FragmentShader, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:FRAGMENT:COMPILATION_FAILED\n" << InfoLog << "\n";
    }

    uint32_t ShaderProgram = glCreateProgram();
    glAttachShader( ShaderProgram, VertexShader );
    glAttachShader( ShaderProgram, FragmentShader );
    glLinkProgram( ShaderProgram );

    glGetProgramiv( ShaderProgram, GL_LINK_STATUS, &Success );
    if (!Success)
    {
        glGetProgramInfoLog( ShaderProgram, 512, nullptr, InfoLog );
        std::cerr << "ERROR:PROGRAM:LINK_FAILED\n" << InfoLog << "\n";
    }

    const char* FragmentShader2String =
        "#version 330 core\n"
        "out vec4 FragColor;\n"
        "void main()\n"
        "{\n"
        "FragColor = vec4(0.5f, 0.1f, 0.2f, 1.0f);\n"
        "}\n";

    uint32_t FragmentShader2 = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(FragmentShader2, 1, &FragmentShader2String, nullptr);
    glCompileShader(FragmentShader2);

    glGetShaderiv(FragmentShader2, GL_COMPILE_STATUS, &Success);
    if (!Success)
    {
        glGetShaderInfoLog( FragmentShader2, 512, nullptr, InfoLog );
        std::cerr << "ERROR:SHADER:FRAGMENT:COMPILATION_FAILED:2\n" << InfoLog << "\n";
    }

    uint32_t ShaderProgram2 = glCreateProgram();
    glAttachShader(ShaderProgram2, VertexShader);
    glAttachShader(ShaderProgram2, FragmentShader2);
    glLinkProgram(ShaderProgram2);

    glGetProgramiv(ShaderProgram2, GL_LINK_STATUS, &Success);
    if (!Success)
    {
        glGetProgramInfoLog(ShaderProgram2, 512, nullptr, InfoLog);
        std::cerr << "ERROR:PROGRAM:LINK_FAILED:2\n" << InfoLog << "\n";
    }

    glDeleteShader( VertexShader );
    glDeleteShader( FragmentShader );
    glDeleteShader(FragmentShader2);

    float Vertices1[]
    {
        -0.5f,  -0.5f,   0.0f, // T1 bottom left
        -0.25f,  0.5f,   0.0f, // T1 top
         0.0f,  -0.5f,   0.0f, // T1 bottom right
    };

    float Vertices2[]
    {
         0.0f,  -0.5f,   0.0f, // T2 bottom left
         0.25f,  0.5f,   0.0f, // T2 top
         0.5f,  -0.5f,   0.0f, // T2 bottom right
    };

    uint32_t VAOs[ 2 ], VBOs[ 2 ];
    glGenVertexArrays( 2, VAOs );
    glGenBuffers( 2, VBOs );

    // Triangle 1
    glBindVertexArray( VAOs[ 0 ] );

    glBindBuffer( GL_ARRAY_BUFFER, VBOs[ 0 ] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertices1 ), Vertices1, GL_STATIC_DRAW );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), nullptr );
    glEnableVertexAttribArray( 0 );

    // Triangle 2
    glBindVertexArray( VAOs[ 1 ] );

    glBindBuffer( GL_ARRAY_BUFFER, VBOs[ 1 ] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertices2 ), Vertices2, GL_STATIC_DRAW );

    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof( float ), nullptr );
    glEnableVertexAttribArray( 0 );

    while (!glfwWindowShouldClose( window ))
    {
        // Input
        ProcessInput( window );

        // Rendering
        glClearColor( 0.2f, 0.3f, 0.3f, 1.0f );
        glClear( GL_COLOR_BUFFER_BIT );

        glUseProgram( ShaderProgram );
        glBindVertexArray( VAOs[0] );
        glDrawArrays(GL_TRIANGLES, 0, 3);

        glUseProgram(ShaderProgram2);
        glBindVertexArray(VAOs[1]);
        glDrawArrays(GL_TRIANGLES, 0, 3);

        // Check events and swap buffers
        glfwSwapBuffers( window );
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}
