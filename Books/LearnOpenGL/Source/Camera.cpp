#include "Camera.h"

#include <algorithm>

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

Camera::Camera(glm::vec3 Position, glm::vec3 Up, float Yaw, float Pitch)
    : Position(Position), Front(glm::vec3(0.0f, 0.0f, -1.0f)), Up(glm::vec3(0.0f)), Right(glm::vec3(0.0f)),
WorldUp(Up), Yaw(Yaw), Pitch(Pitch), MovementSpeed(2.5f), MouseSensitivity(0.05f), FOV(45.0f), ZoomSpeed(3.5f)
{
    UpdateCameraVectors();
}

void Camera::Initialize(glm::vec3 InPosition, glm::vec3 InUp, float InYaw, float InPitch)
{
    Position = InPosition;
    WorldUp = InUp;
    Yaw = InYaw;
    Pitch = InPitch;

    UpdateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix() const
{
    return glm::lookAt(Position, Position + Front, Up);
}

float Camera::GetFOV() const
{
    return FOV;
}

glm::vec3 Camera::GetPosition() const
{
    return Position;
}

glm::vec3 Camera::GetFront() const
{
    return Front;
}

void Camera::ProcessKeyboard(MoveDirection Direction, float DeltaSeconds)
{
    float Velocity = MovementSpeed * DeltaSeconds;
    switch (Direction) {
    case MoveDirection::Forward:
        Position += Front * Velocity;
        break;
    case MoveDirection::Backward:
        Position -= Front * Velocity;
        break;
    case MoveDirection::Left:
        Position -= Right * Velocity;
        break;
    case MoveDirection::Right:
        Position += Right * Velocity;
        break;
    case MoveDirection::Up:
        Position += Up * Velocity;
        break;
    case MoveDirection::Down:
        Position -= Up * Velocity;
        break;
    }

    // FPS exercise
    //Position.y = 0.0f;
}

void Camera::ProcessMouseMovement(float XOffset, float YOffset)
{
    XOffset *= MouseSensitivity;
    YOffset *= MouseSensitivity;

    Yaw += XOffset;
    Pitch += YOffset;

    Pitch = std::clamp(Pitch, -89.0f, 89.0f);

    UpdateCameraVectors();
}

void Camera::ProcessMouseScroll(float YOffset)
{
    FOV = std::clamp(FOV - YOffset * ZoomSpeed, 1.0f, 90.0f);
}

void Camera::UpdateCameraVectors()
{
    glm::vec3 NewFront;
    NewFront.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    NewFront.y = sin(glm::radians(Pitch));
    NewFront.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
    Front = glm::normalize(NewFront);

    Right = glm::normalize(glm::cross(Front, WorldUp));
    Up = glm::normalize(glm::cross(Right, Front));
}
