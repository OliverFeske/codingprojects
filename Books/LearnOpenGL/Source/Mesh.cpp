#include "Mesh.h"

#include "glad/glad.h"

#include "Shader.h"

Mesh::Mesh(std::vector<Vertex> Vertices, std::vector<uint32_t> Indices, std::vector<Texture> Textures)
    : Vertices(Vertices), Indices(Indices), Textures(Textures)
{
    SetupMesh();
}

void Mesh::Draw(Shader& Shader)
{
    uint32_t DiffuseNr = 1;
    uint32_t SpecularNr = 1;
    for (uint32_t i = 0; i < Textures.size(); ++i)
    {
        glActiveTexture(GL_TEXTURE0 + i);
        std::string NumString;
        std::string TypeName = Textures[i].Type;
        if (TypeName == "TextureDiffuse")
        {
            NumString = std::to_string(DiffuseNr++);
        }
        else if (TypeName == "TextureSpecular")
        {
            NumString = std::to_string(SpecularNr++);
        }

        Shader.SetInt("Material." + TypeName + NumString, i);
        glBindTexture(GL_TEXTURE_2D, Textures[i].ID);
    }

    glActiveTexture(GL_TEXTURE0);

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, Indices.size(), GL_UNSIGNED_INT, nullptr);
    glBindVertexArray(0);
}

void Mesh::SetupMesh()
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, Vertices.size() * sizeof(Vertex), &Vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, Indices.size() * sizeof(uint32_t), &Indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));

    glBindVertexArray(0);
}
