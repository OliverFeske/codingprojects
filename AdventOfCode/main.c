#include "input.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

void _2015_01() {
    int floor = 0;
    int i = 0;
    while (i < 7000) {
        floor += input_2015_01[i] == '(' ? 1 : -1;
        i++;
    }
    printf("floor: %i\n", floor);

    i = 0;
    floor = 0;
    while (i < 7000) {
        floor += input_2015_01[i] == '(' ? 1 : -1;
        i++;
        if (floor < 0) {
            break;
        }
    }
    printf("position: %i\n", i);
}

void _2015_02() {
    unsigned long combined_surface_area = 0;
    unsigned long combined_ribbon_length = 0;
    int i = 0;
    int starting_index = 0;
    while (i < 8112) {
        if (input_2015_02[i] == '\n') {
            // copy substring
            char buffer[i - starting_index + 1];
            memcpy(buffer, &input_2015_02[starting_index], i - starting_index);
            buffer[i - starting_index] = '\0';

            unsigned long length = 0, width = 0, height = 0;
            char *p = buffer;
            while (*p) {
                if (isdigit(*p)) {
                    const unsigned long value = strtoul(p, &p, 10);
                    if(length == 0){
                        length = value;
                    } else if(width == 0){
                        width = value;
                    } else {
                        height = value;
                    }
                } else {
                    p++;
                }
            }

            // wrapping paper
            unsigned long lw = length * width, wh = width * height, hl = height * length;
            unsigned long slack = lw;
            if(wh < slack){
                slack = wh;
            }
            if(hl < slack) {
                slack = hl;
            }

            const unsigned long surface_area = (lw + wh + hl) * 2 + slack;
            combined_surface_area += surface_area;

            // ribbon
            lw = length + width;
            wh = width + height;
            hl = height + length;
            unsigned long ribbon_wrapping = lw;
            if(wh < ribbon_wrapping){
                ribbon_wrapping = wh;
            }
            if(hl < ribbon_wrapping){
                ribbon_wrapping = hl;
            }

            ribbon_wrapping *= 2;

            const unsigned long bow = length * width * height;
            const unsigned long ribbon_length = ribbon_wrapping + bow;
            combined_ribbon_length += ribbon_length;

            starting_index = i + 1;
        }
        i++;
    }

    printf("surface area: %lu\n", combined_surface_area);
    printf("ribbon length: %lu\n", combined_ribbon_length);
}

void _2015_03(){
    int horizontal_position = 72, horizontal_position_robot = 72, vertical_position = 3, vertical_position_robot = 3;
    bool visited_houses[400][80];
    unsigned long i = 0;
    while(i < 400){
        unsigned long j = 0;
        while(j < 80){
            visited_houses[i][j] = false;
            j++;
        }
        i++;
    }

    visited_houses[horizontal_position][vertical_position] = true;

    i = 0;
    unsigned long num_visited_houses = 1;
    while(i < 8192){
        char c = input_2015_03[i];
        switch(c){
            case '<':
                if(i % 2 == 0) {
                    horizontal_position -= 1;
                } else {
                    horizontal_position_robot -= 1;
                }
                break;
            case '>':
                if(i % 2 == 0) {
                    horizontal_position += 1;
                } else {
                    horizontal_position_robot += 1;
                }
                break;
            case '^':
                if(i % 2 == 0){
                    vertical_position += 1;
                } else {
                    vertical_position_robot += 1;
                }
                break;
            case 'v':
                if(i % 2 == 0) {
                    vertical_position -= 1;
                } else {
                    vertical_position_robot -= 1;
                }
                break;
        }

        if(visited_houses[horizontal_position][vertical_position] == false){
            visited_houses[horizontal_position][vertical_position] = true;
            num_visited_houses += 1;
        }

        if(visited_houses[horizontal_position_robot][vertical_position_robot] == false){
            visited_houses[horizontal_position_robot][vertical_position_robot] = true;
            num_visited_houses += 1;
        }

        i++;
    }

    printf("%lu", num_visited_houses);
}

void _2015_04(){

}

void _2015_05(){
    unsigned long i = 0;
    unsigned long num_nice_strings = 0;
    while(i < 17000){
        char substring[16];
        memcpy(substring, &input_2015_05[i], 16);

        unsigned long counter = 0;

        // three vowels
        unsigned long vowel_count = 0;
        while(counter < 16){
            char c = substring[counter];
            vowel_count += (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u');
            counter++;
        }

        counter = 0;

        // twice in a row letter
        bool contains_twice_in_a_row_letter = false;
        while(counter < 15){
            char a = substring[counter];
            char b = substring[counter + 1];
            if(a == b){
                contains_twice_in_a_row_letter = true;
                break;
            }

            counter++;
        }

        counter = 0;

        // excluded strings
        bool contains_excluded_string = false;
        while(counter < 15){
            char double_string[3];
            memcpy(double_string, &substring[counter], 2);
            double_string[2] = '\0';

            if(strcmp(double_string, "ab") == 0 ||strcmp(double_string, "cd") == 0 || strcmp(double_string, "pq") == 0 || strcmp(double_string, "xy") == 0){
                contains_excluded_string = true;
                break;
            }

            counter++;
        }

        num_nice_strings += (vowel_count >= 3 && contains_twice_in_a_row_letter && !contains_excluded_string);

        i += 17;
    }

    printf("%lu", num_nice_strings);
}

void _2015_05_1(){
    unsigned long i = 0;
    unsigned long num_nice_strings = 0;
    while(i < 17000){
        char substring[16];
        memcpy(substring, &input_2015_05[i], 16);

        bool contains_duplicate_pair = false;
        unsigned long counter = 0;
        while(counter < 13){
            char pair[2];
            memcpy(pair, &substring[counter], 2);

            unsigned long second_counter = counter + 2;
            while(second_counter < 15){
                char pair_to_check[2];
                memcpy(pair_to_check, &substring[second_counter], 2);

                if(pair[0] == pair_to_check[0] && pair[1] == pair_to_check[1]) {
                    contains_duplicate_pair = true;
                    break;
                }

                second_counter++;
            }

            counter++;
        }

        counter = 0;

        bool contains_repeating_letter = false;
        while(counter < 14) {
            if(substring[counter] == substring[counter + 2]) {
                contains_repeating_letter = true;
                break;
            }
            counter++;
        }

        num_nice_strings += (contains_duplicate_pair && contains_repeating_letter);

        i += 17;
    }

    printf("%lu", num_nice_strings);
}

void _2015_06() {
    FILE* fp = fopen("input_2015_06.txt", "r");

    bool grid[1000][1000];
    unsigned long i = 0;
    while(i < 1000) {
        unsigned long j = 0;
        while(j < 1000) {
            grid[i][j] = false;
            j++;
        }
        i++;
    }

    char string[100];
    unsigned long counter_c = 0;
    while(fgets(string, 100, fp)) {
        printf("%lu %s", counter_c, string);
        counter_c++;
    }
    return;

    unsigned long counter = 0;
    while(counter < 9546) {
        unsigned long string_length = 0;
        /** these ifs also take the following space into account */
        if(input_2015_06[counter] == 't' && input_2015_06[counter + 5] == 'e') {
            string_length += 7;
        } else if(input_2015_06[counter] == 't' && input_2015_06[counter + 6] == 'n') {
            string_length += 8;
        } else if(input_2015_06[counter] == 't' && input_2015_06[counter + 7] == 'f') {
            string_length += 9;
        }

        while(!(input_2015_06[counter + string_length] == '\0' || (input_2015_06[counter + string_length] == 't' && (input_2015_06[counter + string_length + 1] == 'u' || input_2015_06[counter + string_length + 1] == 'o')))) {
            string_length++;
        }

        char substring[string_length];
        memcpy(substring, &input_2015_06[counter], string_length);

        unsigned long min_x = 1000, max_x = 1000, min_y = 1000, max_y = 1000;
        char *p = substring;
        while (*p) {
            if (isdigit(*p)) {
                const unsigned long value = strtoul(p, &p, 10);
                if(min_x == 1000){
                    min_x = value;
                } else if(min_y == 1000){
                    min_y = value;
                } else if(max_x == 1000) {
                    max_x = value;
                } else {
                    max_y = value;
                }
            } else {
                p++;
            }
        }

        unsigned long current_x = min_x;
        while(current_x <= max_x) {
            unsigned long current_y = min_y;
            while(current_y <= max_y) {
                if(substring[5] == 'e') {
                    grid[current_x][current_y] = !grid[current_x][current_y];
                } else if (substring[6] == 'n') {
                    grid[current_x][current_y] = true;
                } else if(substring[7] == 'f') {
                    grid[current_x][current_y] = false;
                }
                current_y++;
            }
            current_x++;
        }

        counter += string_length;
    }

    unsigned long lit_counter = 0;
    i = 0;
    while(i < 1000) {
        unsigned long j  = 0;
        while(j < 1000) {
            lit_counter += grid[i][j];
            j++;
        }
        i++;
    }

    printf("%lu\n", lit_counter);

    fclose(fp);
}

int main() {
    _2015_06();
    return 0;
}
