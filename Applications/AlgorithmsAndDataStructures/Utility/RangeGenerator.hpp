//
// Created by Olive on 21/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_RANGEGENERATOR_HPP
#define ALGORITHMSANDDATASTRUCTURES_RANGEGENERATOR_HPP

#include <vector>
#include <random>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <iostream>

class RangeGenerator {
public:
    RangeGenerator() = delete;
    explicit RangeGenerator(size_t size) {
        if(_unsorted_pre_generated_range.empty()) {
            read_pre_generated_range();
        }

        _unsorted.reserve(size);
        _sorted.reserve(size);
        for(size_t i{0}; i < size; ++i) {
            _unsorted.emplace_back(_unsorted_pre_generated_range[i]);
            _sorted.emplace_back(_unsorted_pre_generated_range[i]);
        }

        std::sort(_sorted.begin(), _sorted.end());
    }
    std::vector<int>::iterator begin() {
        return _unsorted.begin();
    }

    std::vector<int>::iterator end() {
        return _unsorted.end();
    }
    std::vector<int>& get_unsorted() { return _unsorted; }

    void randomize() {
        for(size_t i{0}; i < _sorted.size(); ++i) {
            _unsorted[i] = _unsorted_pre_generated_range[i];
        }
    }
    void reverse() {
        size_t size{_sorted.size()};
        for(size_t i{0}; i < size; ++i) {
            _unsorted[size - i - 1] = _sorted[i];
        }
    }
    void sort() {
        std::sort(_unsorted.begin(), _unsorted.end());
    }

    bool is_sorted() const {
        return _unsorted == _sorted;
    }

    size_t size() const { return _sorted.size(); }

private:
    void create_pre_generated_range_file() {
        _unsorted_pre_generated_range.reserve(_range_size);
        for(int i{0}; i < _range_size; ++i) {
            _unsorted_pre_generated_range[i] = i;
        }

        auto range = std::default_random_engine{};
        std::ranges::shuffle(_unsorted_pre_generated_range, range);

        std::ofstream of{_range_file_name};
        if(!of.is_open()) {
            std::cerr << "Could not open file for writing: " << _range_file_name << std::endl;
            return;
        }

        std::ostream_iterator<int> os{of, "\n"};
        std::copy(_unsorted_pre_generated_range.begin(), _unsorted_pre_generated_range.end(), os);

        of.close();
    }

    void read_pre_generated_range() {
        std::ifstream ifs{_range_file_name};
        if(!ifs.is_open()) {
            create_pre_generated_range_file();
            return;
        }

        std::istream_iterator<std::string> itr{ifs}, eos{};
        _unsorted_pre_generated_range.reserve(_range_size);
        while(itr != eos) {
            _unsorted_pre_generated_range.emplace_back(std::stoi(*itr));
            ++itr;
        }
    }

    std::string _range_file_name{"../Utility/Range.txt"};
    size_t _range_size{1'000'000};

    std::vector<int> _sorted;
    std::vector<int>  _unsorted;
    static std::vector<int> _unsorted_pre_generated_range;
};

std::vector<int>RangeGenerator::_unsorted_pre_generated_range{};

#endif //ALGORITHMSANDDATASTRUCTURES_RANGEGENERATOR_HPP
