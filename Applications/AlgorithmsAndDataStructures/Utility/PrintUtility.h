//
// Created by Olive on 26/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_PRINTUTILITY_H
#define ALGORITHMSANDDATASTRUCTURES_PRINTUTILITY_H
#include <iostream>
#include <chrono>

class PrintUtility {
public:
    template<class T>
    static std::string separate_thousands(T number) {
        bool is_negative{number < 0};
        if(is_negative) {
            number = -number;
        }

        std::string num_string{std::to_string(number)};
        std::string result(num_string.size() + (is_negative ? 1 : 0) + ((num_string.size() - 1) / 3), '\'');

        size_t counter{3};
        int num_string_index{static_cast<int>(num_string.size()) - 1};
        for(int i{static_cast<int>(result.size() - 1)}; i >= 0; --i) {
            if(counter == 0) {
                counter = 3;
                continue;
            }

            result[i] = num_string[num_string_index];
            --num_string_index;
            --counter;
        }

        return result;
    }

    template<class Start, class End>
    static std::string highest_valid_time_unit_as_string(Start start, End end) {
        auto seconds = std::chrono::duration_cast<std::chrono::seconds>(end - start);
        if(seconds.count() > 0) {
            return std::string{std::to_string(seconds.count()) + "s"};
        }

        auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        if(milliseconds.count() > 0) {
            return std::string{std::to_string(milliseconds.count()) + "ms"};
        }

        auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        if(microseconds.count() > 0) {
            return std::string{std::to_string(microseconds.count()) + "mis"};
        }

        auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
        return std::string{std::to_string(nanoseconds.count()) + "ns"};
    }
};
#endif //ALGORITHMSANDDATASTRUCTURES_PRINTUTILITY_H
