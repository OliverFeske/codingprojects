#include "Algorithms/Sorting/Sorting.hpp"
#include "Algorithms/Searching/Searching.hpp"
#include "DataStructures/DataStructures.hpp"

int main() {
    Sorting::run_sorting();
    Searching::run_searching();
    DataStructures::test_data_structures();

    return 0;
}
