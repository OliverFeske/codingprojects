//
// Created by Olive on 26/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_DATASTRUCTURES_HPP
#define ALGORITHMSANDDATASTRUCTURES_DATASTRUCTURES_HPP

#include <iostream>
#include <vector>
#include <map>
#include "BinaryTree.hpp"

class DataStructures {
public:
    static void test_data_structures() {
        BinarySearchTree<int, int> bst;

        auto test_bst = [&bst](const std::vector<int>& expected_existing, unsigned long long expected_height) {
            for(const int key : expected_existing) {
                if(nullptr == bst.find(key)) {
                    std::cerr << "Could not find key" << key << "!\n";
                }
            }

            if(expected_height != bst.height()) {
                std::cerr << "Different height! Expected: " << expected_height << " | Actual: " << bst.height();
            }

            std::cout << std::endl;
        };

        std::map<int, int> map;
        map.insert(std::make_pair(0, 0));
        test_bst({}, 0);

        bst.insert(4, 0);
        test_bst({4}, 0);

        bst.insert(3, 0);
        test_bst({4, 3}, 0);

        bst.insert(2, 0);
        test_bst({4, 3, 2}, 0);

        bst.insert(6, 0);
        test_bst({4, 3, 2, 6}, 0);

        bst.insert(5, 0);
        test_bst({4, 3, 2, 6, 5}, 0);

        bst.insert(7, 0);
        test_bst({4, 3, 2, 6, 5, 7}, 0);

        bst.remove(4);
        test_bst({3, 2, 6, 5, 7}, 0);
    }

private:
};

#endif //ALGORITHMSANDDATASTRUCTURES_DATASTRUCTURES_HPP
