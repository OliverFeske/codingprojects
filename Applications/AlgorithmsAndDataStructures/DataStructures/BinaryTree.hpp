//
// Created by Olive on 26/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_BINARYTREE_HPP
#define ALGORITHMSANDDATASTRUCTURES_BINARYTREE_HPP

template<class Key, class Value>
class BinarySearchTree {
public:
    class Node {
    public:
        explicit Node(Key in_key, Value in_value) : _key{in_key}, _value{in_value}, _left{nullptr}, _right{nullptr} {}

        Key _key;
        Value _value;
        Node *_left, *_right;
    };

    BinarySearchTree() : _root{nullptr}, _height{0} {}
    ~BinarySearchTree() {

    }

    unsigned long long height() const { return _height; }

    Node* minimum(Node* root) const {
        Node* current{root};

        while(current && current->_left) {
            current = current->_left;
        }

        return current;
    }

    Node* find(const Key& key) const { return find_internal(_root, key); }
    Node* insert(const Key& key, const Value& value) {
        return insert_internal(_root, key, value);
    }
    void remove(const Key& key) { remove_internal(_root, key); }

private:
    Node* find_internal(Node* root, const Key& key) const {
        if(!root) {
            return nullptr;
        }

        if(key == root->_key) {
            return root;
        }

        return find_internal((key < root->_key ? root->_left : root->_right), key);
    }

    Node* insert_internal(Node* root, const Key& key, const Value& value) {
        if(!root) {
            return new Node(key, value);
        }

        if(key < root->_key) {
            root->_left = insert_internal(root->_left, key, value);
        }
        else if(key > root->_key) {
            root->_right = insert_internal(root->_right, key, value);
        }

        root->_value = value;
        return root;
    }

    Node* remove_internal(Node* root, Key key) {
        if(!root) {
            return nullptr;
        }

        if(key < root->_key) {
            root->_left = remove_internal(root->_left, key);
        }
        else if(key > root->_key) {
            root->_right = remove_internal(root->_right, key);
        }
        else {
            if(!root->_left) {
                Node* right{root->_right};
                delete root;
                return right;
            }
            else if(!root->_right) {
                Node* left{root->_left};
                delete root;
                return left;
            }
            else {
                Node* min{minimum(root->_right)};
                root->_key = min->_key;
                root->_value = min->_value;
                root->_right = remove_internal(root->_right, min->_key);
            }
        }

        return root;
    }

    Node* _root;
    unsigned long long _height;
};

#endif //ALGORITHMSANDDATASTRUCTURES_BINARYTREE_HPP
