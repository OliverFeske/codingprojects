//
// Created by Olive on 26/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_BINARYSEARCH_HPP
#define ALGORITHMSANDDATASTRUCTURES_BINARYSEARCH_HPP

#include <vector>
#include <limits>

class BinarySearch {
public:
    size_t operator()(const std::vector<int>& vector, int value) {
        if(vector.empty()) {
            return std::numeric_limits<size_t>::max();
        }

        size_t left{0}, right{vector.size() - 1};
        while(left <= right) {
            size_t middle{(left + right) / 2};

            if(value > vector[middle]) {
                left = middle + 1;
            }
            else if(value < vector[middle]) {
                right = middle - 1;
            }
            else {
                return middle;
            }
        }

        return std::numeric_limits<size_t>::max();
    }
};

#endif //ALGORITHMSANDDATASTRUCTURES_BINARYSEARCH_HPP
