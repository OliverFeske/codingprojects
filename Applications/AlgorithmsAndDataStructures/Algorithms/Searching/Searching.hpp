//
// Created by Olive on 26/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_SEARCHING_HPP
#define ALGORITHMSANDDATASTRUCTURES_SEARCHING_HPP

#include "BinarySearch.hpp"

class Searching {
public:
    static void run_searching() {
        RangeGenerator range{_range_size};
        range.sort();
        run_searches_on_range_vector(range, BinarySearch(), {0, 999'999, 1'000'000, 499'999, 250'000}, "BinarySearch");
    }

private:
    template<class Search>
    static void run_search_on_range_vector(RangeGenerator& range, Search search, int value) {
        auto start = std::chrono::high_resolution_clock::now();
        size_t found_index{search(range.get_unsorted(), value)};
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << PrintUtility::separate_thousands(value) << " | success: " << (found_index != std::numeric_limits<size_t>::max()) << " | " << PrintUtility::highest_valid_time_unit_as_string(start, end) << std::endl;
    }

    template<class Search>
    static void run_searches_on_range_vector(RangeGenerator& range, Search search, const std::vector<int>& values, const std::string& name) {
        std::cout << "Searching in " << PrintUtility::separate_thousands(_range_size) << " elements using " << name << "..." << std::endl;

        for(int value : values) {
            run_search_on_range_vector(range, search, value);
        }
    }

    static constexpr size_t _range_size{1'000'000};
};

#endif //ALGORITHMSANDDATASTRUCTURES_SEARCHING_HPP
