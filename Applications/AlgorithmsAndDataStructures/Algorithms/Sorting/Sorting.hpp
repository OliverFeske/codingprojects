//
// Created by Olive on 26/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_SORTING_HPP
#define ALGORITHMSANDDATASTRUCTURES_SORTING_HPP

#include "../../Utility/PrintUtility.h"
#include "../../Utility/RangeGenerator.hpp"
#include "InsertionSort.hpp"
#include "MergeSort.hpp"
#include "QuickSort.hpp"

class Sorting {
public:
    static void run_sorting() {
        run_sorts_on_range(1'000'000, std::sort<std::vector<int>::iterator, std::less<>>, "std");
        run_sorts_on_range(10'000, InsertionSort(), "Insertion");
        run_sorts_on_range_vector(1'000'000, MergeSort(), "Merge");
        run_sorts_on_range_vector(10'000, QuickSort(), "QuickSort");
    }

private:
    template<class Sort>
    static void run_sort_on_range(RangeGenerator& range, Sort sort, const std::string& name) {
        auto start = std::chrono::high_resolution_clock::now();
        sort(range.begin(), range.end(), std::less<>());
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << name << " | success: " << range.is_sorted() << " | " << PrintUtility::highest_valid_time_unit_as_string(start, end) << std::endl;
    }

    template<class Sort>
    static void run_sort_on_range_vector(RangeGenerator& range, Sort sort, const std::string& name) {
        auto start = std::chrono::high_resolution_clock::now();
        sort(range.get_unsorted(), std::less<>());
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << name << " | success: " << range.is_sorted() << " | " << PrintUtility::highest_valid_time_unit_as_string(start, end) << std::endl;
    }

    template<class Sort>
    static void run_sorts_on_range(size_t size, Sort sort, const std::string& name) {
        RangeGenerator range{size};

        std::cout << "Sort " << PrintUtility::separate_thousands(size) << " elements using " << name << "..." << std::endl;

        // Random
        range.randomize();
        run_sort_on_range(range, sort, "Random ");

        // Reverse
        range.reverse();
        run_sort_on_range(range, sort, "Reverse");

        // Sorted
        range.sort();
        run_sort_on_range(range, sort, "Sorted ");
    }

    template<class Sort>
    static void run_sorts_on_range_vector(size_t size, Sort sort, const std::string& name) {
        RangeGenerator range{size};

        std::cout << "Sort " << PrintUtility::separate_thousands(size) << " elements using " << name << "..." << std::endl;

        // Random
        range.randomize();
        run_sort_on_range_vector(range, sort, "Random ");

        // Reverse
        range.reverse();
        run_sort_on_range_vector(range, sort, "Reverse");

        // Sorted
        range.sort();
        run_sort_on_range_vector(range, sort, "Sorted ");
    }
};

#endif //ALGORITHMSANDDATASTRUCTURES_SORTING_HPP
