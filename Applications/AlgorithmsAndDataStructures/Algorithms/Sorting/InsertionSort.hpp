//
// Created by Olive on 21/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_INSERTIONSORT_HPP
#define ALGORITHMSANDDATASTRUCTURES_INSERTIONSORT_HPP

#include <functional>
#include <algorithm>

class InsertionSort {
public:
    template<class Iterator, class Comparator>
    void operator()(Iterator first, Iterator last, Comparator comparator){
        Iterator begin{first}, previous{first}, current{++first};

        while(first != last) {
            while(comparator(*current, *previous)) {
                std::iter_swap(current, previous);
                current = previous;

                if(previous == begin) {
                    break;
                }

                --previous;
            }

            previous = first;
            current = ++first;
        }
    }
    //static void sort() {}
};

#endif //ALGORITHMSANDDATASTRUCTURES_INSERTIONSORT_HPP
