//
// Created by Olive on 25/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_MERGESORT_HPP
#define ALGORITHMSANDDATASTRUCTURES_MERGESORT_HPP

#include <functional>
#include <iostream>

class MergeSort {
public:
    template<class Comparator>
    void operator()(std::vector<int>& vector, Comparator comparator) {
        std::vector<int> left(vector.size() / 2), right(vector.size() / 2);
        merge_sort_internal(vector, 0, vector.size() - 1, comparator, left, right);
    }

private:
    template<class Comparator>
    static void merge_sort_internal(std::vector<int>& vector, size_t first, size_t last, Comparator comparator, std::vector<int>& left, std::vector<int>& right) {
        if(last - first + 1 <= 1) {
            return;
        }

        size_t middle{(first + last) / 2};
        merge_sort_internal(vector, first, middle, comparator, left, right);
        merge_sort_internal(vector, middle + 1, last, comparator, left, right);

        merge(vector, first, middle, last, comparator, left, right);
    }

    template<class Comparator>
    static void merge(std::vector<int>& vector, size_t first, size_t middle, size_t last, Comparator comparator, std::vector<int>& left, std::vector<int>& right) {
        left.resize(middle - first + 1), right.resize(last - middle);
        for(size_t i {0}; i < left.size(); ++i) {
            left[i] = vector[first + i];
        }
        for(size_t i {0}; i < right.size(); ++i) {
            right[i] = vector[middle + 1 + i];
        }

        size_t left_pointer{0}, right_pointer{0}, overall_pointer{first};
        while(left_pointer < left.size() && right_pointer < right.size()) {
            if(comparator(left[left_pointer], right[right_pointer])) {
                vector[overall_pointer] = left[left_pointer];
                ++left_pointer;
            }
            else {
                vector[overall_pointer] = right[right_pointer];
                ++right_pointer;
            }
            ++overall_pointer;
        }

        while(left_pointer < left.size()) {
            vector[overall_pointer] = left[left_pointer];

            ++left_pointer;
            ++overall_pointer;
        }

        while(right_pointer < right.size()) {
            vector[overall_pointer] = right[right_pointer];

            ++right_pointer;
            ++overall_pointer;
        }
    }
};

#endif //ALGORITHMSANDDATASTRUCTURES_MERGESORT_HPP
