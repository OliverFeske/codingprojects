//
// Created by Olive on 26/12/2024.
//

#ifndef ALGORITHMSANDDATASTRUCTURES_QUICKSORT_HPP
#define ALGORITHMSANDDATASTRUCTURES_QUICKSORT_HPP

#include <vector>

class QuickSort {
public:
    template<class Comparator>
    void operator()(std::vector<int>& vector, Comparator comparator) {
        if(vector.size() <= 1) {
            return;
        }

        quick_sort_internal(vector, 0, vector.size() - 1, comparator);
    }

private:
    template<class Comparator>
    static void quick_sort_internal(std::vector<int>& vector, size_t first, size_t last, Comparator comparator) {
        /** Us this instead of first >= last as it prevents overflow issues for size_t */
        if(last - first + 1 <= 1) {
            return;
        }

        size_t left{first};
        int pivot{vector[last]};
        for(size_t i{first}; i < last; ++i) {
            if(comparator(vector[i], pivot)) {
                std::swap(vector[i], vector[left]);
                ++left;
            }
        }

        vector[last] = vector[left];
        vector[left] = pivot;

        quick_sort_internal(vector, first, left - 1, comparator);
        quick_sort_internal(vector, left + 1, last, comparator);
    }
};

#endif //ALGORITHMSANDDATASTRUCTURES_QUICKSORT_HPP
