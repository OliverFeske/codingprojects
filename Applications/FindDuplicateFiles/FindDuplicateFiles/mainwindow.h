
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileInfo>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_LoadFilesInDirectory_triggered();
    void on_FilterLoadedFiles_triggered();

private:
    Ui::MainWindow *ui;
    QMap<QString /* filename */, QList<QFileInfo>> loadedFiles;
};

#endif // MAINWINDOW_H
