#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QMap>
#include <QDirIterator>
#include <QFileDialog>
#include <QMessageBox>
#include <QTableWidgetItem>
#include <algorithm>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection);
    QStringList horizontalHeaderLabels;
    horizontalHeaderLabels << "Filename" << "Size" << "Path";
    ui->tableWidget->setHorizontalHeaderLabels(horizontalHeaderLabels);
    ui->tableWidget->verticalHeader()->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_LoadFilesInDirectory_triggered()
{
    QDir dir = QFileDialog::getExistingDirectory(this, "caption", QString(), QFileDialog::ShowDirsOnly);
    if(!dir.exists() || dir.dirName() == ".")
    {
        QMessageBox::critical(this, "Invalid directory", "Invalid directory selected.");
        return;
    }

    ui->tableWidget->clear();
    loadedFiles.clear();

    QDirIterator itr(dir.absolutePath(), QStringList(), QDir::Files, QDirIterator::Subdirectories);
    while(itr.hasNext())
    {
        QFileInfo fileInfo = itr.nextFileInfo();
        if(loadedFiles.contains(fileInfo.fileName()))
        {
            loadedFiles[fileInfo.fileName()].push_back(fileInfo);
        }
        else
        {
            loadedFiles.insert(fileInfo.fileName(), {fileInfo});
        }
    }

    ui->tableWidget->setRowCount(loadedFiles.size());
    size_t row = 0;
    for(const auto& [key, value] : loadedFiles.asKeyValueRange())
    {
        ui->tableWidget->setItem(row, 0, new QTableWidgetItem(key));

        if(ui->checkBox->isChecked())
        {
            std::sort(value.begin(), value.end(), [](const QFileInfo& a, const QFileInfo& b){
                return a.size() > b.size();
            });
        }

        QString paths, sizes;
        for(const QFileInfo& fileInfo : value)
        {
            sizes += QString::number(fileInfo.size()) + "\n";
            paths += fileInfo.path() + "\n";
        }

        paths.removeLast();
        sizes.removeLast();

        ui->tableWidget->setItem(row, 1, new QTableWidgetItem(sizes));
        ui->tableWidget->setItem(row, 2, new QTableWidgetItem(paths));

        ui->tableWidget->resizeRowToContents(row);

        row++;
    }

    QMessageBox::information(this, "Loading Finished", "Number of loaded files: " + QString::number(loadedFiles.size()));
}

void MainWindow::on_FilterLoadedFiles_triggered()
{
    QStringList fileFilters = ui->lineEdit->text().split('|', Qt::SkipEmptyParts);

    size_t row = 0;
    for(auto [key, value] : loadedFiles.asKeyValueRange())
    {
        bool isIncludedInFilter = fileFilters.isEmpty();
        for(const QString& filter : fileFilters)
        {
            if(key.contains(filter))
            {
                isIncludedInFilter = true;
                break;
            }
        }

        bool meetsMinimumDuplicateRequirement = value.size() >= ui->spinBox->value();

        if(isIncludedInFilter && meetsMinimumDuplicateRequirement)
        {
            ui->tableWidget->showRow(row);
        }
        else
        {
            ui->tableWidget->hideRow(row);
        }

        row++;
    }
}
