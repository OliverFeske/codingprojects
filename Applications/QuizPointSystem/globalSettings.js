import "../p5.js";
import { Participant } from "./participant.js";

export class CanvasSettings
{
    constructor()
    {
        this.sizeX = 1920;
        this.sizeY = 1080;
        this.color = "white";
    }
};

export class PointsSetings
{
    constructor()
    {
        this.perCorrectAnswer = 3;
        this.perWrongAnswer = 1;
        this.allowOnlyPositive = true;
    }
}

export class ButtonSettings
{
    constructor()
    {
        /** correct */
        this.correct_color = "green";
        this.correct_textColor = "black";

        /** wrong */
        this.wrong_color = "red";
        this.wrong_textColor = "black";

        /** +1 */
        this.plusOne_color = "white";
        this.plusOne_textColor = "black";

        /** -1 */
        this.minusOne_color = "white";
        this.minusOne_textColor = "black";
    }
};

export class ActiveParticipantsSettings
{
    constructor()
    {
        this.activeParticipants = [
            new Participant("jeff", 0),
            new Participant("mike", 0),
            new Participant("jeff2", 0),
            new Participant("mike2", 10),
            new Participant("hallo", 10),
            new Participant("hallo2", 10),
            new Participant("hallo3", 10),
        ];
    }
};

export class ParticipantSettings
{
    constructor()
    {
        this.scale = 1;
        this.backgroundColor = "grey";
        this.padding = 10;
        this.nameColor = "black";
        this.pointsColor = "cyan";
    }
};