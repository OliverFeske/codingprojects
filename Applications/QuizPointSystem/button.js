import "../p5.js";
import "./quizPointSystem.js";

export class Button
{
    constructor(s, location, size, color, textColor, textSize, text, eventMessage, userID)
    {
        this.s = s;
        this.location = location;
        this.size = size;
        this.color = color;
        this.textColor = textColor;
        this.textSize = textSize;
        this.text = text;
        this.eventMessage = eventMessage;
        this.userID = userID;
    }

    draw()
    {
        this.s.fill(this.color);
        this.s.rect(this.location.x, this.location.y, this.size.x, this.size.y);
        this.s.fill(this.textColor);
        this.s.textSize(this.textSize);
        this.s.textAlign(this.s.CENTER, this.s.CENTER);
        this.s.text(this.text, this.location.x + (this.size.x / 2), this.location.y + (this.size.y / 2));
    }

    mouseReleased()
    {
        if(this.s.mouseX >= this.location.x 
            && this.s.mouseX <= this.location.x + this.size.x
            && this.s.mouseY >= this.location.y 
            && this.s.mouseY <= this.location.y + this.size.y)
        {
            this.s.buttonEvent(this.eventMessage, this.userID);
        }
    }
};