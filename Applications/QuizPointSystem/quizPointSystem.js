import "../p5.js";
import { ActiveParticipantsSettings, CanvasSettings, PointsSetings } from "./globalSettings.js";


const sketch = function(s)
{
    s.canvasSettings = new CanvasSettings();
    s.activeParticipantsSettings = new ActiveParticipantsSettings();
    s.pointsSettings = new PointsSetings();

    s.setup = function()
    {
        let canvas = s.createCanvas(s.canvasSettings.sizeX, s.canvasSettings.sizeY);
        canvas.background(s.canvasSettings.color);

        for(let i = 0; i < s.activeParticipantsSettings.activeParticipants.length; i++)
        {
            s.activeParticipantsSettings.activeParticipants[i].setup(s, i);
        }
    };

    s.draw = function()
    {
        for(let i = 0; i < s.activeParticipantsSettings.activeParticipants.length; i++)
        {
            s.activeParticipantsSettings.activeParticipants[i].draw();
        }
    };

    s.mouseReleased = function()
    {
        for(let i = 0; i < s.activeParticipantsSettings.activeParticipants.length; i++)
        {
            s.activeParticipantsSettings.activeParticipants[i].mouseReleased();
        }
    };

    s.buttonEvent = function(eventMessage, userID)
    {
        if(eventMessage === "correct")
        {
            s.updatePointsForUser(userID, s.activeParticipantsSettings.activeParticipants[userID].points + s.pointsSettings.perCorrectAnswer);
        }
        else if(eventMessage === "wrong")
        {
            for(let i = 0; i < s.activeParticipantsSettings.activeParticipants.length; i++)
            {
                if(i != userID)
                {
                    s.updatePointsForUser(i, s.activeParticipantsSettings.activeParticipants[i].points + s.pointsSettings.perWrongAnswer);
                }
            }
        }
        else if(eventMessage === "+1")
        {
            s.updatePointsForUser(userID, s.activeParticipantsSettings.activeParticipants[userID].points + 1);
        }
        else if(eventMessage === "-1")
        {
            s.updatePointsForUser(userID, s.activeParticipantsSettings.activeParticipants[userID].points - 1);
        }
    };

    s.updatePointsForUser = function(userID, newPoints)
    {
        s.activeParticipantsSettings.activeParticipants[userID].points = s.pointsSettings.allowOnlyPositive && newPoints < 0 ? 0 : newPoints;
    }
    
};

new p5(sketch);