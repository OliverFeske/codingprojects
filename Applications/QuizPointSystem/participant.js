import "../p5.js";
import "./quizPointSystem.js";
import { Button } from "./button.js";
import { ButtonSettings, ParticipantSettings } from "./globalSettings.js";

export class Participant
{
    constructor(name, startingPoints = 0)
    {
        this.participantSettings = new ParticipantSettings();
        this.buttonSettings = new ButtonSettings();

        this.name = name;
        this.points = startingPoints;
        this.width = 200 * this.participantSettings.scale;
        this.height = 330 * this.participantSettings.scale;

        this.userID = -1;
    }

    setup(s, index)
    {
        this.s = s;
        this.location = this.s.createVector(this.participantSettings.padding * (1 + index) + (this.width * index), this.participantSettings.padding);
        this.userID = index;

        this.correctButton = new Button(
            this.s, 
            this.s.createVector(this.location.x + (5  * this.participantSettings.scale), 160 * this.participantSettings.scale), 
            this.s.createVector(this.width - (10 * this.participantSettings.scale), 50 * this.participantSettings.scale), 
            this.buttonSettings.correct_color, 
            this.buttonSettings.correct_textColor, 
            32 * this.participantSettings.scale, 
            "Correct", 
            "correct", 
            this.userID);

        this.wrongButton = new Button(
            this.s, 
            this.s.createVector(this.location.x + (5  * this.participantSettings.scale), 220 * this.participantSettings.scale), 
            this.s.createVector(this.width - (10  * this.participantSettings.scale), 50 * this.participantSettings.scale), 
            this.buttonSettings.wrong_color, 
            this.buttonSettings.wrong_textColor, 
            32 * this.participantSettings.scale, 
            "Wrong", 
            "wrong", 
            this.userID);

        this.plusOneButton = new Button(
            this.s, 
            this.s.createVector(this.location.x + (5 * this.participantSettings.scale), 280 * this.participantSettings.scale), 
            this.s.createVector(50 * this.participantSettings.scale, 50 * this.participantSettings.scale), 
            this.buttonSettings.plusOne_color, 
            this.buttonSettings.plusOne_textColor, 
            32 * this.participantSettings.scale, 
            "+1", 
            "+1", 
            this.userID);

        this.minusOneButton = new Button(
            this.s, 
            this.s.createVector(this.location.x + (this.width - (55 * this.participantSettings.scale)), 280 * this.participantSettings.scale), 
            this.s.createVector(50 * this.participantSettings.scale, 50 * this.participantSettings.scale), 
            this.buttonSettings.minusOne_color, 
            this.buttonSettings.minusOne_textColor, 
            32 * this.participantSettings.scale, 
            "-1", 
            "-1", 
            this.userID);

        this.buttons = [
            this.correctButton,
            this.wrongButton,
            this.plusOneButton,
            this.minusOneButton
        ]
    }

    draw()
    {
        /** Box */
        this.s.fill(this.participantSettings.backgroundColor);
        this.s.rect(this.location.x, this.location.y, this.width, this.height);

        /** Name */
        this.s.fill(this.participantSettings.nameColor);
        this.s.textSize(32 * this.participantSettings.scale);
        this.s.textAlign(this.s.CENTER, this.s.CENTER);
        this.s.text(this.name, this.location.x + (this.width / 2), 30 * this.participantSettings.scale);

        /** Points */
        this.s.fill(this.participantSettings.pointsColor);
        this.s.textSize(32 * this.participantSettings.scale);
        this.s.textAlign(this.s.CENTER, this.s.CENTER);
        this.s.text(this.points, this.location.x + (this.width / 2), 110 * this.participantSettings.scale);

        for(let i = 0; i < this.buttons.length; i++)
        {
            this.buttons[i].draw();
        }
    }

    mouseReleased()
    {
        for(let i = 0; i < this.buttons.length; i++)
        {
            this.buttons[i].mouseReleased();
        }
    }
};