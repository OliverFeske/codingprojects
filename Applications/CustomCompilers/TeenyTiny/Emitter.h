//
// Created by Olive on 05/05/2024.
//

#ifndef TEENYTINY_EMITTER_H
#define TEENYTINY_EMITTER_H

#include <string>

class Emitter {
public:
    Emitter() = delete;
    explicit Emitter(std::string in_output_file_name);

    void write_file();
    void emit(const std::string& code_fragment);
    void emit_line(const std::string& code_line);
    void header_line(const std::string& header);

private:
    std::string output_file_name_, header_, code_;
};


#endif //TEENYTINY_EMITTER_H
