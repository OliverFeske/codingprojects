//
// Created by Olive on 05/05/2024.
//

#include "Emitter.h"

#include <fstream>
#include <iostream>

Emitter::Emitter(std::string in_output_file_name)
    : output_file_name_{std::move(in_output_file_name)} {

}

void Emitter::write_file() {
    std::ofstream output_file;
    output_file.open(output_file_name_);
    if(!output_file.is_open()) {
        std::cerr << "Could not open or create output file [" << output_file_name_ << "] for emitting.\n";
        return;
    }

    output_file << header_ << code_;
    output_file.close();
}

void Emitter::emit(const std::string &code_fragment) {
    code_ += code_fragment;
}

void Emitter::emit_line(const std::string &code_line) {
    code_ += code_line + "\n";
}

void Emitter::header_line(const std::string &header) {
    header_ += header + "\n";
}
