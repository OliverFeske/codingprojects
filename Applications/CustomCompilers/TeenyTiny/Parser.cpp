//
// Created by Olive on 04/05/2024.
//

#include "Parser.h"
#include "Lexer.h"
#include "Emitter.h"

#include <iostream>

Parser::Parser(const std::shared_ptr<Lexer>& in_lexer, const std::shared_ptr<Emitter>& in_emitter)
    : lexer_{in_lexer}, emitter_{in_emitter}, current_token_{}, peek_token_{}, current_line_{1} {
    statement_function_map_.insert_or_assign(TokenType::Print, std::function<bool(Parser*)>{&Parser::statement_print});
    statement_function_map_.insert_or_assign(TokenType::If, std::function<bool(Parser*)>{&Parser::statement_if});
    statement_function_map_.insert_or_assign(TokenType::While, std::function<bool(Parser*)>{&Parser::statement_while});
    statement_function_map_.insert_or_assign(TokenType::Label, std::function<bool(Parser*)>{&Parser::statement_label});
    statement_function_map_.insert_or_assign(TokenType::Goto, std::function<bool(Parser*)>{&Parser::statement_goto});
    statement_function_map_.insert_or_assign(TokenType::Let, std::function<bool(Parser*)>{&Parser::statement_let});
    statement_function_map_.insert_or_assign(TokenType::Input, std::function<bool(Parser*)>{&Parser::statement_input});

    next_token();
}

void Parser::start() {
    program();
}

bool Parser::check_token(const TokenType type) const {
    return current_token_.type == type;
}

bool Parser::match(const TokenType type) {
    if(!check_token(type)) {
        print_error("Could not match required token type " + Token::token_type_to_string(type) + " to current " + Token::token_type_to_string(current_token_.type));
        return false;
    }

    next_token();
    return true;
}

void Parser::next_token() {
    current_token_ = lexer_->get_token();
}

bool Parser::is_comparison_operator() const {
    return
        current_token_.type == TokenType::EqualEqual
        || current_token_.type == TokenType::NotEqual
        || current_token_.type == TokenType::GreaterThan
        || current_token_.type == TokenType::GreaterThanEqual
        || current_token_.type == TokenType::LessThan
        || current_token_.type == TokenType::LessThanEqual;
}


void Parser::program() {
    emitter_->header_line("#include <stdio.h>");
    emitter_->header_line("int main() {");

    if(check_token(TokenType::NewLine)) {
        new_line();
    }

    while(!check_token(TokenType::EndOFile)) {
        if(!lexer_->is_valid_token(current_token_) || !statement()) {
            std::cerr << "Parser completed with errors!\n";
            return;
        }
    }

    std::unordered_set<std::string> differences;
    std::set_difference(gotoed_labels_.begin(), gotoed_labels_.end(),
                        declared_labels_.begin(), declared_labels_.end(),
                        std::inserter(differences, differences.begin()));

    emitter_->emit_line("return 0;");
    emitter_->emit_line("}");

    if(!differences.empty()) {
        for(auto const& difference : differences) {
            std::cerr << "Attempting GOTO to undeclared label: " << difference << "\n";
        }

        return;
    }

    std::cout << "Parser completed successfully.\n";
}

bool Parser::statement() {
    auto itr = statement_function_map_.find(current_token_.type);
    if(itr != statement_function_map_.end()) {
        return itr->second(this) && new_line();
    }
    else {
        print_error("Could not find statement for token [" + current_token_.to_string() + "]");
        return false;
    }
}

bool Parser::expression() {
    if(!term()) {
        return false;
    }

    while(check_token(TokenType::Plus) || check_token(TokenType::Minus)) {
    emitter_->emit(current_token_.value);
        next_token();
        if(!term()) {
            return false;
        }
    }

    return true;
}

bool Parser::comparison() {
    if(!expression()) {
        return false;
    }

    if(is_comparison_operator()) {
        emitter_->emit(current_token_.value);
        next_token();
        if(!expression()) {
            return false;
        }
    }
    else {
        print_error("Invalid comparison operator found [" + current_token_.to_string() + "]");
    }

    while(is_comparison_operator()) {
        emitter_->emit(current_token_.value);
        next_token();
        if(!expression()) {
            return false;
        }
    }

    return true;
}

bool Parser::new_line() {
    if(!match(TokenType::NewLine)) {
        return false;
    }

    current_line_++;

    while(check_token(TokenType::NewLine)) {
        next_token();
        current_line_++;
    }

    return true;
}

bool Parser::term() {
    if(!unary()) {
        return false;
    }

    while(check_token(TokenType::Asterisk) || check_token(TokenType::Slash)) {
        emitter_->emit(current_token_.value);
        next_token();
        if(!unary()) {
            return false;
        }
    }

    return true;
}

bool Parser::unary() {
    if(check_token(TokenType::Plus) || check_token(TokenType::Minus)) {
        emitter_->emit(current_token_.value);
        next_token();
    }

    return primary();
}

bool Parser::primary() {
    if(check_token(TokenType::Number)) {
        emitter_->emit(current_token_.value);
        next_token();
    }
    else if(check_token(TokenType::Identifier)) {
        if(!symbols_.contains(current_token_.value)) {
            print_error("Referencing variable before assignment: " + current_token_.value);
            return false;
        }

        emitter_->emit(current_token_.value);
        next_token();
    }
    else {
        print_error("Failed to resolve primary from token: [" + current_token_.to_string() + "]");
        return false;
    }

    return true;
}

bool Parser::statement_print() {
    next_token();

    if(check_token(TokenType::String)) {
        emitter_->emit_line("printf(\"" + current_token_.value + "\\n\");");
        next_token();
    }
    else {
        emitter_->emit("printf(\"%.2f\\n\", (float)(");

        if(!expression()) {
            return false;
        }

        emitter_->emit_line("));");
    }

    return true;
}

bool Parser::statement_if() {
    next_token();

    emitter_->emit("if(");

    if(!comparison()) {
        return false;
    }

    if(!match(TokenType::Then)) {
        return false;
    }

    if(!new_line()) {
        return false;
    }

    emitter_->emit_line("){");

    while(!check_token(TokenType::EndIf)) {
        if(!statement()) {
            return false;
        }
    }

    emitter_->emit_line("}");

    return match(TokenType::EndIf);
}

bool Parser::statement_while() {
    next_token();

    emitter_->emit("while(");

    if(!comparison()) {
        return false;
    }

    if(!match(TokenType::Repeat)) {
        return false;
    }

    if(!new_line()) {
        return false;
    }

    emitter_->emit_line("){");

    while(!check_token(TokenType::EndWhile)) {
        if(!statement()) {
            return false;
        }
    }

    emitter_->emit_line("}");

    return match(TokenType::EndWhile);
}

bool Parser::statement_label() {
    next_token();

    if(!declared_labels_.insert(current_token_.value).second) {
        print_error("Label [" + current_token_.value + "] already exists!");
        return false;
    }

    emitter_->emit_line(current_token_.value + ":");

    return match(TokenType::Identifier);
}

bool Parser::statement_goto() {
    next_token();

    gotoed_labels_.insert(current_token_.value);

    emitter_->emit_line("goto " + current_token_.value + ";");

    return match(TokenType::Identifier);
}

bool Parser::statement_let() {
    next_token();

    if(symbols_.insert(current_token_.value).second) {
        emitter_->header_line("float " + current_token_.value + ";");
    }

    emitter_->emit(current_token_.value + " = ");

    if(!(match(TokenType::Identifier) && match(TokenType::Equal) && expression())) {
        return false;
    }

    emitter_->emit_line(";");

    return true;
}

bool Parser::statement_input() {
    next_token();

    if(symbols_.insert(current_token_.value).second) {
       emitter_->header_line("float " + current_token_.value + ";");
    }

    emitter_->emit_line("if(0 == scanf(\"%f\", &" + current_token_.value + ")) {");
    emitter_->emit_line(current_token_.value + " = 0;");
    emitter_->emit_line("scanf(\"%*s\");");
    emitter_->emit_line("}");

    return match(TokenType::Identifier);
}

void Parser::print_error(const std::string &error_message) {
    std::cerr << "Error in line " << current_line_ << ": " << error_message << "\n";
}
