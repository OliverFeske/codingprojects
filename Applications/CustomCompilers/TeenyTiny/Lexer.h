//
// Created by Olive on 02/05/2024.
//

#ifndef LEXER_H
#define LEXER_H

#include <string>
#include <memory>

struct Token;

class Lexer {
public:
    Lexer() = delete;
    explicit Lexer(std::string source);

    Token get_token();
    char peek();
    bool is_valid_token(const Token& token) const;

private:
    void next_char();
    void skip_whitespace();
    void skip_comment();



    std::string source_;
    char current_char_;
    size_t current_pos_;
};


#endif //LEXER_H
