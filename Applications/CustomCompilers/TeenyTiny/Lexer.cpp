//
// Created by Olive on 02/05/2024.
//

#include "Lexer.h"
#include "Token.h"

#include <utility>
#include <iostream>

Lexer::Lexer(std::string source)
    : source_{std::move(source)}, current_char_{'\0'}, current_pos_{0}{
    source_ += "\n";
    current_char_ = source_[current_pos_];
}

Token Lexer::get_token() {
    skip_whitespace();
    skip_comment();

    Token token{current_char_, TokenType::NONE};

    /* Misc */
    if(current_char_ == '\0') {
        token.type = TokenType::EndOFile;
    }
    else if(current_char_ == '\n') {
        token.type = TokenType::NewLine;
    }
    else if(current_char_ == '+') {
        token.type = TokenType::Plus;
    }
    else if(current_char_ == '-') {
        token.type = TokenType::Minus;
    }
    else if(current_char_ == '*') {
        token.type = TokenType::Asterisk;
    }
    else if(current_char_ == '/') {
        token.type = TokenType::Slash;
    }
    else if(current_char_ == '=') {
        if(peek() == '=') {
            token.type = TokenType::EqualEqual;
            next_char();
            token.value += current_char_;
        }
        else {
            token.type = TokenType::Equal;
        }
    }
    else if(current_char_ == '<') {
        if(peek() == '=') {
            token.type = TokenType::LessThanEqual;
            next_char();
            token.value += current_char_;
        }
        else {
            token.type = TokenType::LessThan;
        }
    }
    else if(current_char_ == '>') {
        if(peek() == '=') {
            token.type = TokenType::GreaterThanEqual;
            next_char();
            token.value += current_char_;
        }
        else {
            token.type = TokenType::GreaterThan;
        }
    }
    else if(current_char_ == '!') {
        if(peek() == '=') {
            token.type = TokenType::NotEqual;
            next_char();
            token.value += current_char_;
        }
    }
    else if(current_char_ == '\"') {
        next_char();

        bool is_valid_string = true;
        while(current_char_ != '\"') {
            if(current_char_ == '\r' || current_char_ == '\n' || current_char_ == '\t' || current_char_ == '\\' || current_char_ == '%') {
                is_valid_string = false;
                break;
            }

            token.value += current_char_;
            next_char();
        }

        if(is_valid_string) {
            token.type = TokenType::String;
            token.value = token.value.substr(1);
        }
    }
    else if(std::isdigit(current_char_)) {
        bool is_valid_number = true;
        while(std::isdigit(peek())) {
            next_char();
            token.value += current_char_;
        }

        if(peek() == '.') {
            next_char();
            token.value += current_char_;

            if(std::isdigit(peek())) {
               while(std::isdigit(peek())) {
                    next_char();
                    token.value += current_char_;
               }
            }
            else {
                is_valid_number = false;
            }
        }

        if(is_valid_number) {
            token.type = TokenType::Number;
        }
    }
    else if(std::isalpha(current_char_)) {
        while(std::isalnum(peek())) {
            next_char();
            token.value += current_char_;
        }

        if(token.value == "LABEL") {
            token.type = TokenType::Label;
        }
        else if(token.value == "GOTO") {
            token.type = TokenType::Goto;
        }
        else if(token.value == "PRINT") {
            token.type = TokenType::Print;
        }
        else if(token.value == "INPUT") {
            token.type = TokenType::Input;
        }
        else if(token.value == "LET") {
            token.type = TokenType::Let;
        }
        else if(token.value == "IF") {
            token.type = TokenType::If;
        }
        else if(token.value == "THEN") {
            token.type = TokenType::Then;
        }
        else if(token.value == "ENDIF") {
            token.type = TokenType::EndIf;
        }
        else if(token.value == "WHILE") {
            token.type = TokenType::While;
        }
        else if(token.value == "REPEAT") {
            token.type = TokenType::Repeat;
        }
        else if(token.value == "ENDWHILE") {
            token.type = TokenType::EndWhile;
        }
        else {
            token.type = TokenType::Identifier;
        }
    }

    next_char();
    return token;
}

void Lexer::next_char() {
    current_pos_++;

    current_char_ = current_pos_ >= source_.size() ? '\0' : source_[current_pos_];
}

char Lexer::peek() {
    return current_pos_ + 1 >= source_.size() ? '\0' : source_[current_pos_ + 1];
}

void Lexer::skip_whitespace() {
    while(current_char_ == ' ' || current_char_ == '\t' || current_char_ == '\r') {
        next_char();
    }
}

void Lexer::skip_comment() {
    if(current_char_ != '#') {
        return;
    }

    while(current_char_ != '\n') {
        next_char();
    }
}

bool Lexer::is_valid_token(const Token &token) const {
    if(token.type == TokenType::NONE) {
        std::cerr << "Lexing error: ";

        if(token.value == "!") {
            std::cerr << "Expected !=, got !" << current_char_;
        }
        else if(token.value.ends_with('.')) {
            std::cerr << "Invalid character after fractional point [" << current_char_ << "]";
        }
        else {
            std::cerr << "Unknown token [" << token.value << "]";
        }

        return false;
    }

    return true;
}
