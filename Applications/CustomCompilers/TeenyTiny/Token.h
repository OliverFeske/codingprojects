//
// Created by Olive on 02/05/2024.
//

#ifndef TOKEN_H
#define TOKEN_H

#include <string>

enum class TokenType {
    /* Misc */
    EndOFile = 0,
    NewLine = 1,
    Number = 2,
    Identifier = 3,
    String = 4,

    /* Keywords */
    Label = 100,
    Goto = 103,
    Print = 104,
    Input = 105,
    Let = 106,
    If = 107,
    Then = 108,
    EndIf = 109,
    While = 110,
    Repeat = 111,
    EndWhile = 112,

    /* Operators */
    Equal = 200,
    Plus = 201,
    Minus = 202,
    Asterisk = 203,
    Slash = 204,
    EqualEqual = 205,
    NotEqual = 206,
    LessThan = 207,
    LessThanEqual = 208,
    GreaterThan = 209,
    GreaterThanEqual = 210,

    NONE = 255
};

class Token {
public:
    Token();
    Token(char in_value, TokenType in_type);

    static std::string token_type_to_string(const TokenType type);
    std::string to_string();


    std::string value;
    TokenType type;
};


#endif //TOKEN_H
