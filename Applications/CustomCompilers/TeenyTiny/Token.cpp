//
// Created by Olive on 02/05/2024.
//

#include "Token.h"

Token::Token()
        : value{}, type{TokenType::NONE} {

}

Token::Token(char value, TokenType type)
    : value{value}, type{type} {

}

std::string Token::token_type_to_string(const TokenType type) {
    switch(type) {
        case TokenType::EndOFile: return "EndOFile";
        case TokenType::NewLine: return "NewLine";
        case TokenType::Number: return "Number";
        case TokenType::Identifier: return "Identifier";
        case TokenType::String: return "String";
        case TokenType::Label: return "Label";
        case TokenType::Goto: return "Goto";
        case TokenType::Print: return "Print";
        case TokenType::Input: return "Input";
        case TokenType::Let: return "Let";
        case TokenType::If: return "If";
        case TokenType::Then: return "Then";
        case TokenType::EndIf: return "EndIf";
        case TokenType::While: return "While";
        case TokenType::Repeat: return "Repeat";
        case TokenType::EndWhile: return "EndWhile";
        case TokenType::Equal: return "Equal";
        case TokenType::Plus: return "Plus";
        case TokenType::Minus: return "Minus";
        case TokenType::Asterisk: return "Asterisk";
        case TokenType::Slash: return "Slash";
        case TokenType::EqualEqual: return "EqualEqual";
        case TokenType::NotEqual: return "NotEqual";
        case TokenType::LessThan: return "LessThan";
        case TokenType::LessThanEqual: return "LessThanEqual";
        case TokenType::GreaterThan: return "GreaterThan";
        case TokenType::GreaterThanEqual: return "GreaterThanEqual";
        case TokenType::NONE: return "NONE";
    }

    return "INVALID";
}

std::string Token::to_string() {
    std::string return_string{token_type_to_string(type)};

    switch(type) {
        case TokenType::Number:
        case TokenType::Identifier:
        case TokenType::String:
        case TokenType::Label:
            return_string += value;
            break;
        default:
            break;
    }

    return return_string;
}
