//
// Created by Olive on 04/05/2024.
//

#ifndef PARSER_H
#define PARSER_H

#include "Token.h"

#include <memory>
#include <map>
#include <functional>
#include <unordered_set>

class Lexer;
class Emitter;

class Parser {
public:
    Parser() = delete;
    explicit Parser(const std::shared_ptr<Lexer>& in_lexer, const std::shared_ptr<Emitter>& in_emitter);

    void start();

private:
    bool check_token(const TokenType type) const;
    bool match(const TokenType type);
    void next_token();
    bool is_comparison_operator() const;

    /** Grammar */
    void program();
    bool statement();
    bool expression();
    bool comparison();
    bool new_line();
    bool term();
    bool unary();
    bool primary();

    /** Statements */
    bool statement_print();
    bool statement_if();
    bool statement_while();
    bool statement_label();
    bool statement_goto();
    bool statement_let();
    bool statement_input();

    void print_error(const std::string& error_message);

    std::shared_ptr<Lexer> lexer_;
    std::shared_ptr<Emitter> emitter_;
    Token current_token_, peek_token_;
    std::map<TokenType, std::function<bool(Parser*)>> statement_function_map_;
    size_t current_line_;
    std::unordered_set<std::string> symbols_, declared_labels_, gotoed_labels_;
};

#endif //PARSER_H
