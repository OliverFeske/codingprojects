#include "Lexer.h"
#include "Parser.h"
#include "Emitter.h"

#include <iostream>
#include <fstream>
#include <sstream>

int main(int argc, char* argv[]) {
    std::string file_name{"../TestCode.teeny"};
    std::ifstream file(file_name);
    if(!file.is_open()) {
        std::cout << "File [" << file_name << "] could not be opened.\n";
        return 0;
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    std::shared_ptr<Lexer> lexer{std::make_shared<Lexer>(buffer.str())};
    std::shared_ptr<Emitter> emitter{std::make_shared<Emitter>("../out.c")};
    std::shared_ptr<Parser> parser{std::make_shared<Parser>(lexer, emitter)};

    parser->start();
    emitter->write_file();

    return 0;
}
