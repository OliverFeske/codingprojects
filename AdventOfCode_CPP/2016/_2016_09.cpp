//
// Created by Olive on 04/06/2024.
//

#include "_2016_09.h"

#include <iostream>
#include <sstream>

void _2016_09::run() {
    size_t index{0}, decompressed_length{0}, old_length{0};
    while(true) {
        old_length = decompressed_length;

        while(index < _input.size()) {
            if(_input[index] == '(') {
                size_t start{index};
                while(true) {
                    index++;
                    if(_input[index] == ')') {
                        break;
                    }
                }
                index++;

                std::string marker{_input.substr(start + 1, index - start - 1)};
                std::istringstream marker_stream{marker};

                size_t count{0}, repititions{0};
                char x{};
                marker_stream >> count >> x >> repititions;

                index += count;
                decompressed_length += count * repititions;
            }
            else {
                decompressed_length++;
                index++;
            }
        }

        if(old_length == decompressed_length) {
            break;
        }
    }

    std::cout << "Result: " << decompressed_length << std::endl;
}
