//
// Created by Olive on 02/06/2024.
//

#include <sstream>
#include <vector>
#include <iostream>
#include "_2016_07.h"

void _2016_07::run() {
    std::string line{};
    std::istringstream stream{_input};
    size_t supports_TLS{0}, supports_SSL{0};
    while(stream >> line) {
        std::vector<std::string> supernet_sequences{}, hypernet_sequences{};
        std::istringstream line_stream{line};
        std::string supernet_string{};
        while(std::getline(line_stream, supernet_string, '[')) {
            std::string hypernet_string{};
            std::getline(line_stream, hypernet_string, ']');

            supernet_sequences.push_back(supernet_string);
            hypernet_sequences.push_back(hypernet_string);
        }

        if(supernet_sequences[supernet_sequences.size() - 1].empty()) {
            supernet_sequences.pop_back();
        }

        if(hypernet_sequences[hypernet_sequences.size() - 1].empty()) {
            hypernet_sequences.pop_back();
        }

        bool has_abba_string{false};
        for(const std::string& string : supernet_sequences) {
            if(contains_ABBA(string)) {
                has_abba_string = true;
                break;
            }
        }

        bool has_forbidden_abba_string{false};
        for(const std::string& string : hypernet_sequences) {
            if(contains_ABBA(string)) {
                has_forbidden_abba_string = true;
                break;
            }
        }

        supports_TLS += has_abba_string && !has_forbidden_abba_string;

        std::vector<std::string> ABAs{};
        for(const std::string& string : supernet_sequences) {
            for(size_t i{0}; i < string.size() - 2; i++) {
                if(string[i] == string[i + 2] && string[i] != string[i + 1]) {
                    ABAs.push_back(string.substr(i, 3));
                }
            }
        }


        bool has_BAB{false};
        for(const std::string& string : hypernet_sequences) {
            if(has_BAB) {
                break;
            }

            for(size_t i{0}; i < string.size() - 2; i++) {
                if(has_BAB) {
                    break;
                }

                std::string substring{string.substr(i, 3)};
                for(const std::string& ABA : ABAs) {
                    if(is_BAB(ABA, substring)) {
                        has_BAB = true;
                        break;
                    }
                }
            }
        }

        supports_SSL += has_BAB;
    }

    std::cout << "Result TLS: " << supports_TLS << " | Result SSL: " << supports_SSL << std::endl;
}

bool _2016_07::contains_ABBA(const std::string &string) {
    for(size_t i{0}; i < string.size() - 3; i++) {
        if(string[i] != string[i + 1] && (string[i] == string[i + 3] && string[i + 1] == string[i + 2])) {
            return true;
        }
    }

    return false;
}

bool _2016_07::is_BAB(const std::string &ABA, const std::string& BAB) {
    return BAB[0] == ABA[1] && BAB[1] == ABA[0] && BAB[2] == ABA[1];
}
