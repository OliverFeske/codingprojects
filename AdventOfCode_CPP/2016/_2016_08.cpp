//
// Created by Olive on 02/06/2024.
//

#include "_2016_08.h"

#include <sstream>
#include <iostream>

void _2016_08::run() {
    _grid = std::vector<std::vector<bool>>(_rows, std::vector<bool>(_columns, false));

    std::string line{};
    std::istringstream stream{_input};
    while(std::getline(stream, line)) {
        std::istringstream line_stream{line};
        std::string instruction{};
        line_stream >> instruction;

        if(instruction == "rect") {
            std::string dimensions{};
            line_stream >> dimensions;

            size_t row_counter{static_cast<size_t>(std::stoi(dimensions.substr(0, dimensions.find('x'))))}, column_counter{static_cast<size_t>(dimensions.back() - 48)};
            std::cout << "Instruction: " << row_counter << "x" << column_counter << std::endl;

            for(size_t row{0}; row < row_counter; row++) {
                for(size_t column{0}; column < column_counter; column++) {
                    _grid[column][row] = true;
                }
            }
        }
        else if(instruction == "rotate") {
            std::string direction{}, start{}, by{};
            size_t amount{0}, ID{0};
            char x_or_y{}, equals{};
            line_stream >> direction >> x_or_y >> equals >> ID >> by >> amount;

            std::cout << "Instruction: " << ID << by << amount << std::endl;

            if(direction == "column") {
                std::vector<bool> vector{};
                for(size_t row{0}; row < _rows; row++) {
                    vector.push_back(_grid[row][ID]);
                }

                rotate_vector(vector, amount);

                for(size_t row{0}; row < _rows; row++) {
                    _grid[row][ID] = vector[row];
                }
            }
            else {
                    rotate_vector(_grid[ID], amount);
            }
        }

        display_grid(line);
    }

    size_t lit_pixels{0};
    for(const std::vector<bool>& row : _grid) {
        for(bool element : row) {
            lit_pixels += element;
        }
    }

    std::cout << std::endl << "Result: " << lit_pixels << std::endl;
}

void _2016_08::display_grid(const std::string& command) {
    std::cout << std::endl << "Grid after command: " << command << std::endl;
    for(const std::vector<bool>& row : _grid) {
        for(bool element : row) {
            std::cout << (element ? '#' : '.');
        }

        std::cout << std::endl;
    }
}

void _2016_08::rotate_vector(std::vector<bool> &vector, size_t num_rotations) {
    for(size_t current_rotation{0}; current_rotation < num_rotations; current_rotation++) {
        std::vector<bool> previous{vector};
        for(size_t i{0}; i < vector.size() - 1; i++) {
            vector[i + 1] = previous[i];
        }

        vector[0] = previous.back();
    }
}
