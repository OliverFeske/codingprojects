//
// Created by Olive on 30/05/2024.
//

#include "_2016_06.h"

#include <vector>
#include <sstream>
#include <iostream>
#include <map>

void _2016_06::run() {
    std::istringstream stream{_input};
    std::string row{};
    std::vector<std::string> columns(8, "");
    while(std::getline(stream, row)) {
        for(size_t i{0}; i < 8; i++) {
            columns[i].push_back(row[i]);
        }
    }

    std::string result{};
    for(const std::string& column : columns) {
        std::map<char, size_t> character_count_map{};
        for(char c : column) {
            if(!character_count_map.contains(c)) {
                character_count_map.insert_or_assign(c, 0);
            }

            character_count_map[c]++;
        }

        std::vector<std::pair<char, long>> column_count_sorted{};
        for(const auto& pair : character_count_map) {
            column_count_sorted.push_back(pair);
        }

        std::sort(column_count_sorted.begin(), column_count_sorted.end(), [](const std::pair<char, long>& a, const std::pair<char, long>& b){
            return a.second < b.second;
        });

        result.push_back(column_count_sorted[0].first);
    }

    std::cout << "Result: " << result << std::endl;
}
