//
// Created by Olive on 30/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2016_01_H
#define ADVENTOFCODE_CPP__2016_01_H

#include <string>

enum class Direction {
    North = 0,
    East = 1,
    South = 2,
    West = 3
};

class _2016_01 {
public:
    void run();

private:
    std::string _input{"L5, R1, L5, L1, R5, R1, R1, L4, L1, L3, R2, R4, L4, L1, L1, R2, R4, R3, L1, R4, L4, L5, L4, R4, L5, R1, R5, L2, R1, R3, L2, L4, L4, R1, L192, R5, R1, "
                       "R4, L5, L4, R5, L1, L1, R48, R5, R5, L2, R4, R4, R1, R3, L1, L4, L5, R1, L4, L2, L5, R5, L2, R74, R4, L1, R188, R5, L4, L2, R5, R2, L4, R4, R3, R3, R2, R1, "
                       "L3, L2, L5, L5, L2, L1, R1, R5, R4, L3, R5, L1, L3, R4, L1, L3, L2, R1, R3, R2, R5, L3, L1, L1, R5, L4, L5, R5, R2, L5, R2, L1, L5, L3, L5, L5, L1, R1, L4, "
                       "L3, L1, R2, R5, L1, L3, R4, R5, L4, L1, R5, L1, R5, R5, R5, R2, R1, R2, L5, L5, L5, R4, L5, L4, L4, R5, L2, R1, R5, L1, L5, R4, L3, R4, L2, R3, R3, R3, L2, "
                       "L2, L2, L1, L4, R3, L4, L2, R2, R5, L1, R2,"};
};


#endif //ADVENTOFCODE_CPP__2016_01_H
