//
// Created by Olive on 30/05/2024.
//

#include <sstream>
#include <vector>
#include <iostream>
#include "_2016_02.h"

void _2016_02::run() {
    std::stringstream stream{_input};
    std::string line{};
    int current_digit{5};
    std::vector<int> code;
    while(std::getline(stream, line)) {
        for(char c : line) {
            switch(c) {
                case 'U':
                    if(current_digit != 1 && current_digit != 2 && current_digit != 4 && current_digit != 5 && current_digit != 9) {
                        current_digit -= (current_digit == 3 || current_digit == 13) ? 2 : 4;
                    }
                    break;
                case 'D':
                    if(current_digit != 5 && current_digit != 9 && current_digit != 10 && current_digit != 12 && current_digit != 13) {
                        current_digit += (current_digit == 1 || current_digit == 11) ? 2 : 4;
                    }
                    break;
                case 'L':
                    if(current_digit != 1 && current_digit != 2 && current_digit != 5 && current_digit != 10 && current_digit != 13) {
                        current_digit -= 1;
                    }
                    break;
                case 'R':
                    if(current_digit != 1 && current_digit != 4 && current_digit != 9 && current_digit != 12 && current_digit != 13) {
                        current_digit += 1;
                    }
                    break;
                default:
                    break;
            }
        }

        code.push_back(current_digit);
    }

    std::cout << "Result: " << std::hex;
    for(int digit : code) {
        std::cout << digit << " ";
    }

    std::cout << std::endl;
}
