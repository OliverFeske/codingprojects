//
// Created by Olive on 30/05/2024.
//

#include "_2016_04.h"

#include <sstream>
#include <iostream>
#include <vector>
#include <map>

void _2016_04::run() {
    std::istringstream stream{_input};
    std::string line{};
    size_t sum_sector_IDs{0};
    while(std::getline(stream, line)) {
        std::istringstream line_stream{line};
        std::string part{}, element{};

        while(std::getline(line_stream, part, '-')) {
            if(line_stream.eof()) {
                size_t sector_ID{0};
                std::string check_sum{};

                std::stringstream part_stream{part};
                part_stream >> sector_ID >> check_sum;

                std::map<char, long> element_map{};
                for(char c : element) {
                    if(!element_map.contains(c)) {
                        element_map.insert_or_assign(c, 0);
                    }

                    element_map[c]++;
                }

                std::vector<std::pair<char, long>> element_sorted{};
                for(auto&& [key, value] : element_map) {
                    element_sorted.push_back({key, value});
                }

                std::sort(element_sorted.begin(), element_sorted.end(), [](const std::pair<char, long>& p1, const std::pair<char, long>& p2){
                    return p1.second > p2.second || (p1.second == p2.second && p1.first < p2.first);
                });

                bool is_real{true};
                for(size_t i{0}; i < 5; i++) {
                    if(element_sorted[i].first != check_sum[i + 1]) {
                        is_real = false;
                        break;
                    }
                }

                if(is_real) {
                    sum_sector_IDs += sector_ID;
                }

                std::string decyphered_element{element};
                size_t rotations{sector_ID};
                for(char& c : decyphered_element) {
                    for(size_t i{0}; i < rotations; i++) {
                        c = _alphabet_map[c];
                    }
                }

                if(decyphered_element.contains("northpole")) {
                    std::cout << "North Pole: " << sector_ID << std::endl;
                }
            }
            else {
                element.append(part);
            }
        }
    }

    std::cout << "Result: " << sum_sector_IDs << std::endl;
}
