//
// Created by Olive on 30/05/2024.
//

#include "_2016_01.h"

#include <sstream>
#include <iostream>
#include <set>
#include <vector>

void _2016_01::run() {
    std::istringstream stream{_input};

    Direction direction{Direction::North};
    std::string instruction{};
    int horizontal{0}, vertical{0};
    std::set<std::pair<int, int>> visited_locations{{0, 0}};
    while(stream >> instruction) {
        instruction.pop_back();

        if(instruction.front() == 'R') {
            direction = direction == Direction::West ? Direction::North : static_cast<Direction>(static_cast<int>(direction) + 1);
        }
        else {
            direction = direction == Direction::North ? Direction::West : static_cast<Direction>(static_cast<int>(direction) - 1);
        }

        int steps{std::stoi(instruction.substr(1))};

        switch(direction) {
            case Direction::North:
                for(int i{1}; i <= steps; i++) {
                    if(!visited_locations.insert({horizontal + i, vertical}).second) {
                        std::cout << "Found duplicate: h " << horizontal + i << " | v " << vertical << " | steps: " << std::abs(horizontal + i) + std::abs(vertical) << std::endl;
                    }
                }

                horizontal += steps;
                break;
            case Direction::East:
                for(int i{1}; i <= steps; i++) {
                    if(!visited_locations.insert({horizontal, vertical + i}).second) {
                        std::cout << "Found duplicate: h " << horizontal << " | v " << vertical + i << " | steps: " << std::abs(horizontal) + std::abs(vertical + i) << std::endl;
                    }
                }

                vertical += steps;
                break;
            case Direction::South:
                for(int i{1}; i <= steps; i++) {
                    if(!visited_locations.insert({horizontal - i, vertical}).second) {
                        std::cout << "Found duplicate: h " << horizontal - i << " | v " << vertical << " | steps: " << std::abs(horizontal - i) + std::abs(vertical) << std::endl;
                    }
                }

                horizontal -= steps;
                break;
            case Direction::West:
                for(int i{1}; i <= steps; i++) {
                    if(!visited_locations.insert({horizontal, vertical - i}).second) {
                        std::cout << "Found duplicate: h " << horizontal << " | v " << vertical - i << " | steps: " << std::abs(horizontal) + std::abs(vertical - i) << std::endl;
                    }
                }

                vertical -= steps;
                break;
        }
    }

    std::cout << "Steps away: " << std::abs(horizontal) + std::abs(vertical) << std::endl;
}
