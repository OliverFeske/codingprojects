//
// Created by Olive on 30/05/2024.
//

#include "_2016_03.h"

#include <sstream>
#include <iostream>

void _2016_03::run() {
    std::string line{};
    std::istringstream stream{_input};
    size_t possible_triangles{0};
    while(std::getline(stream, line)) {
        std::istringstream line_stream{line};
        size_t a{0}, b{0}, c{0};

        /* Part 1
        line_stream >> a >> b >> c;

        possible_triangles += (a + b > c) && (b + c > a) && (c + a > b);
         */

        std::string line2{}, line3{};
        std::getline(stream, line2);
        std::getline(stream, line3);
        std::istringstream line_stream2{line2}, line_stream3{line3};

        for(int i{0}; i < 3; i++) {
            line_stream >> a;
            line_stream2 >> b;
            line_stream3 >> c;

            possible_triangles += (a + b > c) && (b + c > a) && (c + a > b);
        }
    }

    std::cout << "Result: " << possible_triangles << std::endl;
}
