//
// Created by Olive on 14/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_10_H
#define ADVENTOFCODE_CPP__2015_10_H

#include <string>

class _2015_10 {
public:
    void run();

private:
    void generate_new_result();

    std::string _current_result;
};


#endif //ADVENTOFCODE_CPP__2015_10_H
