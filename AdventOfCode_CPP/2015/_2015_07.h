//
// Created by Olive on 13/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_07_H
#define ADVENTOFCODE_CPP__2015_07_H

#include <string>
#include <vector>
#include <map>

class _2015_07 {
public:
    void run();

private:
    bool is_value_or_assigned(const std::string& symbol_name) const;
    unsigned short get_value(const std::string& symbol_name) const;
    void initialize_symbol(const std::string& symbol_name, unsigned short value);
    void assignment(const std::string& symbol_name);

    std::vector<std::string> _lines;
    std::map<std::string, unsigned short> _initialized_symbols;
};


#endif //ADVENTOFCODE_CPP__2015_07_H
