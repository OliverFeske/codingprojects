//
// Created by Olive on 15/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_16_H
#define ADVENTOFCODE_CPP__2015_16_H

#include <vector>
#include <map>
#include <string>

class _2015_16 {
public:
    void run();
};


#endif //ADVENTOFCODE_CPP__2015_16_H
