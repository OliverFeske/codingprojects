//
// Created by Olive on 14/05/2024.
//

#include "_2015_12.h"

#include <iostream>
#include <fstream>

void _2015_12::run() {
    std::ifstream file{"../input_2015_12.json"};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    nlohmann::json file_json = nlohmann::json::parse(file);

    resolve_object(file_json);

    file.close();

    std::cout << "Result: " << _result << "\n";
}

void _2015_12::resolve_unknown(const nlohmann::json &json_any) {
    if(json_any.is_object()) {
        resolve_object(json_any);
    }
    else if(json_any.is_array()) {
        resolve_array(json_any);
    }
    else if(json_any.is_number()) {
        resolve_number(json_any);
    }
    else if(!json_any.is_string()) {
        std::cout << "Unresolved json: " << json_any.type_name() << "\n";
    }
}

void _2015_12::resolve_object(const nlohmann::json &json_object) {
    for(const nlohmann::json& json_unknown : json_object) {
        if(json_unknown.is_string() && json_unknown.get<std::string>() == "red") {
            std::cout << "Ignored\n";
            return;
        }
    }

    for(const nlohmann::json& json_unknown : json_object) {
        resolve_unknown(json_unknown);
    }
}

void _2015_12::resolve_array(const nlohmann::json &json_array) {
    for(const nlohmann::json& json_unknown : json_array) {
        resolve_unknown(json_unknown);
    }
}

void _2015_12::resolve_number(const nlohmann::json &json_number) {
    _result += json_number.get<int>();
}

