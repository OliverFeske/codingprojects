//
// Created by Olive on 19/05/2024.
//

#include <fstream>
#include <iostream>
#include "_2015_18.h"

void _2015_18::run() {
    std::ifstream file{"../input_2015_18.txt", std::ifstream::in};
    if (!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    _grid_1.reserve(_size + 2);

    std::vector<bool> edge_line(_size + 2, false);

    _grid_1.push_back(edge_line);

    size_t current_line_index{0};
    while(!file.eof()) {
        std::string line{};
        std::getline(file, line);

        std::vector<bool> line_in_grid(_size + 2, false);
        _grid_1.push_back(line_in_grid);

        for(size_t i{1}; i < _size + 1; i++) {
            _grid_1[current_line_index + 1][i] = line[i - 1] == '#';
        }

        current_line_index++;
    }

    _grid_1.push_back(edge_line);
    _grid_1[1][1] = _grid_1[1][_size] = _grid_1[_size][1] = _grid_1[_size][_size] = true;

    _grid_2 = _grid_1;

    for(size_t  current_step{0}; current_step < _steps; current_step++) {
        if(current_step % 2 == 0) {
            _active_grid = &_grid_1;
            _sample_grid = &_grid_2;
        }
        else {
            _active_grid = &_grid_2;
            _sample_grid = &_grid_1;
        }

        for(size_t y{1}; y < _size + 1; y++) {
            for(size_t x{1}; x < _size + 1; x++) {
                if((y == 1 && x == 1) || (y == 1 && x == _size) || (y == _size && x == 1) || (y == _size && x == _size)) {
                    continue;
                }

                size_t left{(*_sample_grid)[y][x - 1]},
                    right{(*_sample_grid)[y][x + 1]},
                    top{(*_sample_grid)[y - 1][x]},
                    bottom{(*_sample_grid)[y + 1][x]},
                    top_left{(*_sample_grid)[y - 1][x - 1]},
                    top_right{(*_sample_grid)[y - 1][x + 1]},
                    bottom_right{(*_sample_grid)[y + 1][x + 1]},
                    bottom_left{(*_sample_grid)[y + 1][x - 1]};

                size_t num_on_neighbors(left + right + top + bottom + top_left + top_right + bottom_right + bottom_left);

                if((*_sample_grid)[y][x]) {
                    (*_active_grid)[y][x] = num_on_neighbors == 2 || num_on_neighbors == 3;
                }
                else {
                    (*_active_grid)[y][x] = num_on_neighbors == 3;
                }
            }
        }
    }

    for(size_t y{1}; y < _size + 1; y++) {
        for(size_t x{1}; x < _size + 1; x++) {
            _num_turned_on += (*_active_grid)[y][x];
        }
    }

    std::cout << "Result: " << _num_turned_on << "\n";

    print_grid(100);
}

void _2015_18::print_grid(size_t step) {
    std::cout << "Step: " << step << std::endl;
    for(size_t y{0}; y < _size + 2; y++) {
        for(size_t x{0}; x < _size + 2; x++) {
            std::cout << ((*_active_grid)[y][x] ? '#' : '.');
        }

        std::cout << std::endl << std::endl;
    }
}