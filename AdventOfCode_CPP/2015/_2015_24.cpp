//
// Created by Olive on 29/05/2024.
//

#include "_2015_24.h"

#include <iostream>
#include <numeric>

void _2015_24::run() {
    std::reverse(_packages.begin(), _packages.end());

    build_group(0, {}, 0);

    for(const std::vector<size_t>& possible_solution : _possible_solutions) {
        size_t quantum_entanglement{std::accumulate(possible_solution.begin(), possible_solution.end(), 1ul, std::multiplies())};

        if(quantum_entanglement < _current_lowest_quantum_entanglement) {
            _current_lowest_quantum_entanglement = quantum_entanglement;
        }
    }

    std::cout << "Lowest QE: " << _current_lowest_quantum_entanglement << std::endl;
}

void _2015_24::build_group(size_t current_index, const std::vector<size_t>& current_group, size_t current_sum) {
    for(size_t i{current_index}; i < 29; i++) {
        size_t new_sum{current_sum + _packages[i]};
        if(new_sum < _group_sum && current_group.size() < _current_lowest_number_first_packages) {
            std::vector<size_t> new_group{current_group};
            new_group.push_back(_packages[i]);

            build_group(i + 1, new_group, new_sum);
        }
        else if(new_sum == _group_sum && current_group.size() + 1 <= _current_lowest_number_first_packages) {
            std::vector<size_t> new_group{current_group};
            new_group.push_back(_packages[i]);

            if(new_group.size() < _current_lowest_number_first_packages) {
                _current_lowest_number_first_packages = new_group.size();
                _possible_solutions.clear();
            }

            _possible_solutions.push_back(new_group);
        }
    }
}
