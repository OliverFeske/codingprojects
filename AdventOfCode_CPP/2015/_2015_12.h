//
// Created by Olive on 14/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_12_H
#define ADVENTOFCODE_CPP__2015_12_H


#include "../libs/json.hpp"

class _2015_12 {
public:
    void run();

private:
    void resolve_unknown(const nlohmann::json& json_any);
    void resolve_object(const nlohmann::json& json_object);
    void resolve_array(const nlohmann::json& json_array);
    void resolve_number(const nlohmann::json& json_number);

    int _result{0};
};


#endif //ADVENTOFCODE_CPP__2015_12_H
