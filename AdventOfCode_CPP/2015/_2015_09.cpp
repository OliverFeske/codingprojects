//
// Created by Olive on 14/05/2024.
//

#include "_2015_09.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <limits>

void _2015_09::run() {
    std::ifstream file{"../input_2015_09.txt", std::ifstream::in};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    while(!file.eof()) {
        std::string line{};
        std::getline(file, line);

        std::string location_1{}, location_2{}, to{}, equal{};
        size_t distance{0};
        std::istringstream stream{line};
        stream >> location_1 >> to >> location_2 >> equal >> distance;

        if(!_location_distances.contains(location_1)) {
            _location_distances.insert_or_assign(location_1, std::map<std::string, size_t>{});
        }

        if(!_location_distances.contains(location_2)) {
            _location_distances.insert_or_assign(location_2, std::map<std::string, size_t>{});
        }

        _location_distances[location_1].insert_or_assign(location_2, distance);
        _location_distances[location_2].insert_or_assign(location_1, distance);

        if(_locations.end() == std::find(_locations.begin(), _locations.end(), location_1)) {
            _locations.push_back(location_1);
        }

        if(_locations.end() == std::find(_locations.begin(), _locations.end(), location_2)) {
            _locations.push_back(location_2);
        }

        std::cout << location_1 << " " << location_2 << " " << distance << "\n";
    }

    file.close();

    std::sort(_locations.begin(), _locations.end());

    do {
        size_t distance{0};
        for(size_t i{0}; i < _locations.size() - 1; i++) {
            distance += _location_distances[_locations[i]][_locations[i + 1]];
        }

        _permutations_with_final_distance.push_back(std::pair<std::vector<std::string>, size_t>(_locations, distance));
    } while(std::next_permutation(_locations.begin(), _locations.end()));

    std::cout << "Number permutations: " << _permutations_with_final_distance.size() << "\n";

    size_t shortest_distance{std::numeric_limits<size_t>::max()};
    for(auto& pair : _permutations_with_final_distance) {
        if(pair.second < shortest_distance) {
            shortest_distance = pair.second;
        }
    }

    std::cout << "Shortest path: " << shortest_distance << "\n";

    size_t longest_distance{};
    for(auto& pair : _permutations_with_final_distance) {
        if(pair.second > longest_distance) {
            longest_distance = pair.second;
        }
    }

    std::cout << "Longest path: " << longest_distance << "\n";
}

