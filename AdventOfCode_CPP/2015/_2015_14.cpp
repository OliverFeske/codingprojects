//
// Created by Olive on 15/05/2024.
//

#include "_2015_14.h"

#include <fstream>
#include <iostream>
#include <sstream>

void _2015_14::run() {
    std::ifstream file{"../input_2015_14.txt", std::ifstream::in};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    while(!file.eof()) {
        std::string line{};
        std::getline(file, line);

        std::string irrelevant{}, name{};
        size_t speed{0}, fly_time{0}, rest_time{0};
        std::istringstream stream{line};
        stream >> name >> irrelevant >> irrelevant >> speed >> irrelevant >> irrelevant >> fly_time >> irrelevant >> irrelevant
            >> irrelevant >> irrelevant >> irrelevant >> irrelevant >> rest_time >> irrelevant;

        Reindeer reindeer{name, speed, fly_time, rest_time};
        _reindeers_default.push_back(reindeer);
        _reindeers_active.push_back(std::pair<Reindeer, std::pair<size_t, size_t>>(reindeer, std::pair<size_t, size_t>(0, 0)));
    }

    file.close();

    size_t seconds{0}, current_farthest_distance{0};
    while(seconds < 2503) {
        for(size_t i{0}; i < _reindeers_active.size(); i++) {
            Reindeer& reindeer{_reindeers_active[i].first};

            if(reindeer.flying_time > 0) {
                reindeer.flying_time--;

                _reindeers_active[i].second.first += reindeer.speed;

                if(current_farthest_distance < _reindeers_active[i].second.first) {
                    current_farthest_distance = _reindeers_active[i].second.first;
                }
            }
            else if(reindeer.rest_time > 0) {
                reindeer.rest_time--;
                if(reindeer.rest_time == 0) {
                    reindeer.flying_time = _reindeers_default[i].flying_time;
                    reindeer.rest_time = _reindeers_default[i].rest_time;
                }
            }
        }

        /** Award points */
        for(auto&& [reindeer, pair] : _reindeers_active) {
            if(current_farthest_distance == pair.first) {
                pair.second++;
            }
        }

        seconds++;
    }

    std::cout << "After " << seconds << " seconds:\n";

    for(auto&& [reindeer, pair] : _reindeers_active) {
        std::cout << reindeer.name << " traveled " << pair.first << " km and gathered " << pair.second << " points.\n";
    }
}
