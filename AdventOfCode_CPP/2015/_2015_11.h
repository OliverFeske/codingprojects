//
// Created by Olive on 14/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_11_H
#define ADVENTOFCODE_CPP__2015_11_H

#include <string>
#include <map>

class _2015_11 {
public:
    void run();

private:
    void increment();
    bool is_valid();

    std::string _current_password;
    std::map<char, char> _letter_sequence_table{
            {'a', 'b'},
            {'b', 'c'},
            {'c', 'd'},
            {'d', 'e'},
            {'e', 'f'},
            {'f', 'g'},
            {'g', 'h'},
            {'h', 'j'},
            {'j', 'k'},
            {'k', 'm'},
            {'m', 'n'},
            {'n', 'p'},
            {'p', 'q'},
            {'q', 'r'},
            {'r', 's'},
            {'s', 't'},
            {'t', 'u'},
            {'u', 'v'},
            {'v', 'w'},
            {'w', 'x'},
            {'x', 'y'},
            {'y', 'z'},
            {'z', 'a'},
    };
};


#endif //ADVENTOFCODE_CPP__2015_11_H
