//
// Created by Olive on 14/05/2024.
//

#include <iostream>
#include "_2015_11.h"

void _2015_11::run() {
    _current_password = "vzbxxyzz";

    increment();

    size_t count{0};
    while(!is_valid()) {
        increment();
        count++;
    }

    std::cout << "Result: " << _current_password << "\n";
}

void _2015_11::increment() {
    int character_index{8};
    bool has_carry{true};
    while(has_carry && character_index >= 0) {
        character_index--;

        _current_password[character_index] = _letter_sequence_table[_current_password[character_index]];

        has_carry = _current_password[character_index] == 'a';
    }
}

bool _2015_11::is_valid() {
    bool has_increasing_sequence{false};
    for(size_t i{0}; i < 6; i++) {
        char c1{_current_password[i]}, c2{_current_password[i + 1]}, c3{_current_password[i + 2]};

        if(c2 != 'a' && c3 != 'a' && c2 == _letter_sequence_table[c1] && c3 == _letter_sequence_table[c2]) {
            has_increasing_sequence = true;
            break;
        }
    }

    if(!has_increasing_sequence) {
        return false;
    }

    bool has_pairs{false};
    char found_pair_character{'\n'};
    for(size_t i{0}; i < 7; i++) {
        if(_current_password[i] == _current_password[i + 1] && _current_password[i] != found_pair_character) {
            if(found_pair_character == '\n') {
                found_pair_character = _current_password[i];
            }
            else {
                has_pairs = true;
                break;
            }
        }
    }

    return has_pairs;
}
