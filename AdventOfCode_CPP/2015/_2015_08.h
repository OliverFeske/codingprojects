//
// Created by Olive on 14/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_08_H
#define ADVENTOFCODE_CPP__2015_08_H

#include <string>
#include <vector>

class _2015_08 {
public:
    void run();

private:
    void calculate_number(const std::string& line);

    size_t _result{0}, _result_b{0};
};


#endif //ADVENTOFCODE_CPP__2015_08_H
