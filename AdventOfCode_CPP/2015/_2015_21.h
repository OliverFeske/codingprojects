//
// Created by Olive on 28/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_21_H
#define ADVENTOFCODE_CPP__2015_21_H

#include <vector>

struct Item {
    int cost{0}, damage{0}, armor{0};
};

struct Entity {
    int damage{0}, armor{0}, health{0};
};

class _2015_21 {
public:
    void run();

private:
    bool fight_boss();

    int _current_lowest_cost{100'000}, _current_highest_cost{0};
    Entity _player{}, _boss{8, 2, 100};
    std::vector<Item> _weapons{
            {8,  4, 0},
            {10, 5, 0},
            {25, 6, 0},
            {40, 7, 0},
            {74, 8, 0}
    };

    std::vector<Item> _armor{
            {},
            {13, 0, 1},
            {31, 0, 2},
            {53, 0, 3},
            {75, 0, 4},
            {102, 0, 5},
    };

    std::vector<Item> _rings{
            {},
            {},
            {25, 1, 0},
            {50, 2, 0},
            {100, 3, 0},
            {20, 0, 1},
            {40, 0, 2},
            {80, 0, 3},
    };
};


#endif //ADVENTOFCODE_CPP__2015_21_H
