//
// Created by Olive on 28/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_22_H
#define ADVENTOFCODE_CPP__2015_22_H

#include <limits>
#include <vector>

enum class Action {
    MagicMissile,
    Drain,
    Shield,
    Poison,
    Recharge
};

class GameState {
public:
    int mana{500}, player_health{50}, enemy_health{71}, shield_left{0}, enemy_damage{10};
    size_t poison_left{0}, recharge_left{0}, spent_mana{0};
    std::vector<Action> action_stack;
};

class _2015_22 {
public:
    void run();

private:
    void player_turn(GameState game_state);
    void magic_missile(GameState game_state);
    void drain(GameState game_state);
    void shield(GameState game_state);
    void poison(GameState game_state);
    void recharge(GameState game_state);

    void apply_effects(GameState& game_state);
    void enemy_turn(GameState game_state);

    size_t _current_minimum_mana_spent{std::numeric_limits<size_t>::max()};
};


#endif //ADVENTOFCODE_CPP__2015_22_H
