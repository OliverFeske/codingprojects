//
// Created by Olive on 15/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_14_H
#define ADVENTOFCODE_CPP__2015_14_H

#include <string>
#include <vector>
#include <map>

struct Reindeer {
    std::string name{};
    size_t speed{0}, flying_time{0}, rest_time{0};
};

class _2015_14 {
public:
    void run();

private:
    std::vector<Reindeer> _reindeers_default;
    std::vector<std::pair<Reindeer, std::pair<size_t, size_t>>> _reindeers_active;
};


#endif //ADVENTOFCODE_CPP__2015_14_H
