//
// Created by Olive on 14/05/2024.
//

#include <iostream>
#include <vector>
#include "_2015_10.h"

void _2015_10::run() {
    _current_result = "3113322113";

    for(size_t i{0}; i < 50; i++) {
        generate_new_result();
    }

    std::cout << "Result: " << _current_result.size() << "\n";
}

void _2015_10::generate_new_result() {
    std::string new_result;

    char current_character{_current_result[0]};
    std::vector<std::pair<char, size_t>> same_number_sequences{std::pair<char, size_t>{current_character, 1}};
    size_t i{1}, current_sequence_index{0};

    while(i < _current_result.size()) {
        if(_current_result[i] != current_character) {
            current_character = _current_result[i];
            same_number_sequences.push_back(std::pair<char, size_t>{current_character, 1});
            current_sequence_index++;
        }
        else {
            same_number_sequences[current_sequence_index].second++;
        }

        i++;
    }

    for(auto& pair : same_number_sequences) {
        new_result += std::to_string(pair.second) + pair.first;
    }

    _current_result = new_result;
}
