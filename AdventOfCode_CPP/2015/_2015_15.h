//
// Created by Olive on 15/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_15_H
#define ADVENTOFCODE_CPP__2015_15_H

#include <vector>

struct Ingredient {
    int capacity{0}, durability{0}, flavor{0}, texture{0}, calories{0};
};

class _2015_15 {
public:
    void run();
};


#endif //ADVENTOFCODE_CPP__2015_15_H
