//
// Created by Olive on 13/05/2024.
//

#include "_2015_06.h"

#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>

void _2015_06::run() {
    std::ifstream file{"../input_2015_06.txt", std::ifstream::in};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    std::vector<std::vector<size_t>> grid;
    grid.resize(1000);
    size_t i = 0;
    while(i < 1000) {
        grid[i].resize(1000);
        size_t j = 0;
        while(j < 1000) {
            grid[i][j] = 0;
            j++;
        }
        i++;
    }

    while(!file.eof()) {
        std::string line;
        std::getline(file, line);
        std::istringstream stream(line);

        std::string operation, through;
        char comma;
        size_t min_x, max_x, min_y, max_y;

        stream >> operation;
        if(operation != "toggle") {
            stream >> operation;
        }

        stream >> min_x >> comma >> min_y >> through >> max_x >> comma >> max_y;

        size_t current_x = min_x;
        while(current_x <= max_x) {
            size_t current_y = min_y;
            while(current_y <= max_y) {
                if(operation == "toggle") {
                    //grid[current_x][current_y] = grid[current_x][current_y] == 0 ? 1 : 0;
                    grid[current_x][current_y] += 2;
                } else if (operation == "on") {
                    //grid[current_x][current_y] = 1;
                    grid[current_x][current_y] += 1;
                } else if(operation == "off") {
                    //grid[current_x][current_y] = 0;
                    if(grid[current_x][current_y] > 0) {
                        grid[current_x][current_y] -= 1;
                    }
                }
                current_y++;
            }
            current_x++;
        }
    }

    file.close();

    size_t lit_counter = 0;
    i = 0;
    while(i < 1000) {
        size_t j  = 0;
        while(j < 1000) {
            lit_counter += grid[i][j];
            j++;
        }
        i++;
    }

    std::cout << "lit counter: " << lit_counter << "\n";
}
