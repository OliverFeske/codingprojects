//
// Created by Olive on 16/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_17_H
#define ADVENTOFCODE_CPP__2015_17_H

#include <vector>

class _2015_17 {
public:
    void run();

private:
    void add_up(int current_index, int current_sum, int num_used_containers);

    char _containers[20]{1,6,13,13,14,16,18,18,20,24,30,33,35,35,41,42,44,45,48,50};
    int _num_found_combinations{0}, _num_found_combinations_2{0}, _current_num_min_containers{20};
};


#endif //ADVENTOFCODE_CPP__2015_17_H
