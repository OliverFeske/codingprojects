//
// Created by Olive on 14/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_13_H
#define ADVENTOFCODE_CPP__2015_13_H


#include <vector>
#include <string>
#include <map>

class _2015_13 {
public:
    void run();

private:
    std::map<std::string, std::map<std::string, int>> _happiness_table;
    std::vector<std::pair<std::vector<std::string>, int>> _permutations_with_total_happiness;
    std::vector<std::string> _people;
};


#endif //ADVENTOFCODE_CPP__2015_13_H
