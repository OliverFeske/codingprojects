//
// Created by Olive on 29/05/2024.
//

#include <fstream>
#include <iostream>
#include <sstream>
#include "_2015_23.h"

void _2015_23::run() {
    std::ifstream file{"../input_2015_23.txt", std::ifstream::in};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    while(!file.eof()) {
        std::string line{};
        std::getline(file, line);
        _instructions.push_back(line);
    }

    file.close();

    int current_index{0};

    while(true) {
        if(current_index >= _instructions.size()) {
            break;
        }

        std::istringstream stream{_instructions[current_index]};
        std::string instruction{}, comma{};
        char reg{};
        int jump_amount{0};

        stream >> instruction;

        if(instruction == "hlf") {
            stream >> reg;
            if(reg == 'a') {
                _a /= 2;
            }
            else {
                _b /= 2;
            }

            current_index++;
        }
        else if(instruction == "tpl") {
            stream >> reg;
            if(reg == 'a') {
                _a *= 3;
            }
            else {
                _b *= 3;
            }

            current_index++;
        }
        else if(instruction == "inc") {
            stream >> reg;
            if(reg == 'a') {
                _a++;
            }
            else {
                _b++;
            }

            current_index++;
        }
        else if(instruction == "jmp") {
            stream >> jump_amount;
            current_index += jump_amount;
        }
        else if(instruction == "jie") {
            stream >> reg >> comma >> jump_amount;

            if((reg == 'a' ? _a : _b ) % 2 == 0) {
                current_index += jump_amount;
            }
            else {
                current_index++;
            }
        }
        else if(instruction == "jio") {
            stream >> reg >> comma >> jump_amount;

            if((reg == 'a' ? _a : _b ) == 1) {
                current_index += jump_amount;
            }
            else {
                current_index++;
            }
        }
        else {
            std::cout << "Unknown instruction: " << instruction << std::endl;
        }
    }

    std::cout << "Result: " << _b << std::endl;
}
