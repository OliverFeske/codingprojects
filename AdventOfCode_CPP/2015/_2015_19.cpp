//
// Created by Olive on 19/05/2024.
//

#include <fstream>
#include <iostream>
#include <sstream>
#include "_2015_19.h"

void _2015_19::run() {
    std::ifstream file{"../input_2015_19.txt", std::ifstream::in};
    if (!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    for(size_t i{0}; i < 43; i++) {
        std::string line{};
        std::getline(file, line);

        std::string ls{}, arrow{}, rs{};
        std::istringstream stream{line};
        stream >> ls >> arrow >> rs;

        if(!_replacement_options.contains(ls)) {
            _replacement_options.insert_or_assign(ls, std::vector<std::string>{});
        }

        _replacement_options[ls].push_back(rs);

        /** todo: store them in a  map for sorting */
    }

    std::getline(file, _molecule);

    size_t count{0};

    for(auto&& [base, replacements] : _replacement_options) {
        for(const std::string& replacement : replacements) {
            size_t starting_index{0};

            while(true) {
                size_t found_index{_molecule.find(base, starting_index)};
                if(found_index == std::string::npos) {
                    break;
                }

                _distinct_molecules.insert(std::string{_molecule.substr(0, found_index) + replacement + _molecule.substr(found_index + base.size())});

                starting_index = found_index + base.size();

                count++;
            }
        }
    }

    std::cout << "Result: " << _distinct_molecules.size() << " | Count: " << count << std::endl;

    while(true) {
        size_t old_step_count{0};

        for(auto&& [base, replacements] : _replacement_options) {
            for(const std::string& replacement : replacements) {
                replace_token(replacement, base);
            }
        }

        if(old_step_count == _step_count) {
            std::cout << "Result 2: " << _step_count << std::endl;
            break;
        }
    }
}

void _2015_19::replace_token(const std::string &base, const std::string &replacement) {
    size_t starting_index{0};

    while(true) {
        size_t found_index{_molecule.find(base, starting_index)};
        if(found_index == std::string::npos) {
            break;
        }

        _molecule.replace(found_index, base.size(), replacement);

        _step_count++;
    }

    std::cout << _molecule << std::endl;
}
