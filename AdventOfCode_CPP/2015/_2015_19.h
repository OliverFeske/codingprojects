//
// Created by Olive on 19/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_19_H
#define ADVENTOFCODE_CPP__2015_19_H

#include <set>
#include <string>
#include <map>
#include <vector>

class _2015_19 {
public:
    void run();

private:
    void replace_token(const std::string& base, const std::string& replacement);

    std::set<std::string> _distinct_molecules;
    std::map<std::string, std::vector<std::string>> _replacement_options;
    std::string _molecule;
    size_t _step_count{0};
};


#endif //ADVENTOFCODE_CPP__2015_19_H
