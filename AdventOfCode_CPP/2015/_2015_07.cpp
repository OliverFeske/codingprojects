//
// Created by Olive on 13/05/2024.
//

#include "_2015_07.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <iterator>

void _2015_07::run() {
    std::ifstream file{"../input_2015_07.txt", std::ifstream::in};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    while(!file.eof()) {
        std::string line{};
        std::getline(file, line);
        _lines.push_back(line);
    }

    file.close();

    _lines[_lines.size() - 1] += "\r";

    assignment("a");

    std::cout << "Finished! a: " << _initialized_symbols["a"] << "\n";
}

bool _2015_07::is_value_or_assigned(const std::string &symbol_name) const {
    return std::isdigit(symbol_name[0]) || _initialized_symbols.contains(symbol_name);
}

unsigned short _2015_07::get_value(const std::string &symbol_name) const {
    return std::isdigit(symbol_name[0]) ? static_cast<unsigned short>(std::stoul(symbol_name)) : _initialized_symbols.at(symbol_name);
}

void _2015_07::initialize_symbol(const std::string &symbol_name, unsigned short value) {
    _initialized_symbols.insert_or_assign(symbol_name, value);
    std::cout << "Assigned " << value << " to " << symbol_name << "\n";
}

void _2015_07::assignment(const std::string &symbol_name) {
    auto itr = std::find_if(_lines.begin(), _lines.end(), [&symbol_name](const std::string& s){
        return s.contains(" -> " + symbol_name + "\r");
    });

    if(_lines.end() == itr) {
        std::cout << "Could not find: " << symbol_name << "\n";
        return;
    }

    std::string line{*itr};
    std::istringstream stream{line};
    std::vector<std::string> tokens{std::istream_iterator<std::string>(stream), {}};

    unsigned short value{0};
    if(line.contains("AND")) {
        if(!is_value_or_assigned(tokens[0])) {
            assignment(tokens[0]);
        }

        if(!is_value_or_assigned(tokens[2])) {
            assignment(tokens[2]);
        }

        value = get_value(tokens[0]) & get_value(tokens[2]);
    }
    else if(line.contains("OR")) {
        if(!is_value_or_assigned(tokens[0])) {
            assignment(tokens[0]);
        }

        if(!is_value_or_assigned(tokens[2])) {
            assignment(tokens[2]);
        }

        value = get_value(tokens[0]) | get_value(tokens[2]);
    }
    else if(line.contains("LSHIFT")) {
        if(!is_value_or_assigned(tokens[0]))  {
            assignment(tokens[0]);
        }

        value = get_value(tokens[0]) << std::stoul(tokens[2]);
    }
    else if(line.contains("RSHIFT")) {
        if(!is_value_or_assigned(tokens[0]))  {
            assignment(tokens[0]);
        }

        value = get_value(tokens[0]) >> std::stoul(tokens[2]);
    }
    else if(line.contains("NOT")) {
        if(!is_value_or_assigned(tokens[1]))  {
            assignment(tokens[1]);
        }

        value = ~get_value(tokens[1]);
    }
    else {
        if(!is_value_or_assigned(tokens[0])) {
            assignment(tokens[0]);
        }

        value = get_value(tokens[0]);
    }

    initialize_symbol(symbol_name, value);
}
