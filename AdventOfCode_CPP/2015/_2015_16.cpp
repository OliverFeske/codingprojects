//
// Created by Olive on 15/05/2024.
//

#include "_2015_16.h"

#include <fstream>
#include <iostream>
#include <sstream>

void _2015_16::run() {
    std::ifstream file{"../input_2015_16.txt", std::ifstream::in};
    if (!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    static std::map<std::string, std::pair<size_t, char>> real_sue{
            {"children:",    {3, '='}},
            {"cats:",        {7, '>'}},
            {"samoyeds:",    {2, '='}},
            {"pomeranians:", {3, '<'}},
            {"akitas:",      {0, '='}},
            {"vizslas:",     {0, '='}},
            {"goldfish:",    {5, '<'}},
            {"trees:",       {3, '>'}},
            {"cars:",        {2, '='}},
            {"perfumes:",    {1, '='}},
    };

    int high_score{-1000};
    while (!file.eof()) {
        std::string line{};
        std::getline(file, line);

        std::istringstream stream{line};
        std::string placeholder{}, compound1{}, compound2{}, compound3{};
        size_t c1_value{0}, c2_value{0}, c3_value{0}, number{};

        stream >> placeholder >> number >> placeholder
               >> compound1 >> c1_value >> placeholder
               >> compound2 >> c2_value >> placeholder
               >> compound3 >> c3_value;

        std::map<std::string, size_t> sue_to_test{
                {compound1, c1_value},
                {compound2, c2_value},
                {compound3, c3_value}
        };

        int score{0};
        for(auto&& [compound, amount] : sue_to_test) {
            score +=
                    (real_sue[compound].second == '>' && amount > real_sue[compound].first)
                    || (real_sue[compound].second == '<' && amount < real_sue[compound].first)
                    || (real_sue[compound].first == amount);
        }

        if(score >= high_score) {
            high_score = score;
            std::cout << "New or same high score[" << score << "]: " << number << "\n";
        }
    }

    file.close();
}
