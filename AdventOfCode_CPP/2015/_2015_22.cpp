//
// Created by Olive on 28/05/2024.
//

#include <iostream>
#include "_2015_22.h"

void _2015_22::run() {
    GameState initial_state;
    player_turn(initial_state);

    std::cout << "Result: " << _current_minimum_mana_spent << std::endl;
}

void _2015_22::player_turn(GameState game_state) {
    apply_effects(game_state);

    game_state.player_health--;

    if(game_state.player_health <= 0) {
        return;
    }

    if(game_state.enemy_health <= 0) {
        if(game_state.spent_mana < _current_minimum_mana_spent) {
            _current_minimum_mana_spent = game_state.spent_mana;
        }

        return;
    }

    magic_missile(game_state);
    drain(game_state);
    shield(game_state);
    poison(game_state);
    recharge(game_state);
}

void _2015_22::magic_missile(GameState game_state) {
    if(game_state.mana >= 53) {
        game_state.action_stack.push_back(Action::MagicMissile);
        game_state.mana -= 53;
        game_state.spent_mana += 53;
        game_state.enemy_health -= 4;

        if(game_state.enemy_health <= 0) {
            if(game_state.spent_mana < _current_minimum_mana_spent) {
                _current_minimum_mana_spent = game_state.spent_mana;
            }

            return;
        }

        enemy_turn(game_state);
    }
}

void _2015_22::drain(GameState game_state) {
    if(game_state.mana >= 73) {
        game_state.action_stack.push_back(Action::Drain);
        game_state.mana -= 73;
        game_state.spent_mana += 73;
        game_state.enemy_health -= 2;
        game_state.player_health += 2;

        if(game_state.enemy_health <= 0) {
            if(game_state.spent_mana < _current_minimum_mana_spent) {
                _current_minimum_mana_spent = game_state.spent_mana;
            }

            return;
        }

        enemy_turn(game_state);
    }
}

void _2015_22::shield(GameState game_state) {
    if(game_state.mana >= 113 && game_state.shield_left <= 1) {
        game_state.action_stack.push_back(Action::Shield);
        game_state.mana -= 113;
        game_state.spent_mana += 113;
        game_state.shield_left = 6;

        enemy_turn(game_state);
    }
}

void _2015_22::poison(GameState game_state) {
    if(game_state.mana >= 173 && game_state.poison_left <= 1) {
        game_state.action_stack.push_back(Action::Poison);
        game_state.mana -= 173;
        game_state.spent_mana += 173;
        game_state.poison_left = 6;

        enemy_turn(game_state);
    }
}

void _2015_22::recharge(GameState game_state) {
    if(game_state.mana >= 229 && game_state.recharge_left <= 1) {
        game_state.action_stack.push_back(Action::Recharge);
        game_state.mana -= 229;
        game_state.spent_mana += 229;
        game_state.recharge_left = 5;

        enemy_turn(game_state);
    }
}

void _2015_22::apply_effects(GameState &game_state) {
    if(game_state.poison_left > 0) {
        game_state.poison_left--;
        game_state.enemy_health -= 3;
    }

    game_state.shield_left--;

    if(game_state.recharge_left > 0) {
        game_state.recharge_left--;
        game_state.mana += 101;
    }
}

void _2015_22::enemy_turn(GameState game_state) {
    apply_effects(game_state);

    if(game_state.enemy_health <= 0) {
        if(game_state.spent_mana < _current_minimum_mana_spent) {
            _current_minimum_mana_spent = game_state.spent_mana;
        }

        return;
    }

    game_state.player_health -= game_state.shield_left > 0 ? game_state.enemy_damage - 7 : game_state.enemy_damage;

    if(game_state.player_health > 0) {
        player_turn(game_state);
    }
}
