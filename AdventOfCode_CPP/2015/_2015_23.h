//
// Created by Olive on 29/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_23_H
#define ADVENTOFCODE_CPP__2015_23_H

#include <vector>
#include <string>

class _2015_23 {
public:
    void run();

private:
    std::vector<std::string> _instructions;
    int _a{1}, _b{0};
};


#endif //ADVENTOFCODE_CPP__2015_23_H
