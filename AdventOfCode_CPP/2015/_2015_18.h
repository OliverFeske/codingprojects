//
// Created by Olive on 19/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_18_H
#define ADVENTOFCODE_CPP__2015_18_H

#include <vector>

class _2015_18 {
public:
    void run();

private:
    void print_grid(size_t step);

    std::vector<std::vector<bool>> _grid_1, _grid_2;
    std::vector<std::vector<bool>>* _sample_grid{nullptr};
    std::vector<std::vector<bool>>* _active_grid{nullptr};
    size_t _steps{100}, _num_turned_on{0}, _size{100};
};


#endif //ADVENTOFCODE_CPP__2015_18_H
