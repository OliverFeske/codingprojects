//
// Created by Olive on 14/05/2024.
//
/**
 * https://www.geeksforgeeks.org/hamiltonian-path-using-dynamic-programming/
 *
 * solved using the hamilton path on an adjacency matrix
 */

#ifndef ADVENTOFCODE_CPP__2015_09_H
#define ADVENTOFCODE_CPP__2015_09_H

#include <vector>
#include <string>
#include <map>

class _2015_09 {
public:
    void run();

private:
    std::map<std::string, std::map<std::string, size_t>> _location_distances;
    std::vector<std::pair<std::vector<std::string>, size_t>> _permutations_with_final_distance;
    std::vector<std::string> _locations;
};


#endif //ADVENTOFCODE_CPP__2015_09_H
