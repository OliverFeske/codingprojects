//
// Created by Olive on 29/05/2024.
//

#include <iostream>
#include "_2015_25.h"

void _2015_25::run() {
    unsigned long long count{2}, previous{20'151'125};
    while(true) {
        for(unsigned long long i{0}; i < count; i++) {
            unsigned long long row{count - i}, column{i + 1};

            previous = (previous * 252'533) % 33'554'393;

            if(row == 2981 && column == 3075) {
                std::cout << "Code: " << previous << std::endl;
                return;
            }
        }

        count++;
    }
}
