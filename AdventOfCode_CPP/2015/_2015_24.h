//
// Created by Olive on 29/05/2024.
//

#ifndef ADVENTOFCODE_CPP__2015_24_H
#define ADVENTOFCODE_CPP__2015_24_H

#include <vector>
#include <limits>

class _2015_24 {
public:
    void run();

private:
    void build_group(size_t current_index, const std::vector<size_t>& current_group, size_t current_sum);

    size_t _current_lowest_number_first_packages{1000}, _current_lowest_quantum_entanglement{std::numeric_limits<size_t>::max()}, _group_sum{/** 520*/390};
    std::vector<std::vector<size_t>> _possible_solutions;

    std::vector<size_t> _packages{
            1,
            2,
            3,
            7,
            11,
            13,
            17,
            19,
            23,
            31,
            37,
            41,
            43,
            47,
            53,
            59,
            61,
            67,
            71,
            73,
            79,
            83,
            89,
            97,
            101,
            103,
            107,
            109,
            113};
};


#endif //ADVENTOFCODE_CPP__2015_24_H
