//
// Created by Olive on 16/05/2024.
//

#include "_2015_17.h"

#include <iostream>
#include <chrono>

void _2015_17::run() {
    auto starting_time{std::chrono::high_resolution_clock::now()};

    for (int i{0}; i < 20; i++) {
        add_up(i, 0, 0);
    }

    auto end_time{std::chrono::system_clock::now()};

    std::cout << "Took " << std::chrono::duration_cast<std::chrono::microseconds>(end_time - starting_time).count()
              << " microseconds.\nResult: " << _num_found_combinations << " and " << _num_found_combinations_2 << "\n";
}

void _2015_17::add_up(int current_index, int current_sum, int num_used_containers) {
    current_sum += _containers[current_index];
    num_used_containers++;

    if(current_sum > 150) {
        return;
    }
    else if(current_sum == 150) {
        _num_found_combinations++;

        if(num_used_containers < _current_num_min_containers) {
            _num_found_combinations_2 = 0;
            _current_num_min_containers = num_used_containers;
        }

        _num_found_combinations_2 += num_used_containers == _current_num_min_containers;
        return;
    }

    for (int i{current_index + 1}; i < 20; i++) {
        add_up(i, current_sum, num_used_containers);
    }
}
