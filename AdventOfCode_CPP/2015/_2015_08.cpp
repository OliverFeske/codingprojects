//
// Created by Olive on 14/05/2024.
//

#include "_2015_08.h"

#include <iostream>
#include <fstream>

void _2015_08::run() {
    /*
    calculate_number("\"\"\r");
    calculate_number("\"abc\"\r");
    calculate_number("\"aaa\\\"aaa\"\r");
    calculate_number("\"\\x27\"\r");

    std::cout << "Result: " << _result << "\n";
    std::cout << "Result b: " << _result_b << "\n";

    return;
     */

    std::ifstream file{"../input_2015_08.txt", std::ifstream::in};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    while(!file.eof()) {
        std::string line{};
        std::getline(file, line);

        if(file.eof()) {
            line += '\r';
        }

        calculate_number(line);
    }

    file.close();

    std::cout << "Result: " << _result << "\n";
    std::cout << "Result b: " << _result_b << "\n";
}

void _2015_08::calculate_number(const std::string &line) {
    std::string actual_string{line.substr(1, line.size() - 3)};

    size_t string_size{2}, size_in_memory{0}, string_size_b{6};
    for(size_t i{0}; i < actual_string.size(); i++) {
        const char c{actual_string[i]};

        if(c == '\\')  {
            const char next{actual_string[i + 1]};
            if(next == '\"' || next == '\\') {
                string_size += 2;
                string_size_b += 4;
                i++;
            }
            else if(next == 'x') {
                string_size += 4;
                string_size_b += 5;
                i += 3;
            }
            else {
                std::cout << "Unhandled case!\n";
            }
        }
        else {
            string_size++;
            string_size_b++;
        }

        size_in_memory++;
    }

    _result += string_size - size_in_memory;
    _result_b += string_size_b - string_size;

    std::cout << "\"" << line << "\" | string size: " << string_size << " | size in memory: " << size_in_memory << " | string size b: " << string_size_b << "\n";
}
