//
// Created by Olive on 28/05/2024.
//

#include "_2015_21.h"
#include <iostream>

void _2015_21::run() {
    for(const Item& weapon : _weapons) {
        for(const Item& armor : _armor) {
            for(size_t i{0}; i < _rings.size() - 1; i++) {
                for(size_t j{i}; j < _rings.size(); j++) {
                    int current_cost{weapon.cost + armor.cost + _rings[i].cost + _rings[j].cost};
                    _player.damage = weapon.damage + _rings[i].damage + _rings[j].damage;
                    _player.armor = armor.armor + _rings[i].armor + _rings[j].armor;

                    if(fight_boss()) {
                        if(current_cost < _current_lowest_cost) {
                            _current_lowest_cost = current_cost;
                        }
                    }
                    else {
                        if(current_cost > _current_highest_cost) {
                            _current_highest_cost = current_cost;
                        }
                    }
                }
            }
        }
    }

    std::cout << "Current lowest cost: " << _current_lowest_cost << std::endl;
    std::cout << "Current highest cost: " << _current_highest_cost << std::endl;
}

bool _2015_21::fight_boss() {
    _player.health = 100;
    _boss.health = 100;

    while(true) {
        int damage_dealt{std::max(1, _player.damage - _boss.armor)};
        _boss.health -= damage_dealt;

        if(_boss.health <= 0) {
            return true;
        }

        damage_dealt = std::max(1, _boss.damage - _player.armor);

        _player.health -= damage_dealt;

        if(_player.health <= 0) {
            return false;
        }
    }
}
