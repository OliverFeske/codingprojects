//
// Created by Olive on 28/05/2024.
//

#include "_2015_20.h"
#include <iostream>

void _2015_20::run() {
    size_t current_house{800'000};
    while(true) {
        size_t sum{11}, elf{2};

        while(elf <= current_house) {
            if(current_house % elf == 0 && static_cast<float>(current_house) / static_cast<float>(elf) <= 50.f) {
                sum += elf * 11;
            }

            elf++;
        }

        if(sum >= 36'000'000) {
            std::cout << "Current: " << current_house << " | sum: " << sum << std::endl;
            break;
        }

        current_house++;
    }
}
