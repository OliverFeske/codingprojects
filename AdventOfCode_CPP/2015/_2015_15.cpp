//
// Created by Olive on 15/05/2024.
//

#include "_2015_15.h"

#include <iostream>

void _2015_15::run() {
    Ingredient sugar{3, 0, 0, -3, 2};
    Ingredient sprinkles{-3, 3, 0, 0, 9};
    Ingredient candy{-1, 0, 4, 0, 1};
    Ingredient chocolate{0, 0, -2, 2, 8};

    int max_score{0};
    for(int sugar_amount{0}; sugar_amount < 100; sugar_amount++) {
        for(int sprinkles_amount{0}; sprinkles_amount < 100 - sugar_amount; sprinkles_amount++) {
            for(int candy_amount{0}; candy_amount < 100 - sugar_amount - sprinkles_amount; candy_amount++) {
                int chocolate_amount = 100 - sugar_amount - sprinkles_amount - candy_amount;

                int calories{(sugar.calories * sugar_amount) + (sprinkles.calories * sprinkles_amount) + (candy.calories * candy_amount) + (chocolate.calories * chocolate_amount)};
                if(calories != 500) {
                    continue;
                }

                int capacity{(sugar.capacity * sugar_amount) + (sprinkles.capacity * sprinkles_amount) + (candy.capacity * candy_amount) + (chocolate.capacity * chocolate_amount)};
                int durability{(sugar.durability * sugar_amount) + (sprinkles.durability * sprinkles_amount) + (candy.durability * candy_amount) + (chocolate.durability * chocolate_amount)};
                int flavor{(sugar.flavor * sugar_amount) + (sprinkles.flavor * sprinkles_amount) + (candy.flavor * candy_amount) + (chocolate.flavor * chocolate_amount)};
                int texture{(sugar.texture * sugar_amount) + (sprinkles.texture * sprinkles_amount) + (candy.texture * candy_amount) + (chocolate.texture * chocolate_amount)};

                int score{0};
                if(capacity > 0 && durability > 0 && flavor > 0 && texture > 0) {
                    score = capacity * durability * flavor * texture;
                }

                if(score > max_score) {
                    max_score = score;
                    std::cout << "New max score " << max_score << " with following distribution: sugar[" << sugar_amount << "] sprinkles[" << sprinkles_amount << "] candy[" << candy_amount << "] chocolate[" << chocolate_amount << "]\n";
                }
            }
        }
    }

    std::cout << "Final max score: " << max_score << "\n";
}
