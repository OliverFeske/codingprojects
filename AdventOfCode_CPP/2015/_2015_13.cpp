//
// Created by Olive on 14/05/2024.
//

#include "_2015_13.h"

#include <fstream>
#include <iostream>
#include <sstream>

void _2015_13::run() {
    std::ifstream file{"../input_2015_13.txt", std::ifstream::in};
    if(!file.is_open()) {
        std::cout << "Could not open file!\n";
        return;
    }

    while(!file.eof()) {
        std::string line{};
        std::getline(file, line);

        std::string name_1{}, would{}, change{}, happiness{}, units{}, by{}, sitting{}, next{}, to{}, name_2{};
        int happiness_amount{0};
        std::istringstream stream{line};
        stream >> name_1 >> would >> change >> happiness_amount >> happiness >> units >> by >> sitting >> next >> to >> name_2;

        if(name_1[name_1.size() - 1] == '.') {
            name_1.pop_back();
        }

        if(name_2[name_2.size() - 1] == '.') {
            name_2.pop_back();
        }

        if(change == "lose") {
            happiness_amount = -happiness_amount;
        }

        if(!_happiness_table.contains(name_1)) {
            _happiness_table.insert_or_assign(name_1, std::map<std::string, int>{});
        }

        _happiness_table[name_1].insert_or_assign(name_2, happiness_amount);

        if(_people.end() == std::find(_people.begin(), _people.end(), name_1)) {
            _people.push_back(name_1);
        }

        if(_people.end() == std::find(_people.begin(), _people.end(), name_2)) {
            _people.push_back(name_2);
        }
    }

    file.close();

    std::sort(_people.begin(), _people.end());

    do {
        auto permutation{_people};
        permutation.push_back(_people[0]);

        int happiness{0};
        for(size_t i{0}; i < permutation.size() - 1; i++) {
            happiness += _happiness_table[permutation[i]][permutation[i + 1]];
            happiness += _happiness_table[permutation[i + 1]][permutation[i]];
        }

        _permutations_with_total_happiness.push_back(std::pair<std::vector<std::string>, int>(_people, happiness));
    } while(std::next_permutation(_people.begin(), _people.end()));

    std::cout << "Number permutations: " << _permutations_with_total_happiness.size() << "\n";

    int biggest_total_change{0};
    for(auto& pair : _permutations_with_total_happiness) {
        if(pair.second > biggest_total_change) {
            biggest_total_change = pair.second;
        }
    }

    std::cout << "Biggest total change in happiness: " << biggest_total_change << "\n";
}
